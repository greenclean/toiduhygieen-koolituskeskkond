SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE orders;
TRUNCATE TABLE tests;
TRUNCATE TABLE question_set;
TRUNCATE TABLE test_results;
TRUNCATE TABLE certificates;
TRUNCATE TABLE banklink_payments;
TRUNCATE TABLE banklink_payment_workers;
TRUNCATE TABLE course_user;
TRUNCATE TABLE deployments;
TRUNCATE TABLE learning_materials;
DROP TABLE translations_backup;
DELETE FROM users WHERE user_id != 1;
DELETE FROM courses WHERE courses.course_id not in (1,2);

UPDATE users
SET user_has_unused_english_certificate_version = 0 WHERE 1;

SET FOREIGN_KEY_CHECKS = 1;