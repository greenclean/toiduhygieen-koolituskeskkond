<?php
/* This script compares staging server database with local */

require '../config.php';

define('STAGING_DATABASE_HOSTNAME', 'test.diarainfra.com');
define('STAGING_DATABASE_USERNAME', 'toiduh');
define('STAGING_DATABASE_PASSWORD', 'fg2X446sTuJF!');
define('STAGING_DATABASE_DATABASE', 'toiduh');


$connStaging = mysqli_connect(STAGING_DATABASE_HOSTNAME, STAGING_DATABASE_USERNAME, STAGING_DATABASE_PASSWORD,
    STAGING_DATABASE_DATABASE) or die(mysqli_error($connStaging));
$connLocal = mysqli_connect(DATABASE_HOSTNAME, DATABASE_USERNAME, DATABASE_PASSWORD,
    DATABASE_DATABASE) or die(mysqli_error($connLocal));

mysqli_select_db($connStaging, 'information_schema') or die(mysqli_error($connStaging));
mysqli_select_db($connLocal, 'information_schema') or die(mysqli_error($connLocal));

mysqli_query($connStaging, "SET NAMES 'utf8'") or die (mysqli_error($connStaging));
mysqli_query($connLocal, "SET NAMES 'utf8'") or die (mysqli_error($connLocal));

mysqli_query($connStaging, "SET CHARACTER SET utf8") or die (mysqli_error($connStaging));
mysqli_query($connLocal, "SET CHARACTER SET utf8") or die (mysqli_error($connLocal));

$q = mysqli_query($connStaging, "SELECT * FROM `COLUMNS` WHERE TABLE_SCHEMA = '" . STAGING_DATABASE_DATABASE . "'");

$staging_tables = [];

while ($row = mysqli_fetch_assoc($q)) {
    $staging_tables[$row['TABLE_NAME']][$row['COLUMN_NAME']] = $row;
}

$q = mysqli_query($connLocal, "SELECT * FROM `COLUMNS` WHERE TABLE_SCHEMA = '" . DATABASE_DATABASE . "'");

$local_tables = [];

while ($row = mysqli_fetch_assoc($q)) {
    $local_tables[$row['TABLE_NAME']][$row['COLUMN_NAME']] = $row;
}

foreach ($staging_tables as $table_name => $columns) {

    if (!isset($local_tables[$table_name])) {

        $missing_tables[] = $table_name;

        // Get table definition
        $q = mysqli_query($connStaging,
            "SHOW CREATE TABLE `" . STAGING_DATABASE_DATABASE . "`.`$table_name`") or die(mysqli_error($connStaging));
        while ($row = mysqli_fetch_assoc($q)) {
            $create_tables[] = $row['Create Table'] . ";";
        }

    } else {
        foreach ($columns as $column_name => $column) {
            if (!isset($local_tables[$table_name][$column_name])) {
                $missing_columns[] = "$table_name.$column_name";
            } else {

                // MariaDB lists COLUMN_DEFAULT NULL as string ("NULL")
                if (isset($local_tables[$table_name][$column_name]['COLUMN_DEFAULT']) && $local_tables[$table_name][$column_name]['COLUMN_DEFAULT'] == 'NULL') {
                    $local_tables[$table_name][$column_name]['COLUMN_DEFAULT'] = null;
                }

                // MariaDB lists CURRENT_TIMESTAMP as string "current_timestamp()", not CURRENT_TIMESTAMP
                if (isset($local_tables[$table_name][$column_name]['COLUMN_DEFAULT']) && $local_tables[$table_name][$column_name]['COLUMN_DEFAULT'] == 'current_timestamp()') {
                    $local_tables[$table_name][$column_name]['COLUMN_DEFAULT'] = 'CURRENT_TIMESTAMP';
                }

                // MariaDB lists empty default value as a string with two quotes
                if (isset($local_tables[$table_name][$column_name]['COLUMN_DEFAULT']) && $local_tables[$table_name][$column_name]['COLUMN_DEFAULT'] == "''") {
                    $local_tables[$table_name][$column_name]['COLUMN_DEFAULT'] = '';
                }


                // Compare columns
                $fullDiff = array_merge(array_diff($local_tables[$table_name][$column_name],
                    $staging_tables[$table_name][$column_name]),
                    array_diff($staging_tables[$table_name][$column_name], $local_tables[$table_name][$column_name]));

                // Schema name is always different
                unset($fullDiff['TABLE_SCHEMA']);

                // Some new field in MariaDB
                unset($fullDiff['IS_GENERATED']);

                // The order of the columns in the table is irrelevant
                unset($fullDiff['ORDINAL_POSITION']);

                // Privileges in local might be different
                unset($fullDiff['PRIVILEGES']);

                if (!empty($fullDiff)) {

                    foreach (array_keys($fullDiff) as $item) {
                        $fullDiff['local'][$item] = $local_tables[$table_name][$column_name][$item];
                        $fullDiff['staging'][$item] = $staging_tables[$table_name][$column_name][$item];
                    }
                    $column_diffs["$table_name.$column_name"] = $fullDiff;

                }
            }
        }
    }
}
$redundant_columns = [];
foreach ($local_tables as $table_name => $columns) {

    if (!isset($staging_tables[$table_name])) {

        $redundant_tables[] = $table_name;
        $drop_tables[] = "DROP TABLE $table_name;";
        continue;
    }
    foreach ($columns as $column_name => $column) {
        if (!isset($staging_tables[$table_name][$column_name])) {
            $redundant_columns[$table_name][$column_name] = [
                "COLUMN_TYPE" => $column["COLUMN_TYPE"],
                "EXTRA" => $column["EXTRA"],
                "COLUMN_KEY" => $column["COLUMN_KEY"],
                "IS_NULLABLE" => $column["IS_NULLABLE"],
                "COLUMN_DEFAULT" => $column["COLUMN_DEFAULT"],
                "COLUMN_COMMENT" => $column["COLUMN_COMMENT"],
                "CHARACTER_OCTET_LENGTH" => $column["CHARACTER_OCTET_LENGTH"],
                "CHARACTER_MAXIMUM_LENGTH" => $column["CHARACTER_MAXIMUM_LENGTH"]
            ];
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Local and staging databases comparison</title>
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <h1>Tables that are in staging but not in local database:</h1>
    <ul>
        <?php if (!empty($missing_tables)) foreach ($missing_tables as $missing_table): ?>
            <li><?php echo $missing_table ?></li>
        <?php endforeach ?>
    </ul>
    <?php if (!empty($create_tables)) foreach ($create_tables as $create_table): ?>
        <pre><?php echo $create_table ?></pre>
    <?php endforeach ?>

    <h1>Fields that are missing from local database:</h1>
    <ul>
        <?php if (!empty($missing_columns)) foreach ($missing_columns as $missing_column): ?>
            <li><?php echo $missing_column ?></li>
        <?php endforeach ?>
    </ul>

    <h1>Fields that are different in the local database:</h1>
    <ul>
        <?php if (!empty($column_diffs)) foreach ($column_diffs as $col_name => $differing_column): ?>
            <li>
                <h4><?php echo $col_name ?></h4>
                <ul>
                    <?php if (!empty($differing_column)) foreach ($differing_column as $col_type_name => $col_type_value): ?>
                        <li><?php echo $col_type_name ?> =
                            <pre><?php echo json_encode($col_type_value, JSON_PRETTY_PRINT) ?></pre>
                        </li>
                    <?php endforeach ?></ul>
            </li>
        <?php endforeach ?>
    </ul>

    <h1>Fields that exist in local but not in staging:</h1>
    <?php if (!empty($redundant_columns)) foreach ($redundant_columns as $col_name => $column): ?>
        <li>
            <h4><?php echo $col_name ?></h4>
            <ul>
                <?php if (!empty($column)) foreach ($column as $col_type_name => $col_type_value): ?>
                    <li><?php echo $col_type_name ?> =
                        <pre><?php echo json_encode($col_type_value, JSON_PRETTY_PRINT) ?></pre>
                    </li>
                <?php endforeach ?></ul>
        </li>
    <?php endforeach ?>

    <h1>Tables which exist in local but not in staging:</h1>
    <ul>
        <?php if (!empty($redundant_tables)) foreach ($redundant_tables as $redundant_table): ?>
            <li><?php echo $redundant_table ?></li>
        <?php endforeach ?>
    </ul>
    <?php if (!empty($drop_tables)) foreach ($drop_tables as $drop_table): ?>
        <pre style="color: red; font-weight: bold"><?php echo $drop_table ?></pre>
    <?php endforeach ?>
</div>

</body>
</html>
