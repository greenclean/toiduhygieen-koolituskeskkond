<?php


namespace Toiduhygieen;


class Question
{

    public static function get_all($criteria = null): array
    {

        $language_codes_in_use = Language::get_codes_in_use();

        $select = [
            'question_id',
            'question_translations.translation_phrase as question',
            'test_category_id',
            'explanation_translations.translation_phrase as explanation',
            'answer_id',
            'answer_is_correct',
            'answer_translations.translation_phrase as answer',
            'answer_letter',
            'answer_translations.translation_id as answer_translation_id',
            'question_translations.translation_id as question_translation_id',
            'explanation_translations.translation_id as explanation_translation_id',
            'question_set.answer_chosen as answer_chosen'
        ];
        foreach ($language_codes_in_use as $code) {
            $select[] = "question_translations.translation_in_$code as translation_in_$code";
            $select[] = "explanation_translations.translation_in_$code as explanation_translation_in_$code";
            $select[] = "answer_translations.translation_in_$code as answer_translation_in_$code";
        }
        $select = implode(",", $select);

        $result = [];
        $where = SQL::getWhere($criteria, "language_id");
        $rows = get_all("SELECT $select FROM questions 
    left join answers using (question_id) 
    left join translations as question_translations on questions.question_id = question_translations.translation_source_id AND question_translations.translation_source = 'questions'
    left join translations as explanation_translations on questions.question_id = explanation_translations.translation_source_id AND explanation_translations.translation_source = 'questions.explanation'
    left join translations as answer_translations on answers.answer_id = answer_translations.translation_source_id AND answer_translations.translation_source = 'answers'
    left join question_set using (question_id)
    $where ORDER BY question_id, answer_letter");


        foreach ($rows as $row) {

            $result[$row['question_id']]['question_id'] = $row['question_id'];
            $result[$row['question_id']]['test_category_id'] = $row['test_category_id'];
            $result[$row['question_id']]['question'] = $row['question'];
            $result[$row['question_id']]['question_translation_id'] = $row['question_translation_id'];
            $result[$row['question_id']]['explanation'] = $row['explanation'];
            $result[$row['question_id']]['explanation_translation_id'] = $row['explanation_translation_id'];

            foreach ($language_codes_in_use as $code) {
                $result[$row['question_id']]['translations'][$code] = $row['translation_in_' . $code];
                $result[$row['question_id']]['explanation_translations'][$code] = $row['explanation_translation_in_' . $code];
            }
            if (!empty($row['answer_id'])) {
                $result[$row['question_id']]['answers'][$row['answer_id']] = [
                    'answer_id' => $row['answer_id'],
                    'answer_is_correct' => $row['answer_is_correct'],
                    'answer' => $row['answer'],
                    'answer_letter' => $row['answer_letter'],
                    'answer_chosen' => $row['answer_chosen']
                ];
                foreach ($language_codes_in_use as $code) {
                    $result[$row['question_id']]['answers'][$row['answer_id']]['translations'][$code] = $row["answer_translation_in_$code"];
                    $result[$row['question_id']]['answers'][$row['answer_id']]['answer_translation_id'] = $row["answer_translation_id"];
                }
            }
        }
        return $result;
    }
}