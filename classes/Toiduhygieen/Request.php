<?php


namespace Toiduhygieen;


class Request
{
    public static function isAjax(): bool
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    public static function isCli(): bool
    {
        return PHP_SAPI === 'cli';
    }

}