<?php


namespace Toiduhygieen;

use ErrorException;
use Exception;
use FilesystemIterator;
use RecursiveCallbackFilterIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Stichoza\GoogleTranslate\GoogleTranslate;

class Translation
{
    public static $translations = [];
    private static $translations_dynamic = [];
    private static $fixes = [
        'bs' => '/(\n\|. \n)/',
        'ru' => '/(\n\|.\n)/',
        'co' => '/(\nŒ œ\n)/',
        'eu' => '/(\n\| \| \| \|]\n)/'
    ];

    public static function check_code_for_new_phrases($dirs)
    {
        Translation::get_strings();

        $code_files = [];

        // Get a list of files to look within
        foreach ($dirs as $dir) {
            $code_files = array_merge($code_files, self::get_files_from_path($dir));
        }

        // Scan files for phrases
        $current_phrases = self::find_phrases($code_files);

        // Set all phrases as not existing in the code
        q("UPDATE translations SET translation_state = 'does_not_exist' WHERE translation_state != 'dynamic'");

        if (!empty($current_phrases)) {

            $language_codes = Language::get_codes_in_use();

            // Avoid issues with quotes in phrases by adding slashes before quotes
            foreach ($current_phrases as &$phrase) {

                $phrase = trim($phrase);

                if (strlen($phrase) > 765) {

                    // Shorten phrase
                    $phrase = substr($phrase, 0, 765);

                }

                // Escape the phrase
                $escapedPhrase = addslashes($phrase);

                // Insert only missing translations
                if (empty(self::$translations) || !in_array($phrase, array_keys(self::$translations))) {

                    // Prevent double records in the database
                    self::$translations[$phrase] = $phrase;

                    // Add the phrase to INSERT INTO translations
                    $valuesToInsert[$phrase] = $phrase;

                    // Add the phrase to phrases to be translated
                    $phrasesToGoogleTranslate[$phrase] = $phrase;

                } else {
                    // Add the phrase to UPDATE translations
                    $valuesToUpdate[$phrase] = $escapedPhrase;
                }
            }

            if (!empty($valuesToInsert)) {

                // Convert $values array to string
                $valuesToInsert = SQL::generate_values_for_insert($valuesToInsert);

                // Insert all strings to database
                q("INSERT INTO translations (translation_phrase) VALUES $valuesToInsert");


            }
            if (!empty($valuesToUpdate)) {

                // Update existing translations
                foreach ($valuesToUpdate as $phrase) {
                    update('translations', [
                        'translation_state' => 'exists_in_code'
                    ], "translation_phrase = '$phrase'");
                }

            }

            // Delete strings that do not exist any more
            q("DELETE FROM translations WHERE translation_state='does_not_exist'");
            self::insert_missing_dynamic_phrases();

            // Google translate new strings
            foreach ($language_codes as $language_code) {

                // Prevent automatically translating english phrases
                if ($language_code === 'en') {
                    continue;
                }

                self::google_translate_missing_translations($language_code);
            }

            self::delete_unused_dynamic_translations();
        }
    }

    static function get_strings($type_to_return = 'static')
    {
        if (!empty(self::$translations) && $type_to_return === 'static') {
            return self::$translations;
        }

        if (!empty(self::$translations_dynamic) && $type_to_return === 'dynamic') {
            return self::$translations_dynamic;
        }

        $lang_col = Language::get_current()['language_code'];

        // Handle case when current language has been just deleted from the DB
        $translation_column = in_array($lang_col, Language::get_codes_in_use())
            ? "translation_in_$lang_col" : "NULL AS translation_in_$lang_col";

        $rows = get_all("
            SELECT translation_phrase, $translation_column, translation_source, translation_source_id 
            FROM translations");


        foreach ($rows as $row) {

            if (empty($row['translation_source'])) {

                self::$translations[$row['translation_phrase']] = $row["translation_in_$lang_col"] === NULL ? $row['translation_phrase']
                    : $row["translation_in_$lang_col"];
            } else {

                self::$translations_dynamic[$row['translation_source']][$row['translation_source_id']] = $row["translation_in_$lang_col"] === NULL ? $row['translation_phrase']
                    : $row["translation_in_$lang_col"];
            }
        }

        if ($type_to_return === 'dynamic')
            var_dump(self::$translations_dynamic);
        return $type_to_return === 'static' ? self::$translations : self::$translations_dynamic;
    }

    public static function get_files_from_path($path, $file_types = ['php', 'js']): array
    {
        $result = array();
        $directory = new RecursiveDirectoryIterator($path, FilesystemIterator::FOLLOW_SYMLINKS);
        $filter = new RecursiveCallbackFilterIterator($directory, function ($current, $key, $iterator) use ($file_types) {

            // Skip hidden files and directories.
            if ($current->getFilename()[0] === '.' || $current->getFilename()[0] === '..') {
                return FALSE;
            }
            if ($current->isDir()) {
                return true;

            } else {
                // Only consume files of interest.
                $pathinfo = pathinfo($current->getFilename());
                if (empty($pathinfo['extension'])) {
                    return false;
                }
                return in_array($pathinfo['extension'], $file_types);
            }
        });

        $files = new RecursiveIteratorIterator($filter);

        foreach ($files as $file) {
            $result[] = $file->getPathname();
        }
        return $result;

    }

    /**
     * @param array $files Files to search
     * @return array Found phrases
     */
    private static function find_phrases(array $files): array
    {
        $phrases = [];

        foreach ($files as $file) {

            // Find __( 'xxx' ) and __( "xxx" );
            preg_match_all(
                '/__\("(?:[^\'\\\]|\\\\.)+"\)|__\(\'(?:[^\'\\\]|\\\\.)+\'\)/U',
                file_get_contents($file),
                $matches);

            $matches = self::remove_escapes_from_quotes($matches);

            $phrases = array_merge($phrases, $matches[0]);

        }

        $phrases = array_unique($phrases, SORT_STRING);

        // Remove _ _ ( ' from the beginning and ' ) from the end of the phrases
        array_walk($phrases, function (&$item) {
            $item = substr($item, 4, -2);
        });

        return $phrases;
    }

    /**
     * @param $matches
     * @return mixed
     */
    private static function remove_escapes_from_quotes($matches)
    {
        for ($n = 0; $n < count($matches[0]); $n++) {
            $matches[0][$n] = str_replace("\\'", "'", $matches[0][$n]);
        }
        return $matches;
    }

    public static function insert_missing_dynamic_phrases(): void
    {
        $phrases_to_insert = [];

        // Get all possible tables with dynamic content
        $sources = get_col("SELECT DISTINCT translation_source FROM translations WHERE translation_state = 'dynamic'");

        // Get dynamic strings to insert
        foreach ($sources as $source) {
            if ($source === 'questions.explanation') {
                continue;
            }
            $phrases_to_insert[$source] = self::get_missing_phrases_from_source($source);
        }

        // Insert strings to database
        foreach ($phrases_to_insert as $source => $phrases) {

            // Refuse to insert empty phrase to translations table
            if (empty($phrases)) {
                break;
            }

            // Reset values for new source
            $values = [];

            // Build ('foo','table1.field1', 'dynamic'),('bar','table1.field1', 'dynamic')...
            foreach ($phrases as $key => $phrase) {
                $values[$key] = "'" . addslashes($phrase) . "','$source','dynamic'";
            }
            $values = '(' . implode('),(', $values) . ')';

            // Insert them
            q("Insert into translations (translation_phrase, translation_source, translation_state) values $values");

            // Add new dynamic strings to self::$translations to avoid duplicate key database errors
            self::$translations = array_merge(self::$translations, $phrases);
        }
    }

    /**
     * @param $column
     * @param $table
     * @param string $existing_phrases
     * @param array $strings_to_insert
     * @param $source
     * @return array
     */
    private static function get_missing_phrases_from_source($source): array
    {
        // Get table and column
        list($table, $column) = self::get_table_and_field_from_source($source);
        if (!$table || !$column) {
            return [];
        }

        $existing_phrases = SQL::generate_values_for_in(
            get_col("SELECT translation_phrase FROM translations where translation_source = '$source'")
        );

        $result = get_col("SELECT $column FROM $table WHERE $column not in ($existing_phrases)");
        $result = array_unique($result);

        // Copy $result values to its keys
        $result = array_combine($result, $result);

        return $result;
    }

    /**
     * @param $source
     * @return array
     */
    private static function get_table_and_field_from_source($source): array
    {
        $source = explode('.', $source);

        $table = empty($source[0]) ? false : $source[0];
        $column = empty($source[1]) ? false : $source[1];

        return array($table, $column);
    }

    /**
     * Translates max 5000 characters
     * @param $language_code
     * @return int|mixed
     */
    public static function google_translate_missing_translations($language_code)
    {
        // Translate dynamic strings from detected language
        $remaining= self::translate_untranslated_strings($language_code, true);

        // Translate static strings from default language
        $remaining+= self::translate_untranslated_strings($language_code, false);

        return $remaining;
    }

    public static function translate_untranslated_strings($language_code, $dynamic = false)
    {

        // No language given
        if (empty($language_code) || $language_code === 'en')
            return 0;

        $where_dynamic = $dynamic ? "translation_source is not null" : "translation_source is null";

        $untranslated_strings = get_col("SELECT translation_phrase FROM translations WHERE translation_in_$language_code IS NULL AND $where_dynamic");

        $translated_strings = self::google_translate($dynamic ? NULL : 'en', $language_code, $untranslated_strings);


        // Loop over translated array
        for ($n = 0; $n < count($translated_strings); $n++) {

            // Add translation to DB
            update(
                'translations', [
                'translation_in_' . $language_code => substr($translated_strings[$n], 0, 765)
            ], "translation_phrase = '" . addslashes($untranslated_strings[$n]) . "'");
        }

        return self::get_statistics([$language_code])[$language_code]['remaining'];
    }

    public static function google_translate($language_from_code, $language_to_code, $untranslatedStrings): array
    {

        // Build max 5000 char string
        foreach ($untranslatedStrings as $untranslatedString) {

            // Add to array
            $payload[] = $untranslatedString;

            // Check if the imploded length is over 5000
            if (strlen(implode("\n|\n", $payload)) >= 5000) {

                // Remove last member
                array_pop($payload);

                // Cancel foreach
                break;
            }
        }

        // Nothing to translate
        if (empty($payload)) {
            return [];
        }

        // Translate payload from default language (static strings) or detected language (dynamic strings)
        $googleTranslated = GoogleTranslate::trans(implode("\n|\n", $payload), $language_to_code, $language_from_code);

        self::fix_broken_separator($language_to_code, $googleTranslated);

        $googleTranslated = preg_replace("/training/", "course", $googleTranslated);
        $googleTranslated = preg_replace("/Training/", "Course", $googleTranslated);

        // Convert translated strings back to array
        return explode("\n|\n", $googleTranslated);

    }

    /**
     * @param $language_code
     * @param string $googleTranslated
     * @return string|string[]|null
     */
    public static function fix_broken_separator($language_code, &$googleTranslated)
    {


        $pattern = empty(self::$fixes[$language_code]) ? null : self::$fixes[$language_code];
        if ($pattern) {
            // Fix broken separators
            $googleTranslated = preg_replace($pattern, "\n|\n", $googleTranslated);
        }
    }

    public static function delete_unused_dynamic_translations(): void
    {
        $dynamic_translations = get_all("SELECT * FROM translations WHERE translation_state='dynamic'");

        foreach ($dynamic_translations as $dynamic_translation) {

            // Skip iteration if translation_source is not in required format
            if (substr_count($dynamic_translation['translation_source'], '.') != 1) {
                continue;
            }

            list($table, $column) = self::get_table_and_field_from_source($dynamic_translation['translation_source']);

            if (!$table || !$column) {
                continue;
            }

            // Make sure specified table and column actually exist
            if (q("show tables like '$table'") != 1 || q("show columns from $table like '$column'") != 1) {
                continue;
            }

            $values = get_col("SELECT $column FROM $table");

            // Delete the translation if it's no longer in the database
            if (!in_array($dynamic_translation['translation_phrase'], $values)) {
                q("DELETE FROM translations WHERE translation_id = '$dynamic_translation[translation_id]'");
            }
        }
    }

    public static function add(string $translation_phrase, $dynamic_source, $source_id)
    {

        $data = [
            'translation_phrase' => $translation_phrase,
            'translation_state' => $dynamic_source ? 'dynamic' : 'exists_in_code'
        ];

        if ($dynamic_source) {
            $data['translation_source'] = $dynamic_source;
            $data['translation_source_id'] = $source_id;
        }

        insert('translations', $data);

        // Prevent gaps in translations.translation_id due to auto_increment increasing with ON DUPLICATE KEY UPDATE..
        self::$translations[$translation_phrase] = $translation_phrase;
    }

    public static function get_untranslated(array $languages)
    {

        $criteria = empty($languages) ? [] :
            ['translation_in_' . implode(' IS NULL OR translation_in_', $languages) . ' IS NULL'];

        // Remove answer, question and explanation phrases from translations table (these phrases can be edited in admin/questions)
        $criteria[] = "(translation_source is null or translation_source not in ('answers', 'questions', 'questions.explanation'))";
        return self::get_all_languages($criteria);
    }

    public static function get_all_languages($criteria = null)
    {
        $where = SQL::getWhere($criteria);
        return get_all("SELECT * FROM translations $where ORDER BY translation_phrase");
    }

    public static function get_statistics($languages = [])
    {
        $result = [];
        $sums = '';
        $languages = empty($languages) ? Language::get_codes_in_use() : $languages;

        foreach ($languages as $language) {
            $sums .= "SUM(IF(translation_in_$language IS NULL, 0, 1)) As translated_in_$language,";
            $sums .= "SUM(IF(translation_in_$language IS NULL, 1, 0)) As remaining_in_$language,";
        }

        $data = get_first("SELECT $sums COUNT(translation_id) total FROM translations");

        foreach ($languages as $language) {
            $result['total'] = $data['total'];
            $result[$language]['remaining'] = $data['remaining_in_' . $language];
            $result[$language]['translated'] = $data['translated_in_' . $language];
        }
        return $result;
    }

}