<?php namespace Toiduhygieen;

class Transliteration
{
    private static array $symbols = [
        'щ' => 'sch',
        'ш' => 'sh',
        'ч' => 'ch',
        'ц' => 'ts',
        'ю' => 'ju',
        'я' => 'ja',
        'ж' => 'zh',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'e',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ь' => '',
        'ы' => 'y',
        'ъ' => '',
        'э' => 'e',
        'і' => 'i',
        'ґ' => 'g',
        'ї' => 'i',
        'є' => 'je',
        'ğ' => 'g',
        'ş' => 'sh',
        'ı' => 'i',
        'ö' => 'o',
        'ü' => 'u',
        'ç' => 'c',
        'Ş' => 'Sh', // mb_strtoupper does not work on ş
        'ў' => 'u'

    ];

    private static array $symbolsUpper = [];

    /**
     * Converts non-Estonian letters within a string into their latin equivalents.
     * @param string $string The input string, UTF-8.
     * @return string
     */
    public static function symbolsToLatin(string $string): string
    {
        // Generate upper version of symbols, if not already generated
        if (empty(self::$symbolsUpper)) {
            foreach (self::$symbols as $symbol => $transliteration) {
                self::$symbolsUpper[mb_strtoupper($symbol)] = mb_strtoupper($transliteration);
            }
        }

        $string = str_replace(array_keys(self::$symbols), self::$symbols, $string);
        $string = str_replace(array_keys(self::$symbolsUpper), self::$symbolsUpper, $string);

        return $string;
    }


}