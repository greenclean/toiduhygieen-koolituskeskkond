<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 28/07/2017
 * Time: 13:58
 */

namespace Toiduhygieen;

class Course extends Model
{

    static function get_by_user($user)
    {
        $VAT = VAT_PERCENT;
        $REPEAT_TEST_PRICE = REPEAT_TEST_PRICE;
        $UPGRADE_CERTIFICATE_PRICE = UPGRADE_CERTIFICATE_PRICE;
        $result = [];
        $where = '';
        $certificate_ids = [];

        if (isset($user['logged_in']) && $user['logged_in']) {
            if (!$user['is_admin']) {
                $user_course_ids = [];

                $certificate_course_ids = get_all("SELECT course_id FROM courses WHERE course_type != 'course'");
                foreach ($certificate_course_ids as $certificate_course_id) {
                    $certificate_ids[] = $certificate_course_id['course_id'];
                }

                if (!empty($user['employer_id'])) {
                    if ($user['employer_id'] != $user['user_id']) {
                        $employ = get_first('SELECT allow_inherit FROM users WHERE user_id = ' . $user['employer_id']);

                        if ($employ['allow_inherit']) {
                            $users_ids = '(' . $user['employer_id'] . ', ' . $user['user_id'] . ')';
                        } else {
                            $users_ids = '(' . $user['user_id'] . ')';
                        }
                    } else {
                        $users_ids = '(' . $user['user_id'] . ')';
                    }
                } else {
                    $users_ids = '(' . $user['user_id'] . ')';
                }


                $where = '';

                $user_courses = get_all('SELECT course_id FROM course_user WHERE user_id IN ' . $users_ids);

                if (!empty($user_courses)) {
                    foreach ($user_courses as $user_course) {
                        $user_course_ids[] = $user_course['course_id'];
                    }
                }

                $user_course_ids = implode(',', $user_course_ids) ? implode(',', $user_course_ids) : '';

                if ($user_course_ids || $certificate_ids) {
                    $where = 'WHERE courses.course_id IN (' . implode(',', $certificate_ids) . $user_course_ids . ')';
                }

            }
        }
        Certificate::deleteExpired();

        $rows = get_all("SELECT *, 
                                    round(course_price * (1 + $VAT / 100), 2) as course_total_price
                                FROM
                                (SELECT
                                  course_id, course_type, course_name, order_id, test_id, course_status,
                                  CASE
                                  WHEN course_status = 'completed' 
                                    THEN course_price
                                  WHEN course_status = 'failed' and $REPEAT_TEST_PRICE != 0
                                    THEN $REPEAT_TEST_PRICE
                                  ELSE course_price
                                  END AS course_price
                                FROM (SELECT DISTINCT
                                            
                                        courses.course_id,
                                        course_type,
                                        course_name,
                                        course_price,
                                        order_id,
                                        test_id,
                                        CASE
                                        WHEN user_has_unused_english_certificate_version = 1 AND course_type = 'certificate'
                                          THEN 'user_has_unused_english_certificate_version'
                                        WHEN orders.order_id IS NULL OR course_type = 'certificate'
                                          THEN 'unpurchased'
                                        WHEN (test_score < 75)
                                          THEN 'failed'
                                        WHEN (test_id IS NULL) AND order_status_id = 1
                                          THEN 'purchased'
                                        WHEN test_score IS NULL AND order_status_id = 1
                                          THEN 'started'
                                        WHEN order_status_id = 3
                                          THEN 'unconfirmed'
                                        WHEN test_score >= 75
                                            THEN IF(certificate_id IS NULL, 'expired', 'completed')
                                        END AS course_status
                                      FROM courses
                                        LEFT JOIN (SELECT
                                                     course_id,
                                                     max(order_id) order_id
                                                   FROM orders
                                                   WHERE user_id = $user[user_id]
                                                   GROUP BY course_id) AS max_o USING (course_id)
                                        LEFT JOIN orders USING (order_id)
                                        LEFT JOIN tests USING (order_id)
                                        LEFT JOIN certificates USING (test_id)
                                        LEFT JOIN users ON (orders.user_id = users.user_id)
                                        $where
                                      GROUP BY courses.course_id) AS foo) as bar");

        foreach ($rows as $row) {
            $result[$row['course_id']] = $row;
        }

        return $result;
    }

    static function material_exists($learning_material_name)
    {
        $data = get_one("SELECT learning_material_id 
                        FROM learning_materials 
                        WHERE learning_material_name = '$learning_material_name'");

        if (empty($data)) {
            return false;
        } else {
            return true;
        }
    }

    public static function language_ids_in_use() {
        return get_all("select language_id from courses group by language_id");
    }
}