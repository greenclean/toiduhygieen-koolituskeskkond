<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 28/07/2017
 * Time: 13:58
 */

namespace Toiduhygieen;

class Order
{
    static function get($criteria = null, $offset = null, $count = null)
    {
        $function = is_numeric($criteria) ? "get_first" : "get_all";
        $where = SQL::getWhere($criteria, "order_id");
        $limit = SQL::getLimit($offset, $count);
        return $function("
            SELECT *,
                order_price AS order_total_price,
                (SELECT COUNT(*) FROM
                (SELECT orders.order_id
                 FROM orders
                 LEFT JOIN users USING (user_id)
                 LEFT JOIN order_statuses USING (order_status_id)
                 LEFT JOIN courses USING (course_id)
                 LEFT JOIN payment_methods USING (payment_method_id) $where
                 GROUP BY orders.order_id) as foo ) AS order_count
            FROM orders
            LEFT JOIN users USING (user_id)
            LEFT JOIN order_statuses USING (order_status_id)
            LEFT JOIN courses USING (course_id)
            LEFT JOIN payment_methods USING (payment_method_id)
            $where
            ORDER BY order_id DESC
            $limit");
    }

    static function getMulti(array $order_ids)
    {
        $orders_ids =  implode(',', $order_ids);

        $orders = get_all("SELECT *,
                                order_price AS order_total_price
                                FROM orders 
                                LEFT JOIN users USING (user_id)
                                LEFT JOIN order_statuses USING (order_status_id)
                                LEFT JOIN courses USING (course_id)
                                LEFT JOIN payment_methods USING (payment_method_id)
                                WHERE order_id IN  ($orders_ids)
                                ORDER BY order_id DESC
        ");

        return $orders;
    }

    static function getAll()
    {
        return self::get();
    }

    static function getRange( $offset, $count){

        return get_all("
            SELECT *,
                order_price AS order_total_price
            FROM orders 
            LEFT JOIN users USING (user_id)
            LEFT JOIN order_statuses USING (order_status_id)
            LEFT JOIN courses USING (course_id)
            LEFT JOIN payment_methods USING (payment_method_id)
            ORDER BY order_id desc
            LIMIT $offset, $count");
    }


    static function create($user_id, $order_date, $course_id, $payment_method_id, $order_status_id, $order_price, $order_price_vat, $banklink_payment_id, $company_name)
    {
        $order['user_id'] = $user_id;
        $order['order_date'] = $order_date;
        $order['course_id'] = $course_id;
        $order['payment_method_id'] = $payment_method_id;
        $order['order_status_id'] = $order_status_id;
        $order['order_price'] = $order_price;
        $order['order_price_vat'] = $order_price_vat;
        $order['banklink_payment_id'] = $banklink_payment_id;
        $order['company_name'] = $company_name;

        $order_id = insert('orders', $order);

        return $order_id;
    }

    static function exists($banklink_payment_id)
    {
        $orders = get_all("SELECT * 
                                    FROM orders 
                                    WHERE banklink_payment_id = $banklink_payment_id 
                                    ORDER BY order_id
        ");

        return $orders ? true : false;
    }

    public static function getCount(array $criteria)
    {
        $where = SQL::getWhere($criteria);
        return get_one("SELECT COUNT(*) FROM orders $where");
    }


    public static function setStatus($order_id, $status)
    {
        update("orders", ['order_status_id' => $status], "order_id=$order_id");

        $order = get_first("SELECT course_id, user_id FROM orders WHERE order_id = $order_id");

        // If user has purchased additional english language version of the certificate
        if ($order['course_id'] == 3 && $status == 1) {
            update('users', [
                'user_has_unused_english_certificate_version' => 1
            ], "user_id = $order[user_id]");
            update('certificates', [
                'certificate_include_english_version' => 1
            ], "user_id = $order[user_id]");
            update('orders', [
                'course_id' => 2
            ], "user_id = $order[user_id] and course_id = 1");

        }
    }

}