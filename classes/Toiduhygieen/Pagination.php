<?php namespace Toiduhygieen;

use Exception;

/**
 * Helper class for calculating parameters related to table pagination (number of pages etc)
 * @package Toiduhygieen
 */
class Pagination
{
    public $perPage = 500;
    public $numberOfPages;
    public $pageNumber;
    public $previousPageNumber;
    public $nextPageNumber;
    public $second_last;
    public $offset;
    public $q;
    public $totalRecords;
    public $criteria;
    private array $search_fields;

    /**
     * Pagination constructor.
     * @param $search_fields array Fields that can contain the search keywords
     */
    function __construct($search_fields)
    {
        $this->search_fields = $search_fields;
        $this->q = empty($_GET['q']) ? null : trim($_GET['q']);
        $this->criteria = $this->getCriteria($this->q, $search_fields);
        $this->pageNumber = empty($_GET['page']) ? 1 : $_GET['page'];
        $this->offset = ($this->pageNumber - 1) * $this->perPage;
        $this->nextPageNumber = $this->pageNumber + 1;
        $this->previousPageNumber = $this->pageNumber - 1;
    }

    /**
     * Sets total records and all other fields which are derived from total records
     * @param $total_records
     * @throws Exception
     */
    function setTotal($total_records){
        if (!is_numeric($total_records)) {
            throw new Exception('Invalid total_records');
        }
        $this->totalRecords = $total_records;
        $this->numberOfPages = ceil($this->totalRecords / $this->perPage);
        $this->second_last = $this->numberOfPages - 1;

    }
    /**
     * Generates an array of condition strings for SQL::getWhere()
     * @param $keywords string List of keywords separated by spaces
     * @param $search_fields array Array of fields of a table to look the keywords from
     * @return array ["f1 LIKE '%word1'", "f2 LIKE '%word1%'", "f1 LIKE '%word2%'", "f2 LIKE '%word2%'"]
     */
    private function getCriteria($keywords, $search_fields)
    {
        $result = [];
        if (empty($keywords) || !is_string($keywords)) {
            return [];
        }
        $keywords = explode(' ', $keywords);
        foreach ($keywords as $keyword) {
            $criteria = [];
            $keyword = addslashes($keyword);
            foreach ($search_fields as $field) {
                $criteria[] = "$field LIKE '%$keyword%'";
            }
            $result[] = "(" . implode(" OR ", $criteria) . ")";
        }
        return $result;
    }
}