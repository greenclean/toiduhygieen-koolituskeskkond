<?php


namespace Toiduhygieen;


class Test extends Model
{

    public static function regenerate($test_id, $language_id)
    {
        validate($test_id);

        $order_id = get_one("SELECT order_id FROM tests WHERE test_id = $test_id");

        validate($order_id);

        // Delete old test
        q("DELETE FROM question_set WHERE test_id = $test_id");
        q("DELETE FROM tests WHERE test_id = $test_id");

        // Create new test and return its test_id
        return self::generate($order_id, $language_id);
    }

    public static function generate($order_id, $language_id)
    {
        // Retrieve order data
        $order = get_first("SELECT * FROM orders JOIN users USING(user_id) JOIN courses USING (course_id) WHERE course_type != 'certificate' AND order_id=$order_id");

        validate($order, IS_ARRAY);
        validate($language_id, IS_ID);
        validate($order['order_id'], IS_ID);

        $questions_count = $order['questions_count'] ? $order['questions_count'] : QUESTION_AMOUNT;

        // Insert new test
        $test['test_id'] = insert("tests", [
            'order_id' => $order['order_id'],
            'user_id' => $order['user_id'],
            'questions_count' => $questions_count
        ]);

        // Get categories
        $test_categories = get_all("SELECT test_category_id FROM test_categories");
        foreach ($test_categories as $test_category) {
            $test_category_ids[] = $test_category['test_category_id'];
        }

        for ($i = 1; $i <= $questions_count; $i++) {

            // Randomise category_id order
            shuffle($test_category_ids);

            foreach ($test_category_ids as $category_id) {
                // Filter out already selected questions
                $and_question_id_not_in = empty($selected_question_ids)
                      ? "" : "AND question_id NOT IN (" . implode(',', $selected_question_ids) . ")";

                // Get one random question from current category
                $question_id = get_one("SELECT question_id FROM questions 
                     WHERE course_id= '" . $order['course_id'] . "' 
                     AND test_category_id=$category_id
                     $and_question_id_not_in
                     ORDER BY RAND()
                    ");

                // Switch to another category if there weren't any questions left in the current category
                if(!$question_id){
                    continue;
                }

                $selected_question_ids[] = $question_id;

                // Break foreach when QUESTION_AMOUNT has been reached
                if (count($selected_question_ids) >= $questions_count) {
                    break 2;
                }
            }
        }

        // Generate test question set
        foreach ($selected_question_ids as $question_id) {
            $test['question_id'] = $question_id;
            insert('question_set', $test);
        }

        // Return generated test's test_id
        return $test['test_id'];
    }

    public static function get($criteria = null): array
    {

        $tests = [];
        $where = SQL::getWhere($criteria, "test_id");
        $rows = get_all("
            SELECT * FROM tests 
            LEFT JOIN orders USING (order_id) 
            LEFT JOIN question_set USING (test_id)
            LEFT JOIN courses on orders.course_id = courses.course_id
            $where ORDER BY question_id");


        foreach ($rows as $key => $field) {

            $question_ids[] = $field['question_id'];


            foreach ($field as $field => $value) {
                $tests[$field] = $value;
            }
        }

        $question_ids = implode(", ", $question_ids);

        $questions = Question::get_all([
            "question_id in ($question_ids)"
        ]);

        $tests['questions'] = $questions;

        return $tests;
    }

}