<?php


namespace Toiduhygieen;


use Exception;

class Language extends Model
{

    public static $code_max_length = 3;
    public static $code_min_length = 2;
    private static array $current = [];
    private static array $codes_in_use = [];

    public static function delete($language)
    {
        if (!self::is_valid_code($language)) {
            throw new Exception('Invalid language');
        }
        q("ALTER TABLE translations DROP COLUMN translation_in_$language");
    }

    public static function add($language)
    {
        if (!self::is_valid_code($language)) {
            throw new Exception('Invalid language');
        }

        q("ALTER TABLE translations ADD COLUMN translation_in_$language varchar(765) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin");

    }


    public static function is_valid_code($language_code): bool
    {
        return !(empty($language_code)
            || strlen($language_code) > self::$code_max_length
            || strlen($language_code) < self::$code_min_length);
    }

    public static function get_current()
    {
        // Load from DB if cache is empty
        if (empty(Language::$current)) {

            // Check URL first
            if (!empty($_GET['language_id']) && $_GET['language_id'] > 0) {
                return Language::$current = Language::get($_GET['language_id']);
            }

            if (!empty($_SESSION['user_id'])) {
                return Language::$current = Language::get(["language_id = ".User::get($_SESSION['user_id'])[0]['language_id']]);
            }

            // Check session
            if (!empty($_SESSION['language_id']) && $_SESSION['language_id'] > 0) {
                $session_language = Language::get(["language_id = '{$_SESSION['language_id']}'"]);

                // When $_SESSION['language_id'] is supported
                if (!empty($session_language) && in_array($session_language['language_code'], Language::get_codes_in_use())) {
                    return Language::$current = $session_language;
                }
            }

            // Try browser default
            if ($browser_language = Language::get_supported_language_from_browser()) {
                return Language::$current = $browser_language;
            }

            // Check that default languge has been defined in config.php
            if (!defined('DEFAULT_LANGUAGE')) {
                error_out('DEFAULT_LANGUAGE is not defined');
            }

            // Try DEFAULT_LANGUAGE
            if ($default_language = Language::get("language_code = '" . DEFAULT_LANGUAGE . "'")) {
                return Language::$current = $default_language;
            }

        }

        return Language::$current;
    }

    private static function get_supported_language_from_browser(): array
    {
        $codes_in_use = Language::get_codes_in_use();

        // Parse browser supplied language list and pick the first supported language
        foreach (preg_split('/[;,]/', $_SERVER['HTTP_ACCEPT_LANGUAGE']) as $code) {
            if (substr($code, 0, 2) == 'q=') continue;
            if (strpos($code, '-') !== false) $code = explode('-', $code)[0];
            if (in_array(strtolower($code), $codes_in_use)) return Language::get(["language_code='$code'"]);
        }

        return [];
    }

    public static function get_all_in_use(): array
    {
        return Language::get_all(['language_code IN (' . Language::get_codes_in_use(true) . ')'],null,"language_code");
    }

    public static function get_codes_in_use($for_sql = false)
    {

        // Load from DB if cache is empty
        if (empty(self::$codes_in_use)) {

            $codes = [];

            foreach (get_all("SHOW COLUMNS FROM translations LIKE 'translation_in_%'") as $row) {
                $codes[] = substr($row['Field'], 15);
            }

            // Cache them
            self::$codes_in_use = $codes;
        }

        if ($for_sql) {
            return "'" . implode("','", self::$codes_in_use) . "'";
        }

        return self::$codes_in_use;
    }

    public static function get_all_not_in_use(): array
    {
        return Language::get_all('language_code not in(' . Language::get_codes_in_use(true) . ')', "language_name", "language_code");
    }


}