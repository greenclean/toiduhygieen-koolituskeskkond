<?php


namespace Toiduhygieen;


class User
{

    /**
     * Returns an array of users
     * @param $offset
     * @param $count
     * @param $criteria
     * @return mixed
     */
    public static function get($criteria, $offset = 0, $count = 1)
    {
        $where = SQL::getWhere($criteria, "users.user_id");
        $limit = SQL::getLimit($offset, $count);
        return get_all("
            SELECT users.*,
                teamLeader.name as team_leader_name,
                GROUP_CONCAT(DISTINCT order_id ORDER BY order_id SEPARATOR ', ') order_ids,
                GROUP_CONCAT(DISTINCT course_name ORDER BY course_name SEPARATOR ', ') course_names,
                COUNT(subordinates.user_id) worker_count,
                (SELECT COUNT(*) FROM
                (SELECT users.user_id
                 FROM users
                 LEFT JOIN users teamLeader ON teamLeader.user_id = users.employer_id
                 LEFT JOIN users subordinates ON subordinates.employer_id = users.user_id
                 LEFT JOIN orders ON orders.user_id = users.user_id
                 LEFT JOIN courses ON courses.course_id = orders.course_id $where 
                 GROUP BY users.user_id) as foo ) user_count
            FROM users
            LEFT JOIN users teamLeader ON teamLeader.user_id = users.employer_id
            LEFT JOIN users subordinates ON subordinates.employer_id = users.user_id 
            LEFT JOIN orders ON orders.user_id = users.user_id
            LEFT JOIN courses ON courses.course_id = orders.course_id
            $where
            GROUP BY users.user_id
            ORDER BY users.user_id DESC
            $limit");
    }

    public static function withThisPersonalCodeAndUsernameCombinationExists($name, $personal_code)
    {
        $name = addslashes($name);
        $personal_code = addslashes($personal_code);

        return !!get_first("
            SELECT users.name, users.personal_code 
            FROM users 
            WHERE name LIKE '$name' AND personal_code LIKE '$personal_code'");
    }

    public static function setWorkers($user_id, $worker_ids)
    {
        $user_id = (int)$user_id;
        $worker_ids = empty($worker_ids) || !is_array($worker_ids) ? [] : $worker_ids;

        // Release all workers of this employee
        update("users", ['employer_id' => null], "employer_id = $user_id");

        foreach ($worker_ids as $worker_id) {
            update("users", ['employer_id' => $user_id], "user_id = $worker_id");
        }
    }

    public static function withConflictingDataExists($data, $user_id = 0): bool
    {
        $user_id = (int)$user_id;

        $data = escape($data);
        if ($user_id > 0) {
            $data[] = "users.user_id != $user_id";
        }
        return !!User::get($data, 0, 1);
    }

    /**
     * @param $data
     * @param $user_id
     */
    public static function update($user_id, $data)
    {
        $user_id = (int)$user_id;
        update("users", $data, "user_id = $user_id");
    }


    public static function hasCertificates($user_id): bool
    {
        return !!get_one("SELECT count(*) FROM certificates WHERE user_id = $user_id");
    }

    public static function validate($data, $user_id = 0)
    {

        validate($data, IS_ARRAY, 'data');
        validate($data['personal_code'], IS_NON_EMPTY, __('personal code'));
        validate($data['name'], IS_NON_EMPTY, __('name'));

        if (!isValidPersonalCodeOrBirthdate($data['personal_code'])) {
            stop(400, ['title' => __('Error'), 'description' => __('Personal code / date of birth does not match format')]);
        }

        if (personalCodeIsPersonalCode($data['personal_code'])) {
            if (User::withConflictingDataExists(['users.personal_code' => $data['personal_code']], $user_id)) {
                stop(400, ['title' => __('Error'), 'description' => __('A user with this personal code already exists')]);
            }
        }

        if (User::withConflictingDataExists([
            'users.personal_code' => $data['personal_code'],
            'users.name' => $data['name']
        ], $user_id)) {
            stop(400, ['title' => __('Error'), 'description' => __('Name + personal code already exists')]);
        }
    }
}