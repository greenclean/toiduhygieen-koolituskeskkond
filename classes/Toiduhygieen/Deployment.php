<?php


namespace Toiduhygieen;


class Deployment
{

    public static function create()
    {
        // Extract commit meta data
        $output = trim(exec('git log --oneline --format=%h:::%cn:::%ci:::%s -n1 HEAD'));
        $parts = explode(':::', $output);

        if (count($parts) < 4) {
            // Handle error, perhaps log it somewhere
            return;
        }

        list($sha, $author, $commit_date, $message) = $parts;

        // Insert new deployment to database
        insert('deployments',[
            'deployment_commit_date'=>substr($commit_date, 0, 19),
            'deployment_date'=>date('Y-m-d H:i:s'),
            'deployment_commit_message'=>$message,
            'deployment_commit_sha'=>$sha,
            'deployment_commit_author'=>$author
        ], true);
    }

}