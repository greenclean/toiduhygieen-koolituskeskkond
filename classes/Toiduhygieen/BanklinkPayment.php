<?php
/**
 * Created by PhpStorm.
 * User: Gerli
 * Date: 20/02/2019
 * Time: 10:44
 */

namespace Toiduhygieen;


class BanklinkPayment
{
    static function get($course_id, $user_id, $banklink_payment_id = null)
    {
        $function = $user_id ? "get_first" : "get_all";
        $and = $banklink_payment_id ? " AND banklink_payment_id = $banklink_payment_id" : "";

        $banklink_payment = $function("SELECT *,
			                          order_price as banklink_payment_total_price
                                      FROM banklink_payments
                                      WHERE course_id = $course_id AND user_id = $user_id $and
                                      ORDER BY banklink_payment_id DESC
        ");

        return $banklink_payment;

    }

    static function get_id($banklink_payment_id)
    {

        $banklink_payment = get_first("SELECT *,
			                          order_price as banklink_payment_total_price
                                      FROM banklink_payments
                                             LEFT JOIN courses USING (course_id)
                                             LEFT JOIN payment_methods USING (payment_method_id)
                                      WHERE banklink_payment_id = $banklink_payment_id
                                      ORDER BY banklink_payment_id DESC
        ");

        return $banklink_payment;
    }

    static function create($user_id, $order_date, $course_id, $payment_method_id, $order_status_id, $order_price, $order_price_vat, $company_name = '')
    {
        $banklink_payment_data['user_id'] = $user_id;
        $banklink_payment_data['banklink_payment_date'] = $order_date;
        $banklink_payment_data['course_id'] = $course_id;
        $banklink_payment_data['payment_method_id'] = $payment_method_id;
        $banklink_payment_data['order_status_id'] = $order_status_id;
        $banklink_payment_data['order_price'] = $order_price;
        $banklink_payment_data['order_price_vat'] = $order_price_vat;
        $banklink_payment_data['company_name'] = $company_name;

        insert('banklink_payments', $banklink_payment_data);

    }

    static function create_worker($banklink_payment_id, $user_id)
    {
        insert('banklink_payment_workers', [
            'banklink_payment_id' => $banklink_payment_id,
            'user_id' => $user_id
        ]);
    }

    static function get_workers($banklink_payment_id)
    {
        // Avoid double user_id
        return get_all("SELECT banklink_payment_id, banklink_payment_date, company_name, course_id, payment_method_id, order_status_id,
                                order_price, order_price_vat, banklink_payment_workers.user_id, users.email, users.name, users.language_id, users.personal_code
                                FROM banklink_payments
                                   LEFT JOIN banklink_payment_workers USING (banklink_payment_id)
                                    INNER JOIN users ON users.user_id = banklink_payment_workers.user_id
                                WHERE banklink_payment_id = $banklink_payment_id
                                ORDER BY banklink_payment_id DESC
                                
        ");

    }

    static function update_order_status_id($banklink_payment_id)
    {
        update('banklink_payments', ['order_status_id' => 1], 'banklink_payment_id =' . $banklink_payment_id);

    }
}