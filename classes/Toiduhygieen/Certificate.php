<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 17/08/2017
 * Time: 15:20
 */

namespace Toiduhygieen;


use Transliterator;

class Certificate
{

    static function create($test_id, $user_id)
    {

        $user = get_first("SELECT is_admin, employer_id FROM users WHERE user_id = $user_id");

        // Find out if user has employees and is therefore an employer
        $employees = get_all("SELECT * FROM users WHERE employer_id = $user_id AND user_id != $user_id");

        if ($user["is_admin"] == 1) {
            $and = "";

            // User is an employer
        } else if ($employees && $user["employer_id"] === $user_id) {
            $and = "AND employer_id = $user_id";

            // User is an employer who also is an employee of another user
        } else if ($employees && $user["employer_id"] !== $user_id) {

            // Is user downloading their own or someone else's certificate
            $test_owner_id = get_one("SELECT user_id FROM tests WHERE test_id = $test_id");

            if ($test_owner_id === $user_id) {
                $and = "AND certificates.user_id = $user_id";
            } else {
                $and = "AND employer_id = $user_id";
            }

        } else {
            $and = "AND certificates.user_id = $user_id";
        }

        $order = get_first("SELECT *
                                FROM certificates
                                   LEFT JOIN users USING (user_id)
                                   LEFT JOIN tests USING (test_id)
                                   LEFT JOIN orders USING (order_id)
                                WHERE certificates.test_id = $test_id 
                                  $and
                                  AND order_status_id = 1
                                ORDER BY course_id DESC");

        if (!$order) {
            $errors[] = __('Certificate does not exist!');
            require 'templates/error_template.php';
            exit();

        } elseif ($order && $user["is_admin"]) {
            $name = $order['name'];
            $personal_code = $order['personal_code'];
        } else {
            $name = $order['name'];
            $personal_code = $order['personal_code'];

        }

        $date = strtotime($order['certificate_date']);
        $day = date("j", ($date));
        $dateEng = $day . self::getOrdinalSuffix($day) . " of " . date("F Y", $date);

        $months[1] = 'jaanuaril';
        $months[2] = 'veebruaril';
        $months[3] = 'märtsil';
        $months[4] = 'aprillil';
        $months[5] = 'mail';
        $months[6] = 'juunil';
        $months[7] = 'juulil';
        $months[8] = 'augustil';
        $months[9] = 'septembril';
        $months[10] = 'oktoobril';
        $months[11] = 'novembril';
        $months[12] = 'detsembril';

        $files = [];
        $day = date("n", ($date));
        $dateEst = date("j", $date) . "." . $months[$day] . " " . date("Y", $date);

        if($order['questions_count']) {
            $questions_count = $order['questions_count'];
        } else {
            $questions_count = QUESTION_AMOUNT;
        }

        if ($order['course_id'] == 2 || $order['course_id'] == 3 || $order['certificate_include_english_version']) {
            // English and estonian
            $files = array([
                'file' => 'assets/img/Tunnistus.pdf',
                'person_id' => "Isikukood: " . $personal_code,
                'testScore' => "(" . $order["test_score_count"] . " punktiga ".$questions_count."st)",
                'date' => "Välja antud: Tallinnas " . $dateEst
            ], [
                'file' => 'assets/img/Certificate.pdf',
                'person_id' => "Identification no.: " . $personal_code,
                'testScore' => "(" . $order["test_score_count"] . " points out of ".$questions_count.")",
                'date' => "Issued:Tallinn, Estonia, " . $dateEng
            ]);
        } else {
            // Only estonian
            $files = array([
                'file' => 'assets/img/Tunnistus.pdf',
                'person_id' => "Isikukood: " . $personal_code,
                'testScore' => "(" . $order["test_score_count"] . " punktiga ".$questions_count."st)",
                'date' => "Välja antud: Tallinnas " . $dateEst
            ]);

        }

        require 'classes/fpdf/fpdf.php';
        require 'classes/FPDI/fpdi.php';
        $pdf = new \FPDI();


        // add a page
        foreach ($files as $file) {

            $pdf->AddPage();

            // set the source file
            $pdf->setSourceFile($file['file']);

            // import page 1
            $tplIdx = $pdf->importPage(1);

            // use the imported page and place it at point 10,10 with a width of 100 mm
            $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

            // get name of the logged in person
            $pdf->setfont('Arial', '', 36);
            $personName = Transliteration::symbolsToLatin($name);
            $personName = mb_strtoupper($personName);
            $pageWidth = $pdf->GetPageWidth() / 2;
            $stringLength = $pdf->getstringwidth("$personName") / 2;
            $margin = $pageWidth - $stringLength;

            $pdf->SetXY(12, 80);
            CellFit($pdf,186, $h=0, $personName, 0, 0, 'C', false, '', true,false);

            // get personal identification number
            $pdf->setfont('Arial', '', 21);
            $personID = $file['person_id'];
            $stringLength2 = $pdf->getstringwidth("$personID") / 2;
            $margin2 = $pageWidth - $stringLength2;

            $pdf->SetXY($margin2, 95);
            $pdf->Write(0, "$personID");

            // get score count of the test
            $testScore = $file['testScore'];
            $pdf->setfont('Arial', 'B', 18);
            $stringLength3 = $pdf->getstringwidth($testScore) / 2;
            $margin3 = $pageWidth - $stringLength3;

            $pdf->SetXY($margin3, 124);
            $pdf->Write(0, $testScore);

            // get certificate number
            $sert = "SRTFK " . $order['certificate_id'];
            $pdf->setfont('Arial', 'B', 18);
            $stringLength3 = $pdf->getstringwidth($sert) / 2;
            $margin3 = $pageWidth - $stringLength3;

            $pdf->SetXY($margin3, 150);
            $pdf->Write(0, $sert);

            // get date of the certificate created
            $date = $file['date'];
            $pdf->setfont('Arial', '', 18);
            $stringLength4 = $pdf->getstringwidth($date) / 2;
            $margin4 = $pageWidth - $stringLength4;

            $pdf->SetXY($margin4, 160);
            $pdf->Write(0, $date);

        }

        return $pdf;

    }

    static function getOrdinalSuffix($number)
    {
        $number = abs($number);
        $lastChar = substr($number, -1, 1);
        switch ($lastChar) {
            case '1' :
                return ($number == '11') ? 'th' : 'st';
            case '2' :
                return ($number == '12') ? 'th' : 'nd';
            case '3' :
                return ($number == '13') ? 'th' : 'rd';
        }
        return 'th';
    }

    /**
     * @param $user_id
     * @return mixed|null
     */
    public static function count($user_id)
    {
        $user_id = (int)$user_id;
        return get_one("SELECT COUNT(*) as count FROM certificates WHERE  user_id = " . $user_id);
    }

    public static function deleteExpired()
    {
        q("DELETE FROM certificates WHERE certificate_date < (NOW() - INTERVAL 5 YEAR)");
    }

}

//Cell with horizontal scaling if text is too wide
function CellFit(&$fpdf, $w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
{
    //Get string width
    $str_width=$fpdf->GetStringWidth($txt);

    //Calculate ratio to fit cell
    if($w==0)
        $w = $fpdf->w-$fpdf->rMargin-$fpdf->x;
    $ratio = ($w-$fpdf->cMargin*2)/$str_width;

    $fit = ($ratio < 1 || ($ratio > 1 && $force));
    if ($fit)
    {
        if ($scale)
        {
            //Calculate horizontal scaling
            $horiz_scale=$ratio*100.0;
            //Set horizontal scaling
            $fpdf->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
        }
        else
        {
            //Calculate character spacing in points
            $char_space=($w-$fpdf->cMargin*2-$str_width)/max(strlen($txt)-1,1)*$fpdf->k;
            //Set character spacing
            $fpdf->_out(sprintf('BT %.2F Tc ET',$char_space));
        }
        //Override user alignment (since text will fill up cell)
        $align='';
    }

    //Pass on to Cell method
    $fpdf->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);

    //Reset character spacing/horizontal scaling
    if ($fit)
        $fpdf->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
}
