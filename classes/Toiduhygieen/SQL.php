<?php namespace Toiduhygieen;


class SQL
{

    static function getWhere($criteria, $id_field = null)
    {
        $where = '';
        if (!empty($criteria)) {
            if (is_array($criteria)) {
                $where = "WHERE " . implode(' AND ', $criteria);
            } else if (is_numeric($criteria)) {
                $where = "WHERE $id_field = $criteria";
            } else {
                $where = "WHERE $criteria";
            }
        }

        return $where;
    }

    public static function getLimit($offset, $count)
    {
        return is_numeric($offset) && $count > 0 ? "LIMIT $offset, $count" : '';
    }

    public static function generate_values_for_insert($values)
    {
        foreach ($values as $key => $value) {
            $values[$key] = "'".addslashes($value)."'";
        }

        return '(' . implode('),(', $values) . ')';
    }

    public static function generate_values_for_in($values)
    {
        foreach ($values as $key => $value) {
            $values[$key] = addslashes($value);
            $values[$key] = "'$value'";
        }

        return implode(', ', $values);
    }

}
