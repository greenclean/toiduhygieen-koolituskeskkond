<?php
/**
 * Created by PhpStorm.
 * User: Gerli
 * Date: 08/03/2019
 * Time: 09:41
 */

namespace Toiduhygieen;


use CodexWorld\VerifyEmail;

class Email
{
    static function get_content($language_id, $email_type_id)
    {
        return $email_content = get_first("SELECT * FROM email_texts
                                                    LEFT JOIN email_type USING (email_type_id)
                                                 WHERE email_type_id = $email_type_id 
                                                 AND language_id = $language_id");

    }

    public static function isReal($email)
    {
        $email_validator = new VerifyEmail();
        $email_validator->setStreamTimeoutWait(20);
        $email_validator->Debug = false;
        $email_validator->Debugoutput = 'log';

        $email_validator->setEmailFrom(SMTP_FROM);

        return $email_validator->check($email) ? true : false;
    }

    public static function checkEmail($email)
    {
        ob_start();
        $email_validator = new VerifyEmail();
        $email_validator->setStreamTimeoutWait(20);
        $email_validator->Debug = true;
        $email_validator->Debugoutput = 'html';
        $email_validator->setEmailFrom(SMTP_FROM);
        return [
            'result' => $email_validator->check($email),
            'details' => Email::colorSmtpOutput(ob_get_clean())
        ];
    }

    public static function colorSmtpOutput($content)
    {
        $content = preg_replace('/^(2[0-9]{2}.*[^\n])/mi', "<span style='color: green'>$1</span>", $content);
        return preg_replace('/^([4-5][0-9]{2}.*[^\n])/mi', "<span style='color: red'>$1</span>", $content);
    }
}