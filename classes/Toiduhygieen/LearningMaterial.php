<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 2019-04-08
 * Time: 13:41
 */

namespace Toiduhygieen;


class LearningMaterial
{
    public static function get($criteria = null, $include_content = false)
    {
        $function = 'get_all';

        // Do not include content field by default
        $select = $include_content ? '*' : '
            learning_material_id,
            learning_material_name,
            learning_material_date_added,
            learning_material_size,
            language_id';

        $where = '';

        if (is_array($criteria)) {
            $where = 'WHERE ' . implode('AND', escape($criteria));
            $function = 'get_all';
        }
        if (is_numeric($criteria)) {
            $where = "WHERE learning_material_id = $criteria";
            $function = 'get_first';
        }

        return $function("
             SELECT $select
             FROM learning_materials $where");
    }
}