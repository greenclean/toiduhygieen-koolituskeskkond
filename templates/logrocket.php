<?php global $app; ?>

<?php if (!empty($_SERVER['HTTP_HOST']) && defined('LOGROCKET_INIT')): ?>

    <?php
    $_SESSION['session_page_number'] = empty($_SESSION['session_page_number']) ? 1 : ++$_SESSION['session_page_number'];
    $_SESSION['session_page_generation_time'] = date('Y-m-d H:i:s');
    $_SESSION['session_page_generation_time_first'] = empty($_SESSION['session_page_generation_time_first']) ? date('Y-m-d H:i:s') : $_SESSION['session_page_generation_time_first'];
    $_SESSION['session_page_generation_time_previous'] = empty($_SESSION['session_page_generation_time_previous']) ? -1 : $_SESSION['session_page_generation_time_previous'];
    $_SESSION['session_page_generation_time_first_ago'] = prettyDate($_SESSION['session_page_generation_time_first']);
    $_SESSION['session_page_generation_time_previous_ago'] = prettyDate($_SESSION['session_page_generation_time_previous']);
    $baseRef = $_SERVER['HTTP_HOST'] == 'localhost' ? 'http://projects.diarainfra.com/toiduh/' : BASE_URL
    ?>

    <script src="https://cdn.lr-ingest.io/LogRocket.min.js" crossorigin="anonymous"></script>
    <script>window.LogRocket && window.LogRocket.init('<?=LOGROCKET_INIT?>', {
            release: '<?= addcslashes(PROJECT_VERSION, "'")?>',
            dom: {
                baseHref: '<?= $baseRef // URL for CSS files (localhosts are inaccessible for LogRocket)?>'
            }
        });</script>

    <script>

        <?php if(!empty($app->auth->name)):?>
        LogRocket.identify('<?=$_SESSION['user_id']?>', {
            name: '<?= addcslashes($app->auth->name, "'")?>',
            email: '<?= addcslashes($app->auth->email, "'")?>',
            personal_code: '<?= addcslashes($app->auth->personal_code, "'")?>'
        });
        <?php endif ?>

        LogRocket.log('session', {
            session_id: '<?= addcslashes(session_id(), "'")?>',
            session_page_number: '<?= addcslashes($_SESSION['session_page_number'], "'")?>',
            session_page_generation_time: '<?= addcslashes($_SESSION['session_page_generation_time'], "'")?>',
            session_page_generation_time_previous: '<?= addcslashes("$_SESSION[session_page_generation_time_previous] ($_SESSION[session_page_generation_time_previous_ago])", "'")?>',
            session_page_generation_time_first: '<?= addcslashes("$_SESSION[session_page_generation_time_first] ($_SESSION[session_page_generation_time_first_ago])", "'")?>'
        });
    </script>

    <?php // Set previous to current
    $_SESSION['session_page_generation_time_previous'] = $_SESSION['session_page_generation_time'];
    ?>

<?php endif ?>