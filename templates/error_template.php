<?php if (!function_exists('__')) {
    function __($msg)
    {
        echo $msg;
    }
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="UTF-8">
    <title>Toiduhügieen.ee koolituskeskkond</title>
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css?<?= PROJECT_VERSION ?>">
    <link rel="stylesheet" href="assets/themes/canvas/css/style.css?<?= PROJECT_VERSION ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <?php require dirname(__DIR__).'/templates/_smartsupp.php' ?>
    <?php require dirname(__DIR__).'/templates/logrocket.php' ?>
    <?php require dirname(__DIR__).'/templates/_google_analytics.php' ?>
</head>
<body>

<div class="starter-template">
    <h1><?= 'Learning platform' ?></h1>

</div>


<div class="container">
    <br/>
    <br/>
    <?php if (isset($errors)): ?>
        <?php foreach ($errors as $error): ?>
            <div class="alert alert-danger">
                <?= $error ?>
            </div>
        <?php endforeach; ?>
    <?php elseif (isset($error_file_name_or_msg)): ?>
        <?php require 'views/errors/' . $error_file_name_or_msg . '_error_view.php' ?>
    <?php else: ?>
        Tundmatu viga!
    <?php endif; ?>

</div>
<!-- /container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<div id="footer">
    <div class="container">
        <div class="row">COPYRIGHT © 2015-<?=date('Y')?> GREENCLEAN OÜ<br><br>
            <img src="assets/img/mac.png" width="22" class="mac"><a href="#" class="rs-link" data-link-desktop="VAATA TÄISVERSIOONI" data-link-responsive="VAATA MOBIILIVERSIOONI"></a></div>
    </div>
</div>
<script src="vendor/components/jquery/jquery.min.js"></script>
<script src="vendor/components/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
