<?php

use Toiduhygieen\Language;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="UTF-8">
    <title>Toiduhügieen.ee koolituskeskkond</title>

    <link rel="icon" href="https://i.imgur.com/qKrjgui.png" sizes="any" type="image/svg+xml">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content=""/>

    <!-- Stylesheets
    ============================================= -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css?<?= PROJECT_VERSION ?>">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap<?= PROJECT_VERSION ?>"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="assets/css/custom.css?<?= PROJECT_VERSION ?>" type="text/css"/>
    <link rel="stylesheet" href="assets/themes/canvas/css/style.css?<?= PROJECT_VERSION ?>" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <?php require 'templates/_smartsupp.php' ?>
    <?php require 'templates/logrocket.php' ?>
    <?php require '_google_analytics.php' ?>
</head>
<body class="stretched" style="overflow-x: hidden !important;">

<?php require "_menu.php" ?>

<section id="slider" class="slider-element slider-parallax swiper_wrapper min-vh-60 min-vh-md-75 include-header"
         style="margin-top: 0 !important;">
    <div class="slider-inner">

        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <div class="swiper-slide dark">
                    <div class="container">
                        <div class="slider-caption slider-caption-center">
                            <h2 data-animate="fadeInUp"><?= __('Learning platform') ?></h2>
                            <p class="d-block" data-animate="fadeInUp" data-delay="100">
                                <?= __('Your virtual food hygiene trainer. Here you will study, pass the exam and receive a certificate. Employers can manage employees here. The learning platform is for those who come into contact with food, drink.') ?></p>
                        </div>
                    </div>
                    <div class="swiper-slide-bg"
                         style="background-image: url('https://www.toiduhügieen.ee/wp-content/themes/toiduhugieen/img/slide1.jpg');"></div>
                </div>
            </div>
            <!--                <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>-->
            <!--                <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>-->
        </div>

    </div>
</section>

<!-- Content
============================================= -->
<section id="content">


    <div class="row clearfix align-items-stretch">

        <div class="col-xl-6 center col-padding" style="background: #eee;">
            <div class="heading-block border-bottom-0">
                <span class="before-heading color"><?= __('Video about us') ?></span>
                <h3><?= __('Toiduhügieen') ?></h3>
            </div>
            <div class="center bottommargin">
                <video style="width:100%;height:auto;max-height: 100%" controls
                       src="https://www.toiduhügieen.ee/kool/uploads/video.m4v"></video>
            </div>
            <p class="lead mb-0"><?= __('Toiduhügieen offers...') ?></p>
        </div>

        <div class="col-xl-6 center col-padding" style="background: #f5f5f5;">
            <div class="custom-login-padding">
                <div class="heading-block border-bottom-0">
                    <h3><?= __('Log in') ?></h3>
                </div>
                <div class="row col-mb-0">
                    <div class="col-12 login-element">
                        <p><?= __('Log in with existing username and password, to view learning materials and solve tests.') ?></p>
                    </div>

                    <div class="col-12 login-element">
                        <div class="login-container">
                            <form method="post">
                                <label for="txtLoginEmail" style="display: none"><?= __('Name') ?></label>
                                <input placeholder="<?= __('First and last name') ?>" type="text" class="form-control"
                                       id="txtLoginEmail"
                                       name="email_or_name"
                                       style="margin-bottom: 10px; width: 100%">
                                <label for="txtLoginPassword"
                                       style="display: none"><?= __('Password or Personal code') ?></label>
                                <input placeholder="<?= __('Password or personal code') ?>" type="password"
                                       class="form-control"
                                       name="password_or_id"
                                       id="txtLoginPassword"
                                       style="margin-bottom: 10px; width: 100%">
                                <input style="width: 100%" type="submit" class="btn btn-success"
                                       value="<?= __('Log in') ?>" id="btnLogin">
                            </form>
                            <p><?= __('Forgot your password?') ?> <span><a id="aRecoverPassword"
                                                                           href="registration/new_password">
                                    <?= __('Recover your password to e-mail.') ?></a></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--					<div class="col-xl-6 center col-padding" style="background: url('images/services/main-bg.jpg') center center no-repeat; background-size: cover;">-->
        <!--						test-->
        <!--					</div>-->

        <div class="col-xl-6 center col-padding" style="background: #f5f5f5;">
            <div class="heading-block border-bottom-0">
                <h3><?= __('Frequently asked questions') ?></h3>
            </div>
            <p><?= __('Question') ?></p>
            <p><?= __('Answer') ?></p>
            <p><?= __('Question') ?></p>
            <p><?= __('Answer') ?></p>
            <p><?= __('Question') ?></p>
            <p><?= __('Answer') ?></p>
            <form method="get" action="#">
                <button type="submit" class="btn-success btn"><?= __('Read more') ?></button>
            </form>
        </div>

        <div class="col-xl-6 center col-padding" style="background: #eee;">
            <div class="heading-block border-bottom-0">
                <span class="before-heading color"><?= __('Registering is free and easy') ?></span>
                <h3><?= __('Register') ?></h3>
            </div>
            <div class="row col-mb-0">
                <div class="col-md-5 col-12">
                    <p style="margin-bottom: 5px"><?= __('When you register as a user, you will have different options to use Toiduhügieen and it\'s features . By registering on our site you can access learning materials, tests and courses . After registering, log in to your account with your name and password, which will be sent to the e - mail address entered.') ?> </p>
                    <p>
                        <span>*</span><?= __('If you do not have a personal identification code, enter the date of birth (YYYY-MM-DD)') ?>
                    </p>
                </div>


                <div class="col-md-7 col-12">
                    <form method="post">
                        <div style="display: grid; grid-template-columns: 32% auto; margin-bottom: 10px;">
                            <label style="padding-top: 8px; text-align: left;"
                                   for="register-name"><?= __('First & last name') ?></label>
                            <input id="register-name txtRegisterFirstNameLastName"
                                   class="nimi validate form-control"
                                   name="nimi"
                                   placeholder="<?= __('First & last name') ?>"
                                   type="text">
                        </div>
                        <div style="display: grid; grid-template-columns: 32% auto;">
                            <label style="padding-top: 8px; text-align: left;"
                                   for="register-personal-code"><?= __('Personal code') ?></label>
                            <input class="isikukood validate form-control" id="txtRegisterID register-personal-code"
                                   name="isikukood"
                                   placeholder="<?= __('Personal code') ?>" type="text">
                        </div>
                        <div style=" margin-bottom: 10px; text-align: right;">
                            <small><span>*</span><?= __('If you do not have a personal identification code, enter the date of birth (YYYY-MM-DD)') ?>
                            </small></div>
                        <div style="display: grid; grid-template-columns: 32% auto; margin-bottom: 10px;">
                            <label style="padding-top: 8px; text-align: left;"
                                   for="register-language"><?= __('Language') ?></label>
                            <select name="suhtluskeel" class="suhtluskeel form-control" id="txtRegisterLang register-language">
                                <option>-<?= __('Choose language') ?>-</option>
                                <?php foreach (Language::get_all_in_use() as $language): ?>
                                    <option value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div style="display: grid; grid-template-columns: 32% auto; margin-bottom: 10px;">
                            <label style="padding-top: 8px; text-align: left;"
                                   for="register-email"><?= __('E-mail') ?></label>
                            <input name="epost" id="txtRegisterEmail register-email" class="epost form-control"
                                   placeholder="example@email.com"
                                   type="text">
                        </div>

                        <div class="col-12" style="float: right; padding: 0;">
                            <input style="width: 100%" type="submit" name="create" id="btnRegister"
                                   class="btn btn-success" value="<?= __('Sign up') ?>">
                            <div><small><span>*</span><?= __('The password will be sent to your email address') ?></small></div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>

</section><!-- #content end -->

<!-- Footer
============================================= -->
<footer id="footer">
    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap">

            <div class="row col-mb-50">
                <div class="col-xl-3 col-lg-4" id="kontakt">

                    <div class="row col-mb-50">
                        <div class="col-md-11">

                            <div class="widget clearfix">

                                <img src="https://www.toiduhügieen.ee/wp-content/themes/toiduhugieen/img/logo.png"
                                     alt="Logo" class="footer-logo">

                                <h3><strong><?= __('Contact') ?></strong></h3>

                                <div>
                                    <address>
                                        <strong><?= __('Headquarters') ?>:</strong><br>
                                        Viimsi Äritare Paadi tee 3 Viimsi<br>
                                        Harjumaa 74001 (3.korrus, ruum nr 326)<br>
                                    </address>
                                    <abbr><strong><?= __('Tel') ?>.</strong></abbr> <a href="tel:+3726700184">(+372)
                                        6700 184</a><br>
                                    <abbr><strong><?= __('Tel') ?>.</strong></abbr> <a href="tel:+37258501235">(+372)
                                        5850 1235</a><br>
                                    <strong><?= __('Email') ?>:</strong> <a href="mailto: info@greenclean.ee">info@greenclean.ee</a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xl-5 col-lg-3">

                    <div class="row d-lg-none d-xl-flex col-mb-50">

                        <div class="col-md-5 bottommargin-sm">
                            <div class="counter counter-small"><span data-from="50" data-to="24744"
                                                                     data-refresh-interval="30"
                                                                     data-speed="1300">3</span>+
                            </div>
                            <h5 class="mb-0"><?= __('Certificates issued') ?></h5>
                        </div>

                        <div class="col-md-5 bottommargin-sm">
                            <div class="counter counter-small"><span data-from="50" data-to="8000"
                                                                     data-refresh-interval="20"
                                                                     data-speed="1300">8000</span></div>
                            <h5 class="mb-0"><?= __('Clients') ?></h5>
                        </div>

                        <div class="col-md-2 bottommargin-sm">
                            <div class="counter counter-small"><span data-from="0" data-to="3"
                                                                     data-refresh-interval="3"
                                                                     data-speed="1000">3</span></div>
                            <h5 class="mb-0"><?= __('Countries') ?></h5>
                        </div>

                    </div>

                    <div class="d-none d-lg-block d-xl-none col-mb-50">
                        <div class="col-md-4 bottommargin-sm">
                            <div class="counter counter-small"><span data-from="50" data-to="24744"
                                                                     data-refresh-interval="30"
                                                                     data-speed="1300">3</span>+
                            </div>
                            <h5 class="mb-0"><?= __('Certificates issued') ?></h5>
                        </div>

                        <div class="col-md-4 bottommargin-sm">
                            <div class="counter counter-small"><span data-from="50" data-to="8000"
                                                                     data-refresh-interval="20"
                                                                     data-speed="1300">8000</span></div>
                            <h5 class="mb-0"><?= __('Clients') ?></h5>
                        </div>

                        <div class="col-md-4 bottommargin-sm">
                            <div class="counter counter-small"><span data-from="0" data-to="3"
                                                                     data-refresh-interval="3"
                                                                     data-speed="1000">3</span></div>
                            <h5 class="mb-0"><?= __('Countries') ?></h5>
                        </div>
                    </div>


                </div>
            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">
        <div class="container">

            <div class="row col-mb-30">

                <div class="col-md-6 text-center text-md-left">
                    COPYRIGHT &#169; 2021 GREENCLEAN OÜ<br>
                    <div class="copyright-links"><a href="#"><?= __('Privacy policy') ?></a></div>
                </div>

            </div>

        </div>
    </div><!-- #copyrights end -->
</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
class="icon-angle-up"
============================================= -->
<div id="gotoTop">^</div>

<!-- JavaScripts
============================================= -->
<script src="assets/js/Isikukood-js/isikukood.js?<?= PROJECT_VERSION ?>"></script>
<script src="assets/js/main.js"></script>
<script>
    var allUnicodeLetters = "[,\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC]";

    $('input[name="isikukood"]').on('blur', function () {

        $(this).toggleClass('has-error', !isValidIsikukoodOrBirthDate());
    });

    function isValidIsikukoodOrBirthDate() {

        var isikukoodOrBirthday = $('.isikukood').val();


        // Check that this is real ik
        var ik = new Isikukood(isikukoodOrBirthday);
        if (ik.validate()) {
            console.log('success ik');
            return true;
        }


        // Skip fake dates
        var birthDay = new Date(isikukoodOrBirthday);
        if (!(birthDay.getTime() > 0 || birthDay.getTime() < 0)) {
            //console.log('failed date');
            return false;
        }


        // Check that person is at least 15 years old
        // var todayFifteenYearsAgo = new Date();
        // todayFifteenYearsAgo.setUTCFullYear(todayFifteenYearsAgo.getUTCFullYear() - 15);
        // if (todayFifteenYearsAgo.getTime() <= birthDay.getTime()) {
        //     //console.log('too young');
        //     return false;
        // }


        // Check that person is at most 90 years old
        // var todayNinetyYearsAgo = new Date();
        // todayNinetyYearsAgo.setUTCFullYear(todayNinetyYearsAgo.getUTCFullYear() - 90);
        // if (todayNinetyYearsAgo.getTime() > birthDay.getTime()) {
        //     //console.log('too old');
        //     return false;
        // }


        // Check if date has the form of YYYY-MM-DD
        if (!isValidBirthdate(isikukoodOrBirthday)) {
            return false;
        }

        // Valid date
        return true;
    }

    function isValidEmailAddress(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // Empty string for email is not valid
        return email == '' ? false : re.test(email);
    }

    // $('input[name="epost"]').on('blur', function () {
    //     $(this).toggleClass('has-error', !isValidEmailAddress($('input.email').val()));
    // });

    // Keel validation
    function isValidLanguage() {
        return $('select.suhtluskeel').val() != '';

    }

    $('select.suhtluskeel').change(function () {
        $(this).toggleClass('has-error', !isValidLanguage());
    });

    // First- and last name validation
    function isValidName(name) {
        // Regex for full name
        var pattern = "^[a-z]*([-| ][a-z]*)?( [a-z]*)?( [a-z]*[-']?[a-z]*)?( [a-z]*[-']?[a-z]*)?( [a-z]*[-']?[a-z]*)?( [a-z]*[-']?[a-z]*)?( [a-z]*[-']?[a-z]*)$";

        // Replace [a-z] with our unicode class to support Russian etc letters and Anna-Liisas
        pattern = pattern.replace(/\[a-z\]/g, allUnicodeLetters);

        // Run the regex!
        var regEx = new RegExp(pattern, "ig"); // L{2,}L| {2,}

        // Check if regex passed
        return !(name.match(regEx) === null);

    }

    // Check if YYYY-MM-DD
    function isValidBirthdate(date) {
        var pattern = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/;
        return pattern.test(date);
    }

    $('input.nimi').on('keyup change', function () {
        $(this).siblings('.text-danger').remove();
        $(this).toggleClass('has-error', !isValidName($(this).val()));

        //if (!isValidName($(this).val())) {
        //    $(this).after('<p class="text-danger"><?//= __('Up to 6 spaces allowed') ?>//</p>');
        //}
    });

    // Convert name to proper case
    $('input.nimi').change(function () {

        // Convert to proper
        this.value = toTitleCase(this.value).trim();
    });

    $(".required").on("input", function () {
        $(this).removeClass("has-error")
    });

    $('#btnRegister').on('click', function (e) {

        //    $(".required").each(function () {
        //        $(this).removeClass("has-error");
        //    });
        //
        //    $(this).siblings('.text-danger').remove();
        //
        //    if (!(isValidIsikukoodOrBirthDate() && isValidLanguage() && isValidName($('input.nimi').val()) && isValidEmailAddress($('input.email').val()))) {
        //        $(".validate").each(function () {
        //            if ($(this).val() == '') {
        //                $(this).addClass("has-error")
        //            }
        //        });
        //
        //        if (!isValidName($('input.nimi').val())) {
        //            $('input.nimi').after('<p class="text-danger"><?//= __('Up to 6 spaces allowed') ?>//</p>');
        //        }
        //
        //        alert('<?//= addcslashes(__('Correct the boxes which are red'), "'")?>//');
        //        e.preventDefault()
        //    } else {
        e.preventDefault();
        var nimi = $('.nimi').val();
        var isikukood = $('.isikukood').val();
        var suhtluskeel = $('.suhtluskeel').val();
        var epost = $('.epost').val();
        ajax('registration/user_registration', {
            nimi: nimi,
            isikukood: isikukood,
            suhtluskeel: suhtluskeel,
            epost: epost
        }, function (res) {
            gtag('event', 'SuccessfulRegistration', {
                'event_category': 'RegistrationForm'
            });

            // Refresh the page after OK click
            alert('<?= addcslashes(__('Registration successful! Check your e-mail for the password and log in!'), "'")?>')
            window.location.reload();
        });
        //
        //    }
        });

        $("#btnLogin").on("click", function (e) {
            var required_is_empty = makeFieldsRed();

            if (required_is_empty) {
                alert('<?=addcslashes(__('Correct the boxes which are red'), "'")?>');
                e.preventDefault()
            }
        });

        function toTitleCase(str) {

            // Replace [a-z] with our unicode class to support Russian etc letters and Anna-Liisas
            var pattern = '[a-z]+';
            pattern = pattern.replace(/\[a-z\]/g, allUnicodeLetters);

            var regEx = new RegExp(pattern, "g"); // L{2,}L| {2,}

            return str.replace(regEx, function (txt) {
                console.log(txt);
                if (txt == 'de' || txt == 'von' || txt == "d" || ((txt.substr(0, 1) == 'I' && txt.length <= 3) || (txt.substr(0, 1) == 'X' && txt.length <= 3)))
                    return txt;
                else return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();


            });
        }

        function makeFieldsRed() {
            $('.text-danger').remove();

            $(".has-error").each(function () {
                $(this).removeClass("has-error");
            });

            var required_is_empty = false;

            $(".required").each(function () {
                if ($(this).val() === '') {
                    required_is_empty = true;
                    $(this).addClass("has-error");
                }
            });

            return required_is_empty
        }

</script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php require 'templates/error_modal.php'; ?>
<?php require 'system/js_constants.php' ?>

<!-- Footer Scripts
============================================= -->

</body>
</html>
