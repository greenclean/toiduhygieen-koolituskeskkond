<?php use Toiduhygieen\Language;

global $app;
$language_code = Language::get_current()['language_code'];
$logged_in_user_name = empty($app->auth->name) ? __('Unknown user') : $app->auth->name; // Error template does not have $this ?>
<!-- Header
    ============================================= -->
<style>
    .logo-image {
        width: auto;
        height: auto;
        max-width: 60% !important;
    }

     #header-wrap {
         z-index: 1;
     }
</style>
<header id="header" class="full-header">
    <div id="header-wrap">
        <div class="container">
            <div class="header-row">

                <div id="primary-menu-trigger">
                    <svg class="svg-trigger" viewBox="0 0 100 100">
                        <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                        <path d="m 30,50 h 40"></path>
                        <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                    </svg>
                </div>

                <!-- Primary Navigation
                ============================================= -->
                <nav class="primary-menu">
                    <ul class="menu-container">
                        <!-- Logo
                        ============================================= -->
                        <a href="<?= BASE_URL ?>" class="standard-logo"><img
                                    class="logo-image"
                                    src="https://www.toiduhügieen.ee/wp-content/themes/toiduhugieen/img/logo.png"
                                    alt="Toiduhügieen Logo"></a>

                        <?php if (empty($app->auth->logged_in)): ?>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('Log in') ?></div>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="menu-item">
                                <a class="menu-link" href="courses" style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('Courses') ?></div>
                                </a>
                            </li>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>workers"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('employees') ?></div>
                                </a>
                            </li>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>learn"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('materials') ?></div>
                                </a>
                            </li>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>tests"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('Tests') ?></div>
                                </a>
                            </li>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>certificates"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('Certificates') ?></div>
                                </a>
                            </li>
                        <?php endif ?>
                        <li class="menu-item">
                            <a class="menu-link" href="<?= BASE_URL ?>contact"
                               style="padding-top: 19px; padding-bottom: 19px;">
                                <div><?= __('Contact') ?></div>
                            </a>
                        </li>
                        <?php if (!empty($app->auth->is_admin) && !empty($app->auth->logged_in)): ?>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>admin"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('Admin') ?></div>
                                </a>
                            </li>
                        <?php endif ?>
                        <?php if (!empty($app->auth->logged_in)): ?>
                            <li class="menu-item">
                                <a class="menu-link" href="<?= BASE_URL ?>logout"
                                   style="padding-top: 19px; padding-bottom: 19px;">
                                    <div><?= __('Logout') ?> (<?= $logged_in_user_name ?>)</div>
                                </a>
                            </li>
                        <?php endif ?>
                        <?php if ($app->auth->logged_in): ?>
                            <li class="menu-item sub-menu">
                                <a class="menu-link"
                                   style="padding-top: 39px; padding-bottom: 39px; cursor: default;"><?= __('Languages') ?></a>
                                <ul class="sub-menu-container" style="">
                                    <?php foreach (Language::get_all_in_use() as $language): ?>
                                        <a class="menu-link" style="padding: 10px; text-align: center"
                                           href="?language_id=<?= $language['language_id'] ?>"><?= $language['language_name'] ?></a>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php endif ?>
                    </ul>

                </nav><!-- #primary-menu end -->

            </div>
        </div>
    </div>
</header><!-- #header end -->
<script src="assets/themes/canvas/js/functions.js"></script>