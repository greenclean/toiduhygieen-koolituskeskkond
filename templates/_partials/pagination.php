<?php /**
 * @var \Toiduhygieen\Pagination $pagination
 * @var string $controller
 * @var string $action
 */ ?>

<?php if ($pagination->numberOfPages) : ?>

    <strong>Page <?= "$pagination->pageNumber of $pagination->numberOfPages" ?></strong>

    <nav aria-label="Page navigation mt-5">
        <ul class="pagination justify-content-center">

            <!-- PREVIOUS BUTTON -->
            <li class="page-item <?= $pagination->pageNumber <= 1 ? 'disabled' : '' ?>">
                <a href="#"
                   onclick="location.href=updateURLParameter(
                           window.location.href, 'page',<?= $pagination->pageNumber <= 1 ? '#' : "$controller/$action?page=$pagination->previousPageNumber" ?>);return false;"
                   class="page-link">Previous</a>
            </li>

            <?php for ($i = 1; $i <= $pagination->numberOfPages; $i++): ?>
                <li class="page-item <?= $pagination->pageNumber == $i ? 'active' : '' ?>">
                    <a class="page-link" href="#"
                       onclick="location.href=updateURLParameter(window.location.href, 'page',<?= $i ?>);return false;">
                        <?= $i; ?>
                    </a>
                </li>
            <?php endfor ?>

            <!-- NEXT BUTTON -->
            <li class="page-item <?= $pagination->pageNumber >= $pagination->numberOfPages ? 'disabled' : '' ?>">
                <a href="#"
                   onclick="location.href=updateURLParameter(
                           window.location.href, 'page',<?= $pagination->pageNumber >= $pagination->numberOfPages ? '#'
                       : "$controller/$action?page=$pagination->nextPageNumber" ?>);return false;"
                   class="page-link">Next</a>
            </li>

            <!-- LAST BUTTON -->
            <?php if ($pagination->pageNumber < $pagination->numberOfPages): ?>
                <li>
                    <a href="#"
                       onclick="location.href=updateURLParameter(window.location.href, 'page', <?= $pagination->numberOfPages ?>);return false;">Last
                        &rsaquo;&rsaquo;</a>
                </li>
            <?php endif ?>

        </ul>
    </nav>
<?php endif ?>