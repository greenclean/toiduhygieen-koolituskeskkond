<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="UTF-8">
    <title>Toiduhügieen.ee koolituskeskkond</title>

    <link rel="icon" href="https://i.imgur.com/qKrjgui.png" sizes="any" type="image/svg+xml">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content=""/>

    <!-- Stylesheets
    ============================================= -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
            crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap<?= PROJECT_VERSION ?>"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="assets/css/custom.css?<?= PROJECT_VERSION ?>" type="text/css"/>
    <link rel="stylesheet" href="assets/themes/canvas/css/style.css?<?= PROJECT_VERSION ?>" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css?<?= PROJECT_VERSION ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <?php require 'templates/_smartsupp.php' ?>
    <?php require 'templates/logrocket.php' ?>
    <?php require '_google_analytics.php' ?>
</head>
<body>
<?php require "_menu.php" ?>

<div class="starter-template">
    <h1><?= __('Learning platform') ?></h1>

    <p class="lead"><?= __('Documents for food and drink handlers according to the Food Act.') ?></p>
</div>
<?php if (!empty($flash)): ?>
    <div class="alert alert-<?= key($flash) ?>"><?= $flash[key($flash)] ?></div>
<?php endif ?>
<div class="container">

    <!-- Main component for a primary marketing message or call to action -->
    <?php if (!file_exists("views/$controller/{$controller}_$action.php")) error_out('The view <i>views/' . $controller . '/' . $controller . '_' . $action . '.php</i> does not exist. Create that file.'); ?>
    <?php @require "views/$controller/{$controller}_$action.php"; ?>

</div>
<!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<div id="footer" style="margin-top: auto; background: rgb(30, 158, 83); color:white;">
    <div class="container site-main-footer">
        <div class="row">COPYRIGHT © 2021 GREENCLEAN OÜ</div>
    </div>
</div>
<?php require 'templates/error_modal.php'; ?>
<?php require 'system/js_constants.php' ?>
<script src="assets/js/main.js"></script>
<script src="vendor/components/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
