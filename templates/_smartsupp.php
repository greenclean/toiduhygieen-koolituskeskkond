<?php if (!MK_TEST_ENV):
// Session ID
$smartsupp['session']['label'] = 'Session Id';
$smartsupp['session']['value'] = session_id();

// Additional user data
if (!empty($_SESSION['user'])) {

    foreach ($_SESSION['user'] as $field => $value) {

        // Skip password etc
        if (in_array($field, ['password'])) {
            continue;
        }

        $smartsupp[$field]['label'] = $field;
        $smartsupp[$field]['value'] = $value;
    }
}

// Errors
if (!empty($errors[0])) {
    $smartsupp['error']['label'] = 'Error';
    $smartsupp['error']['value'] = $errors[0];
}
?>
<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '0f8bfb7946ed569fe423f853fecf6ec7966449c8';
    window.smartsupp || (function (d) {
        var s, c, o = smartsupp = function () {
            o._.push(arguments)
        };
        o._ = [];
        s = d.getElementsByTagName('script')[0];
        c = d.createElement('script');
        c.type = 'text/javascript';
        c.charset = 'utf-8';
        c.async = true;
        c.src = 'https://www.smartsuppchat.com/loader.js?';
        s.parentNode.insertBefore(c, s);
    })(document);

    <?php if(!empty($_SESSION['user'])):?>
    smartsupp('email', '<?= $_SESSION['user']['email']?>');
    smartsupp('name', '<?= $_SESSION['user']['name']?>');
    <?php endif?>

    smartsupp('variables', <?= json_encode($smartsupp)?>);

</script>
<?php endif?>