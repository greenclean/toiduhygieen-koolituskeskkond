<style>
    #add-question-modal > .modal-dialog {
        display: table;
    }

    th {
        text-align: center;
    }

    th > div {
        padding: 10px
    }

    thead {
        background-color: #fafafa;

    }
</style>
<!-- Button for triggering add question modal
================================================= -->
<button type="button" class="btn btn-primary btn-lg btn-open-add-question-modal" data-toggle="modal"
        data-target="#add-question-modal">Add question</button>

<!-- Add question modal
================================================= -->
<div class="modal fade" id="add-question-modal" tabindex="-1" role="dialog" aria-labelledby="add-question-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add question</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <table class="flex-table table table-nonfluid table-bordered table-hover table-new-question bordered">
                        <thead>
                        <tr>
                            <th colspan="2"><?= $course_language['language_name'] ?></th>
                            <?php foreach ($translated_languages as $language_code => $language): ?>
                                <th>
                                    <div>
                                        <a class="btn btn-primary btn-translate"
                                           style="padding: 2px 6px 4px 4px" href="javascript:undefined"
                                           role="button" data-lang="<?= $language_code ?>">
                                            <img src="assets/img/gtranslate.ico"
                                                 alt="Google Translate"
                                                 style="width:20px">
                                            Google Translate
                                        </a>
                                    </div>
                                    <?= $language['language_name'] ?>
                                    <br>
                                </th>
                            <?php endforeach ?>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        <!-- QUESTION ROW
                        ==========================-->
                        <tr class="question-row">
                            <td colspan="2" data-lang="<?= $course_language['language_code'] ?>">
                                <textarea name="question_text" class="primary-language-question-text"></textarea>
                            </td>
                            <?php foreach ($translated_languages as $language_code => $language): ?>
                                <td data-lang="<?= $language_code ?>">
                                        <textarea
                                                name="translation_in_<?= $language_code ?>"
                                                class="question-translation-in-<?= $language_code ?>"></textarea>
                                </td>
                            <?php endforeach ?>
                            <td>
                                <div>
                                    <a style=" width: 100%;"
                                       class="add-answer-row-to-modal btn btn-success">Add answer</a>
                                </div>
                            </td>
                        </tr>

                        <!-- ANSWER ROW (INITIALLY JUST ONE ROW)
                         ==========================-->
                        <tr class="answer-row">
                            <td><input name="new_answer_radio" class="answer-correct-radio" type="radio"></td>
                            <td data-lang="<?= $course_language['language_code'] ?>">
                                <textarea name="answer_text" class="primary-language-answer-text"></textarea>
                            </td>
                            <?php foreach ($translated_languages as $language_code => $language): ?>
                                <td data-lang="<?= $language_code ?>" class="translation-answer-text">
                                    <textarea class="answer-translation-in-<?= $language_code ?>"></textarea></td>
                            <?php endforeach ?>
                            <td style="text-align: center;">
                                <a class="delete-modal-answer-row btn btn-danger">Delete</a>
                            </td>
                        </tr>

                        <!-- EXPLANATION
                         ==========================-->
                        <tr class="explanation-row">
                            <td colspan="2"
                                data-lang="<?= $course_language['language_code'] ?>">
                                <input type="text" name="explanation" class="primary-language-explanation-text"></td>
                            <?php foreach ($translated_languages as $language_code => $language): ?>
                                <td class="non_primary"
                                    data-lang="<?= $language_code ?>">
                                    <input type="text" name="explanation"
                                           class="explanation-translation-in-<?= $language_code ?>">
                                </td>
                            <?php endforeach ?>
                            <td>
                                <select class="dropdown" style="width: 100%">
                                    <option value="0">Categories</option>
                                    <?php foreach ($categories as $category) : ?>
                                        <option value="<?= $category['test_category_id'] ?>">
                                            <?= $category['test_category_name'] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        </tbody>

                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="save-new-question btn btn-success">Add question</button>
            </div>
        </div>
    </div>
</div>


<script>
    <?php foreach ($translated_languages as $language_code => $language):$answerOptions[] = <<<__
    <td data-lang="$language_code"><textarea class="answer-translation-in-$language_code"></textarea></td>
__; endforeach?>

    // Add new answer row to modal (the main table has editable tds)
    $(".add-answer-row-to-modal").click(function () {
        $(".table-new-question .explanation-row").before(`
        <tr class="answer-row">
            <td>
                <input name="new_answer_radio" class="answer-correct-radio" type="radio"></td>
            <td data-lang="<?=$course_language["language_code"]?>"><textarea class="primary-language-answer-text"></textarea></td>
            <?= implode($answerOptions) ?>
            <td style="text-align: center;">
                <a class="delete-modal-answer-row btn btn-danger">Delete</a></td>
        </tr>
    `)
    })

    // Remove answer row
    $(".table-new-question tbody").on('click', '.delete-modal-answer-row', function () {
        if (!confirm('Are you sure?')) {
            return;
        }
        $(this).parents('tr').remove()
    })

    // Save new question
    $('.save-new-question').click(function () {
        let question = {
            answers: {},
            question: {},
            explanation: {},
            category_id: $('.table-new-question select option:selected').val(),
            course_id: <?= $course['course_id'] ?>,
            course_language: '<?= $course_language['language_code'] ?>'
        }

        addQuestionAndItsTranslations(question);
        addAnswerAndItsTranslations(question);
        addExplanationAndItsTranslations(question);
        setCorrectAnswer(question);

        ajax("admin/save_new_question", {question}, RELOAD)
    })

    function createAnswerObjectIfNotExists(question, number) {
        if (typeof question.answers[number] === 'undefined') {
            question.answers[number] = {
                translations: {},
                answerIsCorrect: false
            }
        }
    }

    function answerIsCorrect(textArea) {
        return !!$(textArea).parents('tr').find('.answer-correct-radio').val();
    }

    function getAnswerNumber(answerTextArea) {
        return $(answerTextArea).parents('tr').data('number');
    }

    function getLanguage(answerTextArea) {
        return $(answerTextArea).parents('td').data('lang');
    }

    function addAnswerAndItsTranslations(question) {
        $('.table-new-question tr.answer-row textarea').each(function (i, answerTextArea) {
            let number = $(answerTextArea).parents('tr').index() - 1
            createAnswerObjectIfNotExists(question, number);
            question.answers[number].translations[getLanguage(answerTextArea)] = $(answerTextArea).val()
            question.answers[number].answerIsCorrect = answerIsCorrect(answerTextArea);
        })
    }

    function addQuestionAndItsTranslations(question) {
        $('.table-new-question tr.question-row textarea').each(function (i, textArea) {
            question.question[$(textArea).parents('td').data('lang')] = $(textArea).val()
        })
    }

    function addExplanationAndItsTranslations(question, textArea) {
        $('.table-new-question tr.explanation-row input').each(function (i, textArea) {
            question.explanation[$(textArea).parents('td').data('lang')] = $(textArea).val()
        })
    }

    function setCorrectAnswer(question) {
        for (const [number] of Object.entries(question.answers)) {
            question.answers[number].answerIsCorrect = $(`.answer-row:eq(${parseInt(number)}) .answer-correct-radio`).prop('checked')
        }
    }

    // Automatic translation for non-primary languages for new question
    $("thead").on('click', '.btn-translate', function () {

        let clickedBtn = $(this)
        let language_code = clickedBtn.data('lang')

        // Make the Google Translate button non-clickable
        disableButton(clickedBtn);

        let questionText = clickedBtn.parents('table').find('.primary-language-question-text').val()
        let answerOptionTextareas = clickedBtn.parents('table').find('.primary-language-answer-text')
        let explanationText = clickedBtn.parents('table').find('.primary-language-explanation-text').val()
        let primary_phrases = [];

        // Untranslated array order: question, explanation, questions
        primary_phrases.push(questionText);
        primary_phrases.push(explanationText);
        answerOptionTextareas.each(function (i, answerOptionTextarea) {
            primary_phrases.push(answerOptionTextarea.value);
        })

        ajax('admin/translate_primary_language_phrases_for_new_question_modal', {
                primary_phrases,
                language_from: '<?= $course_language['language_code'] ?>',
                language_to: language_code

            }, function (res) {

                $(clickedBtn).parents('table').find(`.question-translation-in-${language_code}`).val(res.data[0]);
                res.data.splice(0, 1)

                $(clickedBtn).parents('table').find(`.explanation-translation-in-${language_code}`).val(res.data[0]);
                res.data.splice(0, 1)

                $(clickedBtn).parents('table').find(`.answer-translation-in-${language_code}`).each(function (i, answerOptionTranslationTextarea) {
                    $(answerOptionTranslationTextarea).val(res.data[i])
                })

                // Make the Google Translate button clickable again
                enableButton(clickedBtn);

            },
            function (res) {
                alert('Server said: ' + JSON.stringify(res))
                enableButton(clickedBtn)
            })
    })

    function disableButton(btn) {
        btn.prop('disabled', true).addClass('disabled')
    }

    function enableButton(btn) {
        btn.prop('disabled', false).removeClass('disabled')
    }

</script>