<script src="https://use.fontawesome.com/d37013578f.js"></script>
<style>
    .search {
        width: 100%
    }

    #language-table td:last-child, #language-table th:last-child {
        width: 20px;
        vertical-align: middle;
    }

    #language-table tr:last-child td:first-child {
        vertical-align: middle;
        padding: 0 8px 0 0;
    }

    #language-name, #language-name.form-control:focus, {
        border: 0 !important;
        height: 48px !important;
        box-shadow: none;

    }

    #query:focus {
        box-shadow: none;
    }

    #add-language-modal > div > div > div.modal-header > button {
        color: white;
    }


    #add-language-modal .modal-body {
        padding: 0;
    }

    #language-table {
        margin-bottom: 0
    }

    .fa:hover {
        color: #007bff;
        cursor: pointer;
    }

    #language-table > tbody > tr > td:nth-child(2) > i {
        color: red
    }

    #language-table > tbody > tr:last-child td:nth-child(2) > i {
        color: mediumseagreen;
    }

    #add-language-modal .modal-header {
        background-color: #212529 !important;
        color: white;
        border: 1px solid #212529;
        border-radius: 0;
    }

    table {
        text-align: left;
        position: relative;
        background-color: white;
    }

    tr:first-child th {
        color: black !important;
        position: sticky;
        top: 42px;
        background: #ddd;
    }

    th > label {
        color: black !important;
    }

    tr:nth-child(2) th {
        color: red !important;
        position: sticky;
        top: 113px;
    }

    tr.help-block th {
        font-weight: normal;
        padding: 6px 0 0 9px;
        color: gray;
    }

    #translations-table > thead > tr:nth-child(1) > th > label {
        font-weight: normal;
        color: lightgray;
        white-space: nowrap;
    }

    #translations-table > thead > tr:nth-child(1) > th {
        vertical-align: top;
    }

    #th-search {
        padding: 4px;
        background: #ddd;
    }

    #translations-table {
        width: 100% !important;
    }

    .modal-title {
        color: whitesmoke;
    }

    h5 {
        padding-top: 10px;
    }

    i.fa:hover {
        color: white
    }

    .loader {
        border: 4px solid #f3f3f3;
        border-radius: 50%;
        border-top: 4px solid #3498db;
        width: 24px;
        height: 24px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>

<?php if (!empty($translations_where_phrase_is_too_long)): ?>
    <?php foreach ($translations_where_phrase_is_too_long as $item): ?>
        <div class="alert alert-danger">You have a translation phrase
            <b><?= htmlentities($item['translation_phrase']) ?></b> which is
            too long to store effectively in database. To avoid errors, phrase must be shortened in the code.
        </div>
    <?php endforeach ?>
<?php endif; ?>

<table id="translations-table" class="table table table-nonfluid table-bordered table-hover table-users bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Phrase</th>
        <th>Source</th>
        <?php foreach ($languages_in_use as $language_code => $language): ?>
            <th><?= $language['language_name'] ?><br>
                <label>
                    <input type="checkbox"
                           class="show-untranslated"
                           data-lang="<?= $language_code ?>"
                        <?= isset($show_untranslated[$language_code]) ? 'checked' : '' ?>>Untranslated
                </label>
            </th>
        <?php endforeach ?>
    </tr>
    <tr>
        <th id="th-search" colspan="<?= count($languages_in_use) + 3 ?>">

            <div class="input-group">
                <input type="text" id="query" class="form-control search" placeholder="Search">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="button" data-toggle="modal" role="button"
                            data-target="#add-language-modal">
                        <i class="fa fa-language" aria-hidden="true"></i> Languages
                    </button>
                </div>
            </div>


        </th>
    </tr>
    </thead>
    <tbody class="lookup">
    <?php foreach ($translations as $t): ?>
        <tr data-id="<?= $t['translation_id'] ?>">
            <td><?= ++$n ?></td>
            <td style="white-space: pre-line"><?= $t['translation_phrase'] ?></td>
            <td style="white-space: pre-line"><?= $t['translation_source'] ?></td>
            <?php foreach ($languages_in_use as $language_code => $language): ?>
                <td class="editable"
                    data-lang="<?= $language_code ?>"><?= $t['translation_in_' . $language_code] ?></td>
            <?php endforeach ?>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="add-language-modal" tabindex="-1" role="dialog" aria-labelledby="add-language-modal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-close-language-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Languages</h5>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="language-table">
                                <tbody>
                                <?php foreach ($languages_in_use as $language_code => $language): ?>
                                    <tr>
                                        <td data-lang="<?= $language_code ?>" data-id="<?= $language['language_id'] ?>">

                                            <?= $language['language_name'] ?>

                                            <?php if ($language['language_name'] != 'English'): ?>

                                                <?php if ($statistics[$language_code]['translated'] != $statistics['total']): ?>

                                                    <sup>
                                                        <span class="badge badge-danger">
                                                            <?= $statistics[$language_code]['remaining'] ?>
                                                        </span>
                                                    </sup>

                                                    <span style="float:right;">
                                                        <a class="btn btn-primary btn-translate-remaining"
                                                           style="padding: 2px 6px 4px 4px"
                                                           href="javascript:undefined" role="button"
                                                           data-lang="<?= $language_code ?>">
                                                                <img src="assets/img/gtranslate.ico"
                                                                     alt="Google Translate"
                                                                     style="width:20px">
                                                            Google Translate
                                                        </a>
                                                    </span>

                                                <?php endif ?>

                                            <?php endif ?>
                                        </td>
                                        <td>
                                            <i class="fa fa-minus-square fa-lg delete-language-link"
                                               aria-hidden="true"></i>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td class="add-language-td">
                                        <select class="form-control" name="language-name" id="language-name">
                                            <option value="">-- Select language --</option>
                                            <?php foreach ($languages_not_in_use as $language_code => $language): ?>
                                                <option value="<?= $language_code ?>"><?= $language['language_name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </td>
                                    <td class="add-language-td">
                                        <i id="add-language-link" class="fa fa-plus-square fa-lg"
                                           aria-hidden="true"></i>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="help-block"
                      style="float: left"><sup>*</sup>Google translates up to 5000 chars at a time</span>
                <button style="float: right" type="button" class="btn btn-secondary btn-close-language-modal"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script>

    $('#add-language-link').on('click', function () {
        let clickedBtn = $(this)
        let language_code = $(this).parents('tr').find('td:nth-child(1) select').val()

        if (!language_code) {
            alert('Select language first')
            return
        }

        // Create a spinner and insert it after the button
        let spinner = $('<div class="loader"></div>')

        spinner.insertAfter(clickedBtn);
        clickedBtn.css('display', 'none');

        ajax('admin/translation_add_language', {
            language_code
        }, RELOAD, function (res) {
            spinner.remove();
            clickedBtn.css('display', 'block');
            show_error_modal(res);
        })
    })

    // Reload page when language manager modal is closed
    $('.btn-close-language-modal').on('click', function () {
        location.reload();
    });

    $('.delete-language-link').on('click', function () {

        let clickedBtn = $(this)
        let language_code = clickedBtn.parents('tr').find('td:nth-child(1)').data('lang')
        let language_id = clickedBtn.parents('tr').find('td:nth-child(1)').data('id')

        if (!confirm('Are you really sure you want to remove the language %%% and destroy its translations?'.replace('%%%', language_code.toUpperCase()))) {
            return false;
        }

        clickedBtn.css('display', 'none');
        // Create a spinner and insert it after the button
        let spinner = $('<div class="loader"></div>')
        spinner.insertAfter(clickedBtn);

        ajax('admin/translation_delete_language', {
            language_code,
            language_id
        }, function () {
            clickedBtn.parents('tr').remove();
        }, function (res) {
            show_error_modal(JSON.stringify(res))
            spinner.css('display', 'none');
            clickedBtn.css('display', 'inline-block');
        })

    })

    $('.btn-translate-remaining').on('click', function () {

        let clickedBtn = $(this)
        let language_code = clickedBtn.data('lang')

        // Hide the Google Translate button
        clickedBtn.css('display', 'none');

        // Create a spinner and insert it after the button
        let spinner = $('<div class="loader"></div>')
        spinner.insertAfter(clickedBtn);

        ajax('admin/translate_remaining_strings', {
            language_code
        }, function (res) {

            // Remove the spinner
            spinner.remove();

            // If all strings are translated
            if (res.data.untranslatedCount === "0") {


                // Remove the red badge
                clickedBtn.parents('td').find('.badge-danger').remove();

                // Remove Google Translate button
                clickedBtn.remove();

            } else {

                // Set the badge to untranslated strings count
                clickedBtn.parents('td').find('.badge-danger').html(res.data.untranslatedCount)


                // Re-show the Google Translate button
                clickedBtn.prop('disabled', false);
                clickedBtn.css('display', 'block');
            }
        })
    })

    // Enable editing table
    /* @var $ */
    $(function () {
        $("tbody").on('dblclick', '.editable', function () {
            let cell = $(this)
            let language_code = cell.data('lang');
            let originalContent = cell.html();
            let translation_id = cell.parents('tr').data('id');
            let translation = prompt("Enter new content for:", originalContent);

            if (translation != null) {

                // Send value to back-end
                ajax('admin/translation_edit', {translation_id, language_code, translation}, function (res) {
                    cell.removeClass('alert-danger');
                    cell.addClass('alert-success');

                }, function (res) {
                    if (typeof res !== 'undefined') {
                        show_error_modal(res)
                    }
                    cell.addClass('alert-danger');
                });

                // Write value to table
                $(this).html(translation)
            }
        });
    });

    // Filter translations table
    $('#query').keyup(function () {
        let input, filter, table, tr, td, i;
        input = document.getElementById("query");
        filter = input.value.toUpperCase();
        table = document.getElementById("translations-table");
        tr = table.getElementsByTagName("tr");
        let cell;
        for (i = 2; i < tr.length; i++) {

            // Hide the row initially.
            tr[i].style.display = "none";

            // Get all TDs of the row
            td = tr[i].getElementsByTagName("td");

            for (let j = 0; j < td.length; j++) {
                cell = tr[i].getElementsByTagName("td")[j];
                if (cell) {
                    if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                    }
                }
            }
        }
    })

    $('input[type="checkbox"]').on('change', function () {

        let filteredLanguages = [];

        $('.show-untranslated:checked').each(function (index, element) {
            console.log(element)
            filteredLanguages.push($(element).data('lang'))
        })

        let filter = filteredLanguages.join();

        location.href = `admin/translation${filter ? '?show_untranslated=' + filter : ''}`
    });

    // Widen the table to full page width
    $('.container').addClass('container-fluid').removeClass('container');

    let resetTranslationTableHeaderPosition = function () {

        // Fix mobile view by setting minimum width of the page to be the sufficient for all language columns
        $('body').css('min-width', +<?=150 + (130 * count($languages_in_use))?>);

        $('tr:first-child th').css('top', $('#header-wrap').height() - 1)
    }
    resetTranslationTableHeaderPosition();
    window.onresize = resetTranslationTableHeaderPosition;

</script>