<style>
    #divAnswerOptions {
        margin-left: 25px;
        height: auto;
    }

    .fa-plus {
        color: white;
        font-size: 20px !important;
        padding-right: 10px;
    }

    .delete-answer {
        width: 30px;
        height: 30px;
        float: right
    }

    .txtAnswerOption {
        width: 84%;
        margin-left: auto;
        margin-righT: auto;
        display: inline-block;
    }

    .trueness {
        width: 30px;
    }

    .trueness:hover, .delete-answer:hover {
        cursor: pointer;
    }

    .mt-2{
        margin-top: 20px;
    }

    .mb-2{
        margin-bottom: 20px;
    }
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="col-sm-12 text-right mt-2  mb-2">
    <button class="btn btn-success" data-toggle="collapse" data-target="#course-form" aria-expanded="false" aria-controls="course-form">Add Course</button>
</div>
<div class="col-sm-12" style="border: 1px solid #ddd; background: #efefef">
    <div class="collapse" id="course-form">
        <div class="well">
            <form role="form" method="post" action="admin/course_save">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="data[course_name]" id="name" required>
                </div>
                <div class="form-group">
                    <label for="course_price">Price:</label>
                    <input type="number" min="0" step="any" class="form-control"  name="data[course_price]" id="course_price" required>
                </div>
                <div class="form-group">
                    <label for="questions_count">Questions per test:</label>
                    <input type="number" min="0" class="form-control"  name="data[questions_count]" id="questions_count">
                </div>
                <div class="form-group">
                    <label for="language_id">Course primary language:</label>
                    <select class="form-control" name="data[language_id]" id="language_id" required>
                        <option value="">-- <?= __('Select language') ?> --</option>
                        <?php foreach ($languages_in_use as $language): ?>
                            <option value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="default_course">
                        <input type="checkbox" name="data[default_course]" value="1" id="default_course">
                        Default</label>
                </div>
                <div class="form-group">
                    <label for="course_repeatable">
                        <input type="checkbox" name="data[course_repeatable]" value="1" id="course_repeatable">
                        Open automatically</label>
                </div>
                <div class="form-group">
                    <label for="in_login_status">
                        <input type="checkbox" name="data[in_login_status]" value="1" id="in_login_status" checked>
                        Only logged In users</label>
                </div>
                <button type="submit" name="save" class="btn btn-success">Save</button>
                </button>
            </form>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th class="text-right">Action</th>
            </tr>
            </thead>
            <tbody class="lookup">
            <?php foreach ($courses as $course): ?>
                <tr>
                    <td><?= $course['course_id'] ?></td>
                    <td><?= __($course['course_name'], "courses.course_name", $course['course_id']) ?></td>
                    <td><?= $course['course_price'] ?></td>
                    <td style="display: flex; align-items: center; justify-content: flex-end" class="look">
                        <a style="margin: 3px" href="admin/questions/<?= $course['course_id'] ?>" class="edit_question btn btn-primary">Questions</a>
                        <a style="margin: 3px" href="learn/edit/<?= $course['course_id'] ?>" class="edit_question btn btn-primary">Materials</a>
                        <a style="margin: 3px" href="admin/edit_course?id=<?= $course['course_id'] ?>" class="edit_question btn btn-primary">Change</a>
                        <a style="margin: 3px" href="admin/delete_course?id=<?= $course['course_id'] ?>" class="edit_question btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>
                    </td>
                </tr>

            <?php endforeach ?>
            </tbody>

        </table>
    </div>
</div>


