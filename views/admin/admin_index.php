<style>
    h2 {
        margin: 10px 0;
        font-weight: normal;
    }
    h2 > a {
        color: black;
    }
    h2 > a:hover {
        text-decoration: underline solid black !important;
    }
</style>
<h2><a href="orders">Payments</a></h2>
<h2><a href="admin/email">Email templates</a></h2>
<!--<h2><a href="results">Test results</a></h2>-->
<h2><a href="admin/customers">Users</a></h2>
<h2><a href="admin/translation">Translations</a></h2>
<h2><a href="admin/courses">Courses</a></h2>