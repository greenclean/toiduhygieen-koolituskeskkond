<style>
    .mt-2{
        margin-top: 20px;
    }

    .mb-2{
        margin-bottom: 20px;
    }
</style>

<div class="col-sm-6 mt-2">
    <div class="well">
        <form role="form" method="post" action="admin/course_update">
            <input type="hidden" name="course_id" value="<?= $course['course_id'] ?>">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="data[course_name]" id="name" value="<?= $course['course_name'] ?>">
            </div>
            <div class="form-group">
                <label for="name">Price:</label>
                <input type="number" step="any" class="form-control"  name="data[course_price]" id="personal_code" value="<?= $course['course_price'] ?>">
            </div>
            <div class="form-group">
                <label for="questions_count">Questions per test:</label>
                <input type="number" min="0" class="form-control"  name="data[questions_count]" id="questions_count"  value="<?= $course['questions_count'] ?>">
            </div>
            <div class="form-group">
                <label for="language_id">Course primary language:</label>
                <select class="form-control" name="data[language_id]" id="language_id" required>
                    <option value="">-- <?= __('Select language') ?> --</option>
                    <?php foreach ($languages_in_use as $language): ?>
                        <option value="<?= $language['language_id'] ?>" <?= $language['language_id'] == $course['language_id'] ? "Selected" : "" ?>><?= $language['language_name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <label for="default_course">
                    <input type="checkbox" name="data[default_course]" value="1" id="default_course" <?= $course['default_course'] ? 'checked' : ''?>>
                    Default</label>
            </div>
            <div class="form-group">
                <label for="course_repeatable">
                    <input type="checkbox" name="data[course_repeatable]" value="1" id="course_repeatable" <?= $course['course_repeatable'] ? 'checked' : ''?>>
                    Open automatically</label>
            </div>
            <div class="form-group">
                <label for="in_login_status">
                    <input type="checkbox" name="data[in_login_status]" value="1" id="in_login_status" <?= $course['in_login_status'] ? 'checked' : ''?>>
                    Only logged In users</label>
            </div>
            <button type="submit" name="update" class="btn btn-success">Save</button>
            </button>
        </form>
    </div>
</div>
<script>
    $(function () {
        $("#default_course").click(function () {
          if(!this.checked) {
              box = confirm('This action will remove this course from all users.\n' +
                 'Are you sure you want to perform this action?');
              if (box==true) {
                  return true;
              } else {
                  return false;
              }
          }
        });
    });
</script>