<table style="width:100%" class="table table-bordered table-hover table-condensed emailed">
    <caption>
        <h2>Email contents</h2>
    </caption>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Content</th>
        <th>Language</th>
        <th>Type</th>
        <th></th>
    </tr>
    <?php foreach ($emails as $email): ?>
        <tr>
            <td><?= $email['email_text_id'] ?></td>
            <td><?= $email['email_text_subject'] ?></td>
            <td><?= $email['email_text_content'] ?></td>
            <td><span class="ServiceName">  <?= $email['language_name'] ?></span></td>
            <td><span class="ServiceDate"><?= $email['email_type_name'] ?></span></td>
            <td class="look"><a href="#" class="look pull-right muuda">Edit</a></td>

        </tr>
    <?php endforeach ?>
</table>
<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit email</h4>
            </div>
            <div class="modal-body">
                <p>Fill the fields.</p>


                <h2>Email editing</h2>
                <form role="form" method="post" action="<?= BASE_URL ?>admin/email_text_edit">
                    <input type="hidden" name="id" id="id_text">
                    <div class="form-group">
                        <label for="subject">Title:</label>
                        <input type="text" class="form-control"
                               name="data[email_text_subject]" id="subject">
                    </div>
                    <div class="form-group">
                        <label for="content">Content:</label>
                        <textarea rows="10" class="form-control" name="data[email_text_content] "
                                  id="content"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="languages">Choose language:</label><br>
                        <select name="data[language_id]" id="language_id">
                            <?php foreach ($languages as $language): ?>
                                <option
                                        value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
                            <?php endforeach ?>
                        </select>

                    </div>

                    <div class="form-group">
                        <label for="type">Choose type:</label><br>
                        <select name="data[email_type_id]" id="email_type_id">
                            <?php foreach ($types as $type): ?>
                                <option value="<?= $type['email_type_id'] ?>"><?= $type['email_type_name'] ?></option>
                            <?php endforeach ?>
                        </select>

                    </div>


                    <button type="submit" name="go" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Cancel
                    </button>
                </form>
            </div>

        </div>

    </div>
</div>
<script src="assets/components/trumbowyg/dist/trumbowyg.min.js"></script>
<link rel="stylesheet" href="assets/components/trumbowyg/dist/ui/trumbowyg.min.css">
<script>

</script>

<script>

    // Widen the table to full page width
    $('.container').addClass('container-fluid').removeClass('container');

    $(".btn-secondary, .close").on("click", function () {

        $('textarea').trumbowyg('destroy');
    });

    $(".muuda").click(function (e) {

        e.preventDefault();
        var id = $(this).parents('tr').find('td:first-child').text();
        $.post("admin/email_edit", {id: id}, function (res) {
            if (res.result == 'OK') {
                $("#id_text").val(res.data[0].email_text_id);
                $("#subject").val(res.data[0].email_text_subject);
                $("#content").val(res.data[0].email_text_content);
                $("#language_id").val(res.data[0].language_id);
                $("#email_type_id").val(res.data[0].email_type_id);

                $("#myModal").modal("show");
                $('textarea').trumbowyg();


            } else {
                alert("Tekkis viga!")

            }
        });
    });

    function email_delete(id) {
        $.post("admin/email_delete", {id: id}, function (result) {
            if (result == "OK") {
                window.location.href = window.location.href;
            }
        })
    }
</script>