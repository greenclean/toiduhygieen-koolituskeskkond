<style>
    .answers-input input[type="text"] {
        display: inline;
        width: 95%;
    }

    .dropdown {
        width: 150px;
        border: 1px solid #cfcfcf;
        height: auto;
        border-radius: 5px;
        padding: 3px 4px 4px;
        position: relative;
        z-index: 0;
        overflow: hidden;
    }

    .dropdown select {
        border: none;
        background-color: transparent;
        outline: none;
        padding: 0;
        width: 150px;
        background-position: 55%;
    }

    /* not 100% sure this next option rule is needed */

    .dropdown option {
        width: 150px
    }

    td {
        padding: 5px !important;
        vertical-align: middle !important;
    }

    textarea {
        width: 100%;
        height: 100%;
        border: 1px solid #eee;
    }

    .btn-open-add-question-modal {
        padding-left: -50px !important;
    }

    table.table-fit {
        width: auto !important;
        table-layout: auto !important;
    }

    table.table-fit thead th, table.table-fit tfoot th {
        width: auto !important;
    }

    table.table-fit tbody td, table.table-fit tfoot td {
        width: auto !important;
    }

    body > div.container-fluid > table > thead > tr > th {
        border-width: 1px !important;
    }


    table {
        border-collapse: collapse !important;
        border-spacing: 0;

    }

    .separator-row {
        border: 0 solid red;
        height: 30px;
    }

    .separator-row td {
        border: 0 !important;
    }

    table td {
        background-color: #fafafa;
    }

    table td:last-child {
        width: 150px;
    }

    .radio-td {
        padding-right: 15px;
        width: 20px;
    }

    form, select {
        margin-bottom: 0 !important;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="alert alert-info">Double-click to edit fields</div>

<!-- Add question button and modal
================================================= -->
<?php require 'views/admin/admin_questions_add_question_modal.php'; ?>

<br>

<table class="table table-nonfluid table-bordered table-hover table-questions bordered">
    <tbody class="lookup">
    <tr class="separator-row"></tr>

    <?php foreach ($questions as $key => $question): ?>
        <!-- Headers -->
        <tr data-question_id="<?= $question['question_id'] ?>">
            <td style="text-align: center; color: #8b96aa" colspan="2"><?= $course_language['language_name'] ?></td>
            <?php foreach ($translated_languages as $language): ?>
                <td style="text-align: center; color: #8b96aa">
                    <?= $language['language_name'] ?>
                </td>
            <?php endforeach; ?>
            <td><a style=" width: 100%;" class="delete-question btn btn-danger">Delete question</a></td>
        </tr>

        <!-- Question-->
        <tr data-translation_source_id="<?= $question['question_id'] ?>"
            data-question_id="<?= $question['question_id'] ?>">
            <td colspan="2" class="editable primary"
                data-lang="<?= $course_language['language_code'] ?>"
                data-translation_source="questions"
            ><?= $question['translations'][$course_language['language_code']] ?></td>
            <?php foreach ($translated_languages as $language_code => $language): ?>
                <td class="editable"
                    data-lang="<?= $language_code ?>"
                    data-translation_source="questions"><?= $question['translations'][$language_code] ?></td>
            <?php endforeach ?>
            <td>
                <form>
                    <select class="question-category dropdown">
                        <?php foreach ($categories as $category) : ?>
                            <option value="<?= $category['test_category_id'] ?>" <?= $question['test_category_id'] === $category['test_category_id'] ? 'selected' : '' ?>>
                                <?= $category['test_category_name'] ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </form>
            </td>
        </tr>
        <!-- Question end -->

        <!-- Answer -->
        <?php foreach ($question['answers'] as $answer): ?>
            <tr data-translation_source_id="<?= $answer['answer_id'] ?>"
                data-question_id="<?= $question['question_id'] ?>">
                <td class="radio-td">
                    <input type="radio"
                           name="answer_for_question_<?= $question['question_id'] ?>" <?= $answer['answer_is_correct'] === "0" ? '' : 'checked' ?>>
                </td>
                <td class="editable primary"
                    data-translation_source="answers"
                    data-lang="<?= $course_language['language_code'] ?>"><?= $answer['translations'][$course_language['language_code']] ?></td>
                <?php foreach ($translated_languages as $language_code => $language): ?>
                    <td class="editable"
                        data-translation_source="answers"
                        data-lang="<?= $language_code ?>"><?= $answer['translations'][$language_code] ?></td>
                <?php endforeach ?>
                <td style="text-align: center;">
                    <a class="delete-answer-row btn btn-danger">Delete answer</a>
                </td>
            </tr>
        <?php endforeach ?>
        <!-- Answer end -->

        <!-- Explanation start -->
        <tr class="bottom-border" data-translation_source_id="<?= $question['question_id'] ?>">
            <td colspan="2" class="editable primary"
                data-translation_source="questions.explanation"
                data-lang="<?= $course_language['language_code'] ?>"
            ><?= $question['explanation_translations'][$course_language['language_code']] ?></td>
            <?php foreach ($translated_languages as $language_code => $language): ?>
                <td class="editable"
                    data-translation_source="questions.explanation"
                    data-lang="<?= $language_code ?>"><?= $question['explanation_translations'][$language_code] ?></td>
            <?php endforeach ?>
            <td class="border-bottom-red">
                <div style="text-align: center;">
                    <a style=" width: 100%;"
                       class="add-answer-row-to-main-table btn btn-success"
                       data-question_id="<?= $question['question_id'] ?>">Add answer</a>
                </div>
            </td>
        </tr>
        <tr class="separator-row"></tr>
    <?php endforeach ?>

    </tbody>
</table>

<?php require 'views/admin/admin_questions_js.php' ?>