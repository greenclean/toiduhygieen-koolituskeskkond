<?php /** @var \Toiduhygieen\Pagination $pagination */ ?>

<style>
    th {
        border: 1px solid white;
        background-color: #e3e3e3;
        color: #8b96aa;
        font-size: 1.1em !important;
    }

    tr {
        background-color: #f1f1f1;
    }

    tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    td {
        border: solid 1px white;
        max-width: 245px;
    }

    .header {
        width: 40%;
        float: left;
    }

    .otsing {
        float: right;
        margin-top: 20px;
    }

    .add_team_lider input {
        padding: 5px;
        margin: 0;
    }

    .add_team_lider .bi-search,
    .add_workers_cont .bi-search {
        cursor: pointer;
        margin-left: 7px;
        font-size: 17px;
    }

    .add_team_lider .bi-x,
    .add_workers_cont .bi-x {
        cursor: pointer;
        color: #dc1c1c;
        font-size: 17px;
        margin: 1px 6px;
    }

    .add_team_lider .team_lid_id {
        width: 10%;
    }

    .add_team_lider .team_lid_name {
        width: 41%;
    }

    .add_workers_cont textarea {
        margin-top: 5px;
        width: 60%;
    }

    .add_workers_cont input {
        padding: 5px 5px;
        width: 52%;
    }

    #showOnlyFaulty {
        margin: 30px 0 10px 0;
    }

    #customers-table>tbody>tr>td {
        text-align: center;
        vertical-align: middle;
    }

    .bi-x {
        color: red;
    }

    .bi-check {
        color: green;
    }

</style>
<div class="table-responsive">
    <table class="table ordered" id="customers-table">
        <caption>
            <h2 class="header">Users</h2>
            <label>
                <input type="checkbox" <?= $showOnlyFaulty ?> id="showOnlyFaulty">
                Only faulty
            </label>
            <input type="text" placeholder="Otsi..." class="otsing" value="<?= $pagination->q ?>">
        </caption>
        <tr>
            <th>ID</th>
            <th>User's name</th>
            <th>Purchases</th>
            <th>Personal code</th>
            <th>E-mail</th>
            <th>Employer</th>
            <th>Workers</th>
            <th>Inheritance</th>
            <th></th>
        </tr>
        <?php if (!empty($users)): foreach ($users as $user): ?>
            <tr>
                <td><?= $user['user_id'] ?></td>
                <td><span class="FirstNameLastName"><?= $user['name'] ?></span></td>
                <td><span class="ServiceName"><?= $user['course_names'] ?></span></td>
                <td>
                    <span class="ServiceIsik <?= $user['faulty_personal_code'] ? "red" : "" ?>">
                        <?= $user['personal_code'] ?>
                    </span>
                </td>
                <td><span class="ServiceMail"><?= $user['email'] ?></span></td>
                <td><?= $user['team_leader_name'] ? $user['team_leader_name'] . ' (id=' . $user['employer_id'] . ')' : '' ?></td>
                <td><?= $user['worker_count'] ?: '' ?></td>
                <td><i class="bi bi-<?= $user['allow_inherit']?'check':'x' ?>"></i></td>
                <td class="look"><span class="look"><a href="#" class="muuda">Muuda</a></span></td>
            </tr>
        <?php endforeach; endif; ?>

    </table>
    <?php require 'templates/_partials/pagination.php' ?>
</div>
<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Change user info</h2>
            </div>
            <div class="modal-body">


                <h4>Fill the fields</h4>

                <input type="hidden" name="id" id="id_text">
                <div class="form-group add_team_lider team_lider">
                    <div><label for="name">Employer:</label></div>
                    <input type="number" class="team_lid_id" min="0">
                    <input type="text" class="team_lid_name" disabled>
                    <i class="bi bi-search search_teamlider"></i>
                    <i class="bi bi-x remove_teamlider"></i>
                </div>
                <div class="form-group add_workers_cont add_workers">
                    <div><label for="name">Workers:</label></div>
                    <input type="text" class="worker_ids" placeholder="1,2,3 ...">
                    <input type="hidden" id="worker_ids" name="worker_ids">
                    <i class="bi bi-search search_workers"></i>
                    <i class="bi bi-x remove_workers"></i>
                    <textarea name="" id="" cols="30" rows="10" class="form-control worker_names"
                              disabled></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input maxlength="148" type="text" class="form-control"
                           name="data[name]" id="name">
                </div>
                <div class="form-group">
                    <label for="name">Personal code:</label>
                    <input type="text" class="form-control"
                           name="data[personal_code]" id="personal_code">
                </div>
                <div class="form-group">
                    <label for="name">E-mail:</label>
                    <input type="email" class="form-control"
                           name="data[email]" id="email">
                </div>
                <div class="form-group">
                    <label for="languages">Languages:</label><br>
                    <select name="data[language_id]" id="language_id">
                        <?php foreach ($languages as $language): ?>
                            <option
                                    value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
                        <?php endforeach ?>
                    </select>

                </div>
                <div class="form-group">
                    <label for="courses">Courses:</label><br>
                    <select name="courses[]" id="courses" class="form-control" multiple="multiple">
                        <?php foreach ($courses as $course): ?>
                            <option checked value="<?= $course['course_id'] ?>"
                                    id="course-<?= $course['course_id'] ?>"><?= $course['course_name'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="allow_inherit">
                        <input type="checkbox" value="1" name="data[allow_inherit]" id="allow_inherit">
                        Allow employees to inherit courses from this employer</label>
                </div>
                <button type="button" name="save" class="btn btn-success edit-worker">Save</button>
                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Cancel
                </button>
            </div>

        </div>

    </div>
</div>

<div class="modal modal-wide fade" id="email-check-result-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="email-check-modal-title">Email address not verified</h4>
            </div>
            <div class="modal-body email-check-modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"
                        id="btn-proceed-anyway">Continue anyway</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    let loggedin_user_id;

    // Remove invisible characters
    function removeWhitespace(text) {
        return text && text.replace(/[\u200E\u200F\u202A-\u202C]/g, "");
    }

    // Remove all characters except - and integers
    function sanitizePersonalCode(personalCode) {
        return personalCode && personalCode.replace(/[^0-9-]+/g, "");
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }


    (function ($) {
        $("#btn-proceed-anyway").on('click', function (e) {
            save();
        })

        $(".muuda").on('click', function (e) {
            e.preventDefault();
            loggedin_user_id = $(this).parents('tr').find('td:first-child').text();
            $.post("admin/customer_edit", {id: loggedin_user_id}, function (res) {
                $("#id_text").val(res.data.user_id);
                $("#name").val(removeWhitespace(res.data.name));
                $("#personal_code").val(sanitizePersonalCode(res.data.personal_code));
                $("#language_id").val(res.data.language_id);
                $("#email").val(removeWhitespace(res.data.email));

                if (Number(res.data.allow_inherit)) {
                    $("#allow_inherit").prop('checked', true);
                } else {
                    $("#allow_inherit").prop('checked', false);
                }

                var course_ids = [];
                res.data.user_courses.forEach(function (el) {
                    course_ids.push(el.course_id);
                });

                $('#courses').val(course_ids);
                $('#courses').trigger('change');
                $('.team_lid_id').val('');
                $('.team_lid_name').val('');
                $('#team_lid_id').val('');
                $('.worker_names').val('');
                $('.worker_ids').val('');
                $('#worker_ids').val('');

                if (res.data.team_leader_name) {
                    var class_teamlid_cont = '';

                    class_teamlid_cont = 'team_lider';


                    $('.' + class_teamlid_cont + ' .team_lid_id').val(res.data.employer_id);
                    $('.' + class_teamlid_cont + ' #team_lid_id').val(res.data.employer_id);
                    $('.' + class_teamlid_cont + ' .team_lid_name').val(res.data.team_leader_name);
                    $('.' + class_teamlid_cont).append('<input type="hidden" id="team_lid_id" name="data[employer_id]" value="' + res.data.employer_id + '">');

                }

                if (res.data.workers.length) {
                    var worker_names = '';
                    var worker_ids = '';
                    res.data.workers.forEach(function (el) {
                        worker_names += el.name + ', ';
                        worker_ids += el.user_id + ',';
                    });

                    worker_names = worker_names.substring(0, worker_names.length - 2);
                    worker_ids = worker_ids.substring(0, worker_ids.length - 1);
                    $('.worker_names').val(worker_names);
                    $('.worker_ids').val(worker_ids);
                    $('#worker_ids').val(worker_ids);
                }

                $("#myModal").modal("show");
            });

        });

        $('.edit-worker').on('click', function () {

            email = removeWhitespace($('#email').val());

            if (!validateEmail(email)) {
                $('#email').css('border-color', 'red');
                return false;
            }

            // Validate email address
            ajax('admin/check_email', {
                'email': email,
            }, function (json) {

                console.log(json.data.emailCheck.result);
                console.log(json.data.emailCheck.details);

                if (!json.data.emailCheck.result) {

                    // Validation failed, show validation details
                    $('.email-check-modal-body').html(json.data.emailCheck.details)
                    $('#email-check-result-modal').modal('toggle')

                } else {

                    // Validation passed, save changes and close the modal
                    save();
                }

            });


        })

        function save() {
            ajax('admin/customer_save', {
                'user_id': $('#id_text').val(),
                'data': {
                    'employer_id': $('.team_lid_id').val(),
                    'name': removeWhitespace($('#name').val()),
                    'personal_code': sanitizePersonalCode($('#personal_code').val()),
                    'email': email,
                    'language_id': $('#language_id').val(),
                    'allow_inherit': $('#allow_inherit').prop('checked')?1:0
                },
                'courses': $('#courses').val(),
                'worker_ids': $('#worker_ids').val()
            }, RELOAD)
        }

        $(".search_teamlider").on('click', function (e) {
            let t_id = $('.team_lider .team_lid_id').val();
            $('.team_lid_name').val('');
            $('#team_lid_id').val('');
            ajax("admin/customer_edit", {id: t_id}, function (json) {
                if (loggedin_user_id != json.data.user_id) {
                   // $('#team_lid_id').remove();
                    $('.team_lider').append('<input type="hidden" id="team_lid_id" name="data[employer_id]">');
                    $('.team_lider .team_lid_name').val(json.data.name);
                    $('.team_lider #team_lid_id').val(json.data.user_id);
                }
            });
        });

        $(".remove_teamlider").on('click', function (e) {
            $('.team_lid_id').val('');
            $('.team_lid_name').val('');
            $('#team_lid_id').val('');
            $('#team_lid_id').val('0');
        });

        $(".add_workers .search_workers").on('click', function (e) {
            var w_ids = $('.worker_ids').val();
            $('.worker_names').val('');
            $('#worker_ids').val('');
            $.post("admin/get_workers", {id: w_ids}, function (res) {
                if (res.result == "Ok") {
                    var worker_names = '';
                    var worker_ids = '';
                    res.data.forEach(function (el) {
                        worker_names += el.name + ', ';
                        worker_ids += el.user_id + ',';
                    });

                    worker_names = worker_names.substring(0, worker_names.length - 2);
                    worker_ids = worker_ids.substring(0, worker_ids.length - 1);
                    $('.worker_names').val(worker_names);
                    $('#worker_ids').val(worker_ids);
                }
            });
        });

        $(".add_workers .remove_workers").on('click', function (e) {
            $('.worker_names').val('');
            $('.worker_ids').val('');
            $('#worker_ids').val('');
        });

        $(function () {
            $('#courses').select2({
                width: '100%'
            });
        });


        $('.otsing').change(function () {
            var url = updateURLParameter(window.location.href, "page", null)
            location.href = updateURLParameter(url, "q", $(this).val())
        });

        $('#showOnlyFaulty').change(function () {
            location.href = updateURLParameter(window.location.href, "showOnlyFaulty", this.checked ? 1 : 0)
        });


    }(jQuery));
</script>