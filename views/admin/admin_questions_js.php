<?php

use Toiduhygieen\Language;

?>
<script>

    // Widen the table to full page width
    $('.container').addClass('container-fluid').removeClass('container');

    // Add answer row for existing question (differs from modal new answer row)
    $(".add-answer-row-to-main-table").click(function () {

        let that = $(this)
        let name = that.parents("tr").prev().find('[type="radio"]').prop("name")
        let question_id = that.data('question_id')
        let answer_count = $('input[name*="' + name + '"]').length

        // Reload because otherwise the tr data is incorrect
        ajax('admin/add_answer_row_to_main_table', {question_id, answer_count}, function (res) {
            that.parents("tr").prev().after(`
    <tr data-translation_source_id="${res.data}" data-question_id="${question_id}">
        <td><input type="radio" class="answer-correct-radio" name="${name}"></td>
        <td class="primary editable" data-lang="<?= $course_language['language_code'] ?>" data-translation_source="answers"></td>
        <?php foreach ($translated_languages as $language_code => $language): ?>
            <td class="editable"
                data-lang="<?= $language_code ?>" data-translation_source="answers"></td>
        <?php endforeach ?>
        <td style="text-align: center;">
            <a class="delete-answer-row btn btn-danger" onclick="return confirm('Are you sure?')">Delete answer</a>
        </td>
    </tr>`)
        })

    })


    // Delete answer row
    $("tbody").on('click', '.delete-answer-row', function () {
        if (!confirm('Are you sure?')) {
            return;
        }

        const answer_row = $(this).parents("tr")
        let answer_id = answer_row.data('translation_source_id');

        // Only run for existing question answer rows
        if (answer_id) {
            answer_row.hide()

            ajax('admin/delete_answer', {answer_id}, function () {
                answer_row.remove();
            }, function () {
                answer_row.show();
            })
        }
    })

    // Delete answer
    $('.delete-question').click(function () {
        if (!confirm('Are you sure?')) {
            return;
        }

        let question_id = $(this).parents('tr').data('question_id')

        ajax('admin/delete_question', {question_id}, RELOAD)
    })

    // Enable editing table
    /* @var $ */
    $(function () {
        $(".table-questions tbody").on('dblclick', '.editable',function () {
            let cell = $(this)
            let originalContent = cell.text();
            let language_code = cell.data('lang');
            let translation_source_id = cell.parents('tr').data('translation_source_id');
            let translation_source = cell.data('translation_source');
            let translation = prompt("Enter new content for:", originalContent).trim();

            if (translation != null) {

                // Send value to back-end
                ajax('admin/translation_edit_dynamic', {
                    translation_source_id,
                    language_code,
                    translation,
                    translation_source
                }, function (res) {
                    cell.removeClass('alert-danger');
                    cell.removeClass('alert-warning');
                    cell.addClass('alert-success');

                    // Color other td yellow if changed cell was in the course's primary language
                    if (cell.hasClass('primary')) {
                        cell.siblings('.editable').removeClass('alert-danger');
                        cell.siblings('.editable').removeClass('alert-success');
                        cell.siblings('.editable').addClass('alert-warning');
                    }

                }, function (res) {
                    if (typeof res !== 'undefined') {
                        show_error_modal(res)
                    }
                    cell.addClass('alert-danger');
                });

                // Write value to table
                $(this).html(translation)
            }
        });
    });

    // Update question category
    $('.question-category').change(function () {
        let question_id = $(this).parents('tr').data('question_id')
        let test_category_id = $(this).val()
        ajax('admin/question_category_edit', {question_id, test_category_id})
    })

    // Update question correct answer
    $(".table-questions").on('change', "input[type='radio']", function(){
        let answer_id = $(this).parents('tr').data('translation_source_id')
        let question_id = $(this).parents('tr').data('question_id')

        ajax('admin/question_correct_answer_edit', {question_id, answer_id})
    })

</script>

