<!-- Main content -->
<section class="content">
    <h1>Test results</h1>
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
            <table id="resultsTable" class="table table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="filter-select filter-exact">Course</th>
                    <th>Language</th>
                    <th>Result</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $result): ?>
                    <tr>
                        <td><a href="results/<?= $result['test_id'] ?>"><?= $result['test_id'] ?></a></td>
                        <td><?= $result['name'] ?></td>
                        <td><?= $result['course_name'] ?></td>
                        <td><?= $result['language_name'] ?></td>
                        <td><?= $result['test_score'] ?>%</td>
                        <td><?= $result['test_created'] ?></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>

<link rel="stylesheet" href="assets/components/datatables/dataTables.bootstrap.css?<?= PROJECT_VERSION ?>">
<script src="assets/components/datatables/jquery.dataTables.js?<?= PROJECT_VERSION ?>"></script>
<script src="assets/components/datatables/dataTables.bootstrap.js?<?= PROJECT_VERSION ?>"></script>

<script>
    $(document).ready(function () {

        $('#resultsTable').DataTable({
            "ordering": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": [[0, 'desc']],
            "bInfo": true,
            "bAutoWidth": true
        });


    });
</script>





