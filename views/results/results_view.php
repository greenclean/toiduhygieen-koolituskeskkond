<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <h1><?= __('Test') ?> #<?= $test_id ?></h1>
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">


                        <?php if ($questions): ?>

                            <h4><?= __('Wrong answer(s)') ?></h4>
                            <table style="width:100%" class="ordered">
                                <tr>
                                    <th><?= __('Number') ?></th>
                                    <th><?= __('Question') ?></th>
                                    <th><?= __('You chose:') ?></th>
                                    <th><?= __('Correct answer') ?></th>
                                    <th><?= __('Explanation') ?></th>
                                </tr>
                                <?php $n = 0;
                                foreach ($questions as $question):$n++; ?>
                                    <tr>
                                        <td><?= $n ?></td>
                                        <td><span><?= $question["question"] ?></span></td>
                                        <td>
                                            <?= __('Option') ?> <?= $question['user_choice_letter'] ?>
                                        </td>

                                        <td><span class="donetext"><?= $question['test_answer_letter'] ?> <br> </span><?= $question['correct_answer'] ?></td>
                                        <td><span><?= $question['explanation'] ?></span></td>

                                    </tr>

                                <?php endforeach ?>
                            </table>
                        <?php else: ?>
                            <caption><?= __('There were no wrong answers') ?></caption>
                        <?php endif ?>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">





