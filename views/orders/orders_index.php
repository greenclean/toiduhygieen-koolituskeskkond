<?php /** @var \Toiduhygieen\Pagination $pagination */ ?>
<div class="row">
    <div class="table-responsive">
        <table class="ordered">
            <caption>
                <h2 class="header">Payments</h2>
                <input type="text" placeholder="Search..." class="otsing" value="<?= $pagination->q ?>">
            </caption>
            <thead>
            <tr>
                <th>ID</th>
                <th>Service</th>
                <th>Payment method</th>
                <th>Sum</th>
                <th>Date</th>
                <th>Name</th>
                <th>Personal code</th>
                <th>Email</th>
                <th>Status</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order): ?>
                <tr>
                    <td><?= $order['order_id'] ?></td>
                    <td><?= $order['course_name'] ?></td>
                    <td><?= $order['payment_method_name'] ?></td>
                    <td><span class="ServiceName">  <?= $order['order_price'] ?></span></td>
                    <td><span class="ServiceDate"><?= $order['order_date'] ?></span></td>
                    <td><span class="ServiceMail"><?= $order['name'] ?></span></td>
                    <td><span class="ServiceMail"><?= $order['personal_code'] ?></span></td>
                    <td><span class="ServiceMail"><?= $order['email'] ?></span></td>
                    <td><?= $order['order_status_name'] ?></td>
                    <td class="look"><span class="look"><a
                                    onclick="event.preventDefault();order_status(1,'<?= $order['order_id'] ?>')"
                                    href="#">Confirm</a></span></td>
                    <td class="look"><span class="look"><a
                                    onclick="event.preventDefault();order_status(2,'<?= $order['order_id'] ?>')"
                                    href="#">Reject</a></span></td>

                </tr>
            <?php endforeach ?>
            </tbody>
        </table>
        <?php require 'templates/_partials/pagination.php' ?>

    </div>
</div>
<style>
    table {
        border-collapse: separate;

    }

    .header {
        width: 40%;
        float: left;
    }

    .otsing {
        float: right;
        margin-top: 20px;
    }

    th {
        border: 1px solid white;
        background-color: #e3e3e3;
        color: #8b96aa;
        font-size: 1.1em !important;
    }

    tr {
        background-color: #f1f1f1;
    }

    tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    td {
        border: solid 1px white;
    }
</style>
<link rel="stylesheet"
      href="assets/components/tablesorter/tablesorter2/dist/css/theme.bootstrap.min.css?<?= PROJECT_VERSION ?>">
<script src="assets/components/tablesorter/tablesorter2/dist/js/jquery.tablesorter.js?<?= PROJECT_VERSION ?>"></script>
<script src="assets/components/tablesorter/tablesorter2/dist/js/jquery.tablesorter.widgets.js?<?= PROJECT_VERSION ?>"></script>
<script>
    function order_status(status, id) {
        $.post("orders/order_status", {status: status, id: id}, function (result) {
            if (result == "OK") {
                window.location.href = window.location.href;
            }
        })
    }

    $('.otsing').change(function () {
        var url  = updateURLParameter(window.location.href, "page", null)
        location.href = updateURLParameter(url, "q", $(this).val())
    });
</script>





