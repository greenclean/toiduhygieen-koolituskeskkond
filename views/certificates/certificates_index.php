<h2><?= __('Certificates') ?></h2>
<table style="width:100%" class="ordered">
    <tr>
        <th><?= __('Number') ?></th>
        <th><?= __('Owner of the certificate') ?></th>
        <th><?= __('Date of issue') ?></th>
        <th><?= __('Course name') ?></th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($certificates as $certificate): ?>
        <tr>
            <td><?= $certificate['certificate_id'] ?></td>
            <td><?= $certificate['name'] ?></td>
            <td><span class="ServiceName"><?= date("Y-m-d", strtotime($certificate['certificate_date'])) ?></span></td>
            <td><span class="ServiceLang"><?= __("$certificate[course_name]", "courses.course_name", $certificate['course_id']) ?></span></td>
            <td>
                <a href="certificates/view_certificate/<?= $certificate['test_id'] ?>">
                    <?= __('Download certificate') ?></a></td>
            <td>
                <a data-toggle="modal" href="#add_email_modal" class="btnOpenEmailModal"
                   data-test_id="<?= $certificate['test_id'] ?>">
                    <?= __('Send to e-mail') ?>
                </a>
            </td>
            <td>
                <a href="payment/index/1">
                    <?= __('Order the certificate in Estonian') ?>
                </a>
            </td>
            <td>
                <a href="payment/index/2">
                    <?= __('Order the certificate in English') ?>
                </a>
            </td>
        </tr>
    <?php endforeach ?>

</table>


<div class="modal fade" id="add_email_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Enter e-mail') ?></h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control user-email" required="required" value="<?= $email ?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button"
                        class="btn btn-primary send-email" <?= $email == '' ? 'disabled' : '' ?>><?= __('Send!') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var test_id;

    $(".user-email").on('keyup change', function () {
        if ($(this).val() == '') {
            $(".send-email").prop('disabled', true);
        } else {
            $(".send-email").prop('disabled', false);
        }
    });

    $(".btnOpenEmailModal").on("click", function () {
        test_id = $(this).data("test_id")
    });

    $(".send-email").click(function () {
        $.post("certificates/send/" + test_id, {
            user_email: $('.user-email').val()
        }, function () {
            alert("<?= __('Certificate has been sent to e-mail') ?>");
            location.reload();
        });
    });
</script>