<style>
    td {
        max-width: 150px;
        word-break: break-all;
    }
</style><h2><?= __('Results') ?></h2>
<?php if ($testScore >= 75): ?>
    <p><?= __('Correct answers') ?>
        <span class="donetext"> <b><?= $NoOfCorrectAnswers ?></b></span>.
        <?= __('Test score') ?>: <?= $testScore . "%" ?></p>
    <p class="text-success"><?= __('You can download your certificate or send it to your e-mail address here:') ?> <a
                href="certificates"> <?= __('Certificates') ?></a></p>
<?php else: ?>
    <?php if ($test['course_repeatable']): ?>
        <p><?= __('Test failed. The test result was') ?> <?= (int)$testScore ?>%.</p>
        <button onclick='location="tests/<?= $test['test_id'] ?>"'
                class="btn btn-primary"><?= __('Take the test again') ?></button>
        <button onclick='location="courses"' class="btn btn-primary"><?= __('Please try again later') ?></button>
    <?php else: ?>
        <p class="text-danger"><?= __('Test failed.') ?></p>
        <p><?= __('Correct answers ') ?> <span
                    class="donetext"><b><?= $NoOfCorrectAnswers ?></b></span>.
            <?= __('Test score') ?>: <?= $testScore . "%" ?></p>
        <button onclick='location="courses"' class="btn btn-primary"><?= __('Back to homepage') ?></button>
    <?php endif ?>
<?php endif ?>
<?php if ($incorrectAnswers): ?>

    <table style="width:100%" class="ordered">
        <caption><?= __('Wrong answer(s)') ?></caption>
        <tr>
            <th><?= __('Number') ?></th>
            <th><?= __('Question') ?></th>
            <th><?= __('You chose:') ?></th>
            <th><?= __('Correct answer') ?></th>
            <th><?= __('Explanation') ?></th>
        </tr>
        <?php $n = 0;
        foreach ($incorrectAnswers as $question):$n++; ?>
            <tr>
                <td><?= $n ?></td>
                <td><span><?= $question['question_text'] ?></span></td>
                <td>
                    <?= __('Option') ?> <?= $question['user_choice'] ?> <br> "<?= $question['user_choice_text'] ?>"
                </td>

                <td>
                    <span class="donetext"><?= __('Option') ?> <?= $question['general']['answer_letter'] ?> <br> "<?= $question['correct_choice_text'] ?>"</span>
                </td>
                <td><span><?= $question['question_explanation_text'] ?></span></td>

            </tr>

        <?php endforeach ?>

    </table>

<?php endif ?>
