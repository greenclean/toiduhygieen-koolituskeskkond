<style>
    ul {
        list-style-type: none;
    }

    .grid--auto-fit {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(400px, 1fr));
    }

    .grid__item {
        padding: 15px;
        margin:3px;
    }

</style>
<?php if ($test): ?>
    <br>
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?= __('There is no time limit.') ?>
        <?php if (!$test['course_repeatable']): ?>
            <?= __('Tests can only be submitted once.') ?>
        <?php endif ?>
        <?= __('If in doubt, save your answers, check the materials and submit later.') ?>
    </div>
    <form id="test" method="post">
        <div>
            <table class="table table-responsive table-striped">
                <tr>
                    <th><?= __('Number') ?></th>
                    <th><?= __('Question') ?></th>
                    <th><?= __('Answer options') ?></th>
                    <th><?= __('Your answer') ?></th>
                </tr>
                <?php $n = 0;
                foreach ($test['questions'] as $question): $n++ ?>
                    <tr data-question_id="<?= $question['question_id'] ?>"
                        data-test_id="<?= $test['test_id'] ?>">
                        <td><?= $n ?></td>
                        <td><span class="FirstNameLastName"><?= $question['translations'][$language_code] ?></span></td>
                        <td>
                            <div class="ServiceName">
                                <ul class="options">
                                    <?php foreach ($question['answers'] as $answer): ?>
                                        <li>
                                            <?= $answer['answer_letter'] ?>
                                            . <?= $answer['translations'][$language_code] ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </td>

                        <td>
                            <span class="ServiceIsik">
                                <select class="answers validate"
                                        name="chosenAnswerLetter[<?= $question['question_id'] ?>]"
                                        data-answer_id = "<?= $answer['answer_id'] ?>">
                                    <option value=""><?= __('Choose an answer') ?></option>
                                    <?php foreach ($question['answers'] as $answer): ?>
                                        <option value="<?= $answer['answer_letter'] ?>" <?= $answer['answer_letter'] === $answer['answer_chosen'] ? 'selected' : '' ?>>
                                            <?= $answer['answer_letter'] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </span>
                        </td>

                    </tr>

                <?php endforeach ?>

            </table>
        </div>
        <div class="grid--auto-fit">
            <button type="submit" class="grid__item btn btn-success submitAnswers" name="testSubmit"
                    formaction="tests/view_results/<?= $this->params[0] ?>"><?= __('Submit the test and see the results') ?>
            </button>
            <button type="submit" class="grid__item btn btn-secondary pull-right"
                    formaction="tests/save_questions/<?= $this->params[0] ?>"><?= __('Save your answers and continue later') ?>
            </button>
        </div>
    </form>
<?php else: ?>
    <?= __('Course has not been purchased') ?>

<?php endif ?>
<script>

    $('select.answers').on('blur', function () {
        if ($(this).val() === '') {
            $(this).addClass('has-error')
        } else {
            $(this).removeClass('has-error')
        };
    });

    $('.submitAnswers').click(function () {

        $(".validate").each(function () {
            if ($(this).val() === '') {
                $(this).addClass("has-error")
            }
        });
        if ($(".has-error").length > 0) {
            alert('<?= addcslashes(__('Correct the boxes which are red'), "'")?>');
            return false;

        }

    });

    $("tbody").on('change', '.answers', function () {
        let answer_id = $(this).data('answer_id')
        let question_id = $(this).parents('tr').data('question_id')
        let test_id = $(this).parents('tr').data('test_id')
        let answer_letter = $(this).val()
        ajax('tests/save_user_choice', {answer_id, question_id, test_id, answer_letter})
    })

    $("#test").submit(function () {
        setTimeout(function () {
            $(".submitAnswers").attr("disabled", true);
        }, 0);
        return true;
    });
</script>