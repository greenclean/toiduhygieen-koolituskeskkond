<h2><?= __('Learning materials') ?></h2>
<?php if ($makstud_koolitus): ?>

    <?php foreach ($learning_material as $material): ?>
        <p>
            <a href="learn/download_material?id=<?= $material['learning_material_id'] ?>"
               target="_blank" download><?= $material['learning_material_name'] ?></a>
        </p>
    <?php endforeach ?>

    <br>
    <div class="row">
        <div class="col-md-8">
            <a href="tests" class="btn btn-success pull-right"><?= __('Test') ?></a>
        </div>
    </div>
<?php else: ?>


    <div class="row">
        <div class="col-md-8">
            <a href="courses" class="btn btn-success pull-left"><?= __('Choose a course') ?></a>
            <a href="#" class="btn btn-secondary pull-right"
               onClick="history.go(-1); return false;"><?= __('Go back') ?></a>
        </div>
    </div>
<?php endif ?>