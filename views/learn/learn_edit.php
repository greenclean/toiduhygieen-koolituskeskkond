<h2>Uploading materials</h2>
<form enctype='multipart/form-data' method='post' accept-charset="UTF-8">
    <input type='file' name='filename' accept="application/pdf"/>
    <input type='submit' name='submit' value='Upload'>
    <select name="language" id="txtRegisterLang" class="suhtluskeel">
        <option value="">Course language</option>
        <?php foreach ($languages as $language): ?>
            <option value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
        <?php endforeach; ?>
    </select>

</form>

<table style="width:100%" class="ordered">
    <caption>
        <h2>Learning materials</h2>
    </caption>
    <tr>
        <th>ID</th>
        <th>Study material</th>
        <th>Added</th>
        <th>Language</th>
        <th>Action</th>
    </tr>
    <?php foreach ($learning_material as $material): ?>
        <tr data-id="<?= $material['learning_material_id'] ?>">
            <td><?= $material['learning_material_id'] ?></td>
            <td>
                <a href="learn/download_material?id=<?= $material['learning_material_id'] ?>"><?= $material['learning_material_name'] ?></a>
            </td>
            <td><?= $material['learning_material_date_added'] ?></td>
            <td>
                <?php
                foreach ($languages as $lang) {
                    if ($lang['language_id'] == $material['language_id']) {
                        echo $lang['language_name'];
                    }
                }
                ?>

            </td>
            <td class="look"><span class="look"><a
                            onclick="event.preventDefault();delete_material('<?= $material['learning_material_name'] ?>','<?= $material['learning_material_id'] ?>')"
                            href="<?= BASE_URL ?>">Delete</a></span></td>
        </tr>

    <?php endforeach ?>
</table>
<script>
    function delete_material(name, id) {

        if (!confirm('<?=addcslashes(__('Are you sure?'), "'")?>')) {
            return;
        }

        $.post("learn/delete_material", {name: name, id: id}, function (result) {
            if (result === "Ok") {

                // Remove this material row from DOM
                $('tr[data-id="' + id + '"]').remove();
            }
        })
    }
</script>


