<div class="data">
    <h3><?= __('Enter the e-mail address which you used to register on the site') ?></h3>
    <br>
    <div id="login" class="tab-pane fade show active">
        <form method="post">
            <input class="email validate" name="email" type="email" id="txtLoginEmail" placeholder="<?= __('E-mail') ?>"><br/>
            <input type="submit" value="<?= __('Send!') ?>" id="btnRegister">
        </form>
        <p><?= __('New password will be sent to the e-mail address') ?></p>
    </div>


</div>