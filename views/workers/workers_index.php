<!-- DataTables -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<style>
    .row {
        width: 100%;
    }
    .first-add-new-employee-form {
        padding-left: 0;
    }
    .dataTable thead th {
        padding: 10px !important;
    }
    .change-button {
        text-align: end;
        padding-right: 0 !important;
    }
    .container {
        padding-left: 38px;
    }
    .site-main-footer {
        padding-left: 0;
    }

</style>
<div class="row">
    <h2><?= __('Employees') ?></h2>


    <div class="row">
        <div class="col-md-11">
            <h4 class="text-left"><?= __('Instructions') ?></h4>
            <ol style="text-align: left">
                <li><?= __('Tick the box in front of the employee whom you wish to purchase the course for.') ?></li>
                <li><?= __('Scroll down until you see the selection of courses, and choose one') ?>.</li>
                <li><?= __('Then press "Buy course"') ?>.</li>
            </ol>
        </div>
    </div>

    <form id="main" method="post">
        <table class="table table-hover" id="workers-table">

            <thead>
            <tr>
                <th><input type="checkbox" class="check-all-checkboxes"></th>
                <th><?= __('No') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Personal code') ?></th>
                <th><?= __('Language') ?></th>
                <th><?= __('E-mail') ?></th>
                <th><?= __('Courses') ?></th>
                <th><?= __('supervisor') ?></th>
                <th class="text-right"><?= __('Action') ?></th>
            </tr>
            </thead>

            <tbody>
            <?php $i = 0; ?>
            <?php foreach ($workers as $worker): ?>
                <?php
                if ($i < count($workers)) {
                    $i++;
                }
                ?>
                <tr <?= $i ?>>
                    <td>
                        <?php if ($worker['user_id'] != $auth->user_id) { ?>
                            <input type="checkbox"
                                   class="check-worker"
                                   name="user_id[<?= $worker['user_id'] ?>]"
                                   value="1"
                                <?= isset($_SESSION['selected_workers'][$worker['user_id']]) ? 'checked' : '' ?>
                            >
                        <?php } else { ?>
                            <input type="checkbox"
                                   class="check-worker"
                                   name="user_id[<?= $worker['user_id'] ?>]"
                                   value="1"
                                <?= isset($_SESSION['selected_workers'][$worker['user_id']]) ? 'checked' : '' ?>
                            >
                        <?php } ?>
                    </td>
                    <td><?= $i ?></td>
                    <td><?= $worker['name'] ?></td>
                    <td class="<?= $worker['faulty_personal_code'] ? "red" : "" ?>"><?= $worker['personal_code'] ?></td>
                    <td>
                        <?php foreach ($languages as $language): ?>
                            <?php

                            if ($language['language_id'] == $worker['language_id']) {
                                echo $language['language_name'];
                            }

                            ?>
                        <?php endforeach; ?>
                    </td>
                    <td><?= $worker['email'] ?></td>
                    <td>
                        <?php foreach ($user_courses[$worker['user_id']] as $user_course): ?>
                            <span class="badge <?= $user_course['label_class_name'] ?>">
                            <?= $user_course['course_name'] ?>
                        </span>
                        <?php endforeach; ?>
                    </td>
                    <td>
                        <?php if ($worker['user_id'] != $auth->user_id) { ?>
                            <?= $workers[0]['name']; ?>
                        <?php } ?>
                    </td>
                    <td width="12%" class="change-button">
                        <a href="#" class="muuda btn btn-success"
                           data-id="<?= $worker['user_id'] ?>"><?= __('Change') ?></a>
                        <?php if ($worker['user_id'] != $auth->user_id) { ?>
                            <a href="workers/delete_worker/<?= $worker['user_id'] ?>" class="delete btn btn-danger"
                               onclick="return confirm('Are you sure you want to remove <?= $worker['name'] ?> from your employees list?')"><i
                                        class="bi bi-trash" aria-hidden="true"></i></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </form>
    <span class="badge badge-warning"><?= __('Not started') ?></span>
    <span class="badge badge-success"><?= __('Passed') ?></span>
    <span class="badge badge-danger"><?= __('Failed') ?></span>
    <span class="badge badge-primary"><?= __('In progress') ?></span>
    <span class="badge badge-secondary"><?= __('Payment unconfirmed') ?></span>
    <span class="badge badge-info"><?= __('Certificate') ?></span>
    <span class="badge badge-dark"><?= __('Declined') ?></span>
</div>

<br>

<div class="row">
    <h2 style="margin-top: 0"><?= __('Choose a course') ?></h2>

    <div class="form-group col-sm-10">
        <select class="form-control selected-course-id" title="<?= __('Language') ?>">
            <?php $i = 0 ?>
            <?php foreach ($courses as $course): ?>
                <?php if ($course['course_type'] == 'course') { ?>
                    <?php $i++ ?>
                    <option value="<?= $course['course_id'] ?>"><?= __($course['course_name'], 'courses.course_name', $course['course_id']) ?></option>
                <?php } ?>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group col-sm-2 text-center">
        <input type="button" class="btn btn-success buy-course  <?= (!$i) ? 'disabled' : '' ?>"
               value="<?= __('Buy course') ?>">
    </div>
    <div class="col-md-12">
        <h4 class="text-left"><?= __('Instructions') ?></h4>
        <ol style="text-align: left">
            <li><?= __('Choose a course from the drop-down menu') ?></li>
            <li><?= __('Then press "Buy course"') ?>.</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-left"><?= __('Send instructions to employee') ?></h4>
            <input type="button" class="btn btn-success send-guide"
                   value="<?= __('Send instructions') ?>">
        </div>
    </div>
</div>
<div class="row">
    <h2 style="margin-top: 0"><?= __('Add new employee') ?></h2>
</div>
<div class="row">

    <div class="form-group col-sm-3 first-add-new-employee-form">
        <input class="form-control new-worker-name" placeholder="<?= __('First & last name') ?>">
    </div>
    <div class="form-group col-sm-3">
        <input class="form-control new-worker-personal-code" placeholder="<?= __('Personal code') ?>">
        <p class="new-worker-input-info"><?= __('If you don\'t have a personal code, please insert your date of birth (YYYY-MM-DD)') ?></p>
    </div>
    <div class="form-group col-sm-3">
        <input class="form-control new-worker-email" type="email" placeholder="<?= __('Email') ?>">
        <p class="new-worker-input-info"><?= __('Add your HR manager\'s email address or your own personal e-mail. Email address will be used for receiving instructions and certificates.') ?></p>
    </div>
    <div class="form-group col-sm-2">
        <select class="form-control new-worker-language-id" title="<?= __('Language') ?>">
            <?php foreach ($languages as $language): ?>
                <option value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group col-sm-1">
        <input type="button" class="btn btn-success add-new-worker" value="<?= __('Add') ?>">
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"><?= __('Edit user information') ?></h2>
            </div>
            <div class="modal-body">
                <h4><?= __('Fill in the fields') ?></h4>

                <input type="hidden" name="user_id" id="id_text">

                <div class="form-group">
                    <div class="accept_decline">
                        <div>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name"><?= __('Name:') ?></label>
                    <input maxlength="148" type="text" class="form-control"
                           name="data[name]" id="name">
                </div>
                <div class="form-group">
                    <label for="name"><?= __('Personal code:') ?></label>
                    <input type="text" class="form-control"
                           name="data[personal_code]" id="personal_code">
                </div>
                <div class="form-group">
                    <label for="name"><?= __('Email:') ?></label>
                    <input type="email" class="form-control"
                           name="data[email]" id="email">
                </div>
                <div class="form-group">
                    <label for="languages"><?= __('Communication language:') ?></label><br>
                    <select name="data[language_id]" id="language_id">
                        <?php foreach ($languages as $language): ?>
                            <option
                                    value="<?= $language['language_id'] ?>"><?= $language['language_name'] ?></option>
                        <?php endforeach ?>
                    </select>

                </div>

                <div class="modal-footer">
                    <button type="button" name="save" class="btn btn-success edit-worker"><?= __('Save') ?></button>
                    <button type="button" class="btn btn-secondary pull-right"
                            data-dismiss="modal"><?= __('Cancel') ?>
                    </button>
                </div>

            </div>

        </div>

    </div>
</div>
<script>

    // Remove invisible characters
    function removeWhitespace(text) {
        return text && text.replace(/[\u200E\u200F\u202A-\u202C]/g, "");
    }

    // Remove all characters except - and integers
    function sanitizePersonalCode(personalCode) {
        return personalCode && personalCode.replace(/[^0-9-]+/g, "");
    }

    // Prevent empty user
    function inputsFilled() {
        return $('.new-worker-name').val() !== '' && $('.new-worker-email').val() !== '' && $('.new-worker-personal-code').val() !== '';
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $(document).ready(function ($) {
        // Initialize datatable
        $('#workers-table').DataTable({
            "paging": false,
            "ordering": false
        });

        // Add new workers
        $('.add-new-worker').on('click', function () {

            if (!inputsFilled()) {
                alert("<?= __('Missing info!')?>");
                return;
            }

            email = removeWhitespace($('.new-worker-email').val());

            if (!validateEmail(email)) {
                $('.new-worker-email').css('border-color', 'red');
                return false;
            }

            ajax('workers/new', {
                'name': removeWhitespace($('.new-worker-name').val()),
                'email': email,
                'personal_code': sanitizePersonalCode($('.new-worker-personal-code').val()),
                'language_id': $('.new-worker-language-id').val()

            }, RELOAD);
        })

        $('.edit-worker').on('click', function () {

            email = removeWhitespace($('#email').val());

            if (!validateEmail(email)) {
                $('#email').css('border-color', 'red');
                return false;
            }

            ajax('workers/worker_save', {
                'user_id': $('#id_text').val(),
                'data': {
                    'name': removeWhitespace($('#name').val()),
                    'email': email,
                    'personal_code': sanitizePersonalCode($('#personal_code').val()),
                    'language_id': $('#language_id').val()
                }
            }, RELOAD);
        })
    });

    // Check/uncheck all checkboxes
    $('.check-all-checkboxes').on('click', function () {
        $('.check-worker').each(function () {
            $(this).click();
        });
    });


    // Redirect user to selected course payment
    $('.buy-course').on('click', function () {
        if (!$(this).hasClass('disabled')) {
            var formMain = $('#main');
            formMain.attr('action', "payment/index/" + $('.selected-course-id').val());
            formMain.submit();
        }
    });
    // Redirect user to selected course payment
    $('.send-guide').on('click', function () {
        ajax('workers/send_guide', $('#main').serializeArray(), function (json) {
            if (json.status === "200") {
                alert("<?= __('Instructions sent')?>");
                location.reload();
            } else {
                console.log(json);
                alert("<?= __('An error occurred')?>");
            }
        })
    });

    $(".muuda").on('click', function (e) {
        e.preventDefault();
        var auth_user_id = '<?= $auth->user_id ?>';
        var user_id = $(this).attr('data-id');
        $('.accept_decline div').remove();

        $.post("workers/get_user", {user_id: user_id}, function (res) {
            if (res.result == "Ok") {
                $("#id_text").val(res.data.user_id);
                $("#name").val(removeWhitespace(res.data.name));
                $("#personal_code").val(sanitizePersonalCode(res.data.personal_code));
                $("#language_id").val(res.data.language_id);
                $("#email").val(removeWhitespace(res.data.email));

                $("#myModal").modal("show");

                if (auth_user_id == user_id) {
                    var notification = null;

                    <?php if($auth->employer_id){ ?>
                    notification = '<?= $team_lead["name"] ?> <a href="workers/delete_teamlead" class="delete btn btn-danger" onclick=" return confirm(\"<?= __('Are you sure you want to leave?') . ' ' . $team_lead["name"] . ' ' . __('team')?>") "><i class="bi bi-trash" aria-hidden="true"></i></a>';
                    <?php } ?>

                    if (notification) {
                        $('.accept_decline').append('<div>' + notification + '</div>');
                    }
                }

            } else {
                alert("Kasutaja andmete kuvamisel tekkis probleem!")
            }
        })

    })

</script>