<style>
    .unconfirmed-order:disabled, .unconfirmed-order:disabled:hover {
        background-color: #D65B3C;
        opacity: 1;
    }

    .teenuseblokk {
        text-align: center;
    }

    .btn {
        font-size: 20px;
    }
</style>
<h3><?= __('Courses') ?></h3>
<div class="row">
    <?php foreach ($this->courses as $course): ?>
        <div class="col-lg-4 col-md-4 col-sm-12 teenuseblokk<?= $course['course_id'] ?> teenuseblokk">
            <h2 style="min-height: 112px;"><?= __($course['course_name'], "courses.course_name", $course['course_id']) ?></h2>

            <p class="valgehind"><?= empty($course['course_price']) ? '&nbsp;' : $course['course_price'] . ' + km' ?></p>

            <?php if ($course['course_status'] == 'completed'): ?>
                <button onclick='location="certificates/view_certificate/<?= $course['test_id'] ?>"'
                        class="btn btn-primary">
                    <i class="bi bi-cloud-arrow-down"></i> <?= __('Certificate') ?>
                </button>
                <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-success">
                    <i class="bi bi-cart3"></i> <?= __('Repeat test') ?>
                </button>
            <?php elseif ($course['course_status'] == 'purchased'): ?>
                <button onclick='location="learn"' class="btn btn-primary">
                    <i class="bi bi-book"></i> <?= __('Learn') ?>
                </button>
                <button onclick='location="tests"' class="btn btn-primary">
                    <i class="bi bi-pencil"></i> <?= __('Test') ?>
                </button>
            <?php elseif ($course['course_status'] == 'started'): ?>
                <button onclick='location="learn"' class="btn btn-primary">
                    <i class="bi bi-book"></i> <?= __('Learn') ?>
                </button>
                <button onclick='location="tests/<?= $course['test_id'] ?>"' class="btn btn-primary">
                    <i class="bi bi-pencil"></i> <?= __('Continue') ?>
                </button>
            <?php elseif ($course['course_status'] == 'unconfirmed'): ?>
                <button class="btn btn-danger unconfirmed-order" disabled>
                    <?= __('Order is being confirmed') ?>
                </button>
            <?php elseif ($course['course_status'] == 'user_has_unused_english_certificate_version'): ?>
                <button class="btn btn-danger unconfirmed-order" disabled>
                    <?= __('Already bought') ?>
                </button>
            <?php elseif ($course['course_status'] == 'failed'): ?>
                <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-primary">
                    <i class="bi bi-cart3"></i> <?= __('buy another test') ?>
                </button>
            <?php else: ?>
                <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-success">
                    <i class="bi bi-cart3"></i> <?= __('buy') ?>
                </button>
            <?php endif; ?>

        </div>
    <?php endforeach; ?>
</div>
