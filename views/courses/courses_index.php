<?php use Toiduhygieen\User;

?>

<style>
    .unconfirmed-order:disabled, .unconfirmed-order:disabled:hover {
        background-color: #D65B3C;
        opacity: 1;
    }

    .teenuseblokk {
        text-align: center;
    }

    .btn {
        font-size: 20px;
    }

    .grid--auto-fit {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
    }

    .grid__item {
        padding: 1.5em;
        margin:3px;
    }

</style>
    <h2><?= __('Courses') ?></h2>

    <div class="grid--auto-fit">
        <?php foreach ($this->courses as $course): ?>
            <?php  if($course['course_type'] == 'course'):  ?>
            <div class="grid__item teenuseblokk<?= $course['course_id'] ?> teenuseblokk">
                <h2 style="min-height: 112px;"><?= __($course['course_name'], "courses.course_name", $course['course_id']) ?></h2>
                <p class="valgehind">
                <?php if (in_array($course['course_status'],['unconfirmed', 'started', 'purchased'])): ?>
                    &nbsp;
                <?php else: ?>
                    <?= empty($course['course_price']) ? '&nbsp;' : $course['course_price'] . ' + km' ?>
                <?php endif; ?>
                </p>
                <?php if ($course['course_status'] == 'completed'): ?>
                    <button onclick='location="certificates/view_certificate/<?= $course['test_id'] ?>"'
                            class="btn btn-success">
                        <span class="bi bi-cloud-download"></span> <?= __('Certificate') ?>
                    </button>
                    <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-success">
                        <i class="bi bi-cart3"></i> <?= __('Repeat test') ?>
                    </button>
                <?php elseif ($course['course_status'] == 'purchased'): ?>
                    <button onclick='location="learn"' class="btn btn-success">
                        <i class="bi bi-book"></i> <?= __('Learn') ?>
                    </button>
                    <button onclick='location="tests"' class="btn btn-success">
                        <i class="bi bi-pencil"></i> <?= __('Test') ?>
                    </button>
                <?php elseif ($course['course_status'] == 'started'): ?>
                    <button onclick='location="learn"' class="btn btn-success">
                        <span class="bi bi-book"></span> <?= __('Learn') ?>
                    </button>
                    <button onclick='location="tests/<?= $course['test_id'] ?>"' class="btn btn-success">
                        <i class="bi bi-pencil"></i> <?= __('Continue') ?>
                    </button>
                <?php elseif ($course['course_status'] == 'unconfirmed'): ?>
                    <button class="btn btn-danger unconfirmed-order" disabled>
                        <?= __('Order is being confirmed') ?>
                    </button>
                <?php elseif ($course['course_status'] == 'user_has_unused_english_certificate_version'): ?>
                    <button class="btn btn-danger unconfirmed-order" disabled>
                        <?= __('Already bought') ?>
                    </button>
                <?php elseif ($course['course_status'] == 'failed' && !empty($_SESSION['user']['employer_id'])): ?>
                    <p> <?= __('Contact your employer to take the retest') ?></p>
                <?php elseif ($course['course_status'] == 'failed'): ?>
                    <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-success">
                        <i class="bi bi-cart3"></i> <?= __('buy another test') ?>
                    </button>
                <?php else: ?>
                    <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-success">
                        <i class="bi bi-cart3"></i> <?= __('buy') ?>
                    </button>
                <?php endif; ?>

            </div>
            <?php  endif; ?>
        <?php endforeach; ?>
    </div>

<br>
    <?php if (User::hasCertificates($this->auth->user_id)): ?>
    <div class="grid--auto-fit" >
        <h2><?= __('Certificates') ?></h2>
        <br>
        <?php foreach ($this->courses as $course): ?>
            <?php   if($course['course_type'] !== 'course'):  ?>
            <div class="grid__item teenuseblokk<?= $course['course_id'] ?> teenuseblokk">
                <h2 style="min-height: 112px;"><?= __($course['course_name'], "courses.course_name", $course['course_id']) ?></h2>

                <p class="valgehind"><?= empty($course['course_price']) ? '&nbsp;' : $course['course_price'] . ' + km' ?></p>

                <?php if ($course['course_status'] == 'unconfirmed'): ?>
                    <button class="btn btn-danger unconfirmed-order" disabled>
                        <?= __('Order is being confirmed') ?>
                    </button>
                <?php elseif ($course['course_status'] == 'user_has_unused_english_certificate_version'): ?>
                    <button class="btn btn-danger unconfirmed-order" disabled>
                        <?= __('Already bought') ?>
                    </button>
                <?php else: ?>
                    <button onclick='location="payment/index/<?= $course['course_id'] ?>"' class="btn btn-success">
                        <i class="bi bi-cart3"></i> <?= __('buy') ?>
                    </button>
                <?php endif; ?>

            </div>
            <?php  endif; ?>
        <?php endforeach; ?>
    </div>
    <?php  endif; ?>

<?php if ($payment_method_id): ?>

    <div class="modal fade" id="modal-paid">
        <div class="modal-dialog">
            <?php switch ($payment_method_id): ?>
<?php case PAYMENT_BY_INVOICE: ?>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?= __('Thank you, your order is being confirmed!') ?></h4>
                        </div>
                        <div class="modal-body">
                            <?= __('Invoice request has been sent and your order is being confirmed') ?>.
                            <ol>
                                <li><?= __('Check your e-mail for the invoice') ?>.</li>
                                <li><?= __('Pay the invoice') ?>.</li>
                                <li><?= __('Wait for the confirmation that we received the payment, on your e-mail') ?>
                                    .
                                </li>
                                <li><?= __('Log in and start using the service') ?>.</li>
                            </ol>

                            <?= __('Invoice is sent by e-mail on Mon-Fri between 9am - 17pm') ?>.
                        </div>
                        <div class="modal-footer" style="text-align: center !important;">
                            <button type="button" class="btn btn-sm btn-secondary"
                                    data-dismiss="modal"><?= __('I understand') ?></button>
                        </div>
                    </div><!-- /.modal-content -->
					<script>
						$(document).ready(function () {
							gtag('event', 'SendInvoice', {
							  'event_category': 'PaymentByInvoiceForm'
							});
						});
					</script>
                    <?php break; ?>
                <?php case PAYMENT_BY_BANK_TRANSFER: ?>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?= __('Thank you. You chose to pay by transfer, here are the instructions:') ?></h4>
                        </div>
                        <div class="modal-body">
                            <ol>
                                <li>
                                    <?= __('Select the bank you wish to use for transfer. There is no time limit to do the transfer') ?>
                                    .
                                </li>
                                <li>
                                    <?= __('Make a payment to: Greenclean OÜ , account number: EE371010220088591013, explanation: YOUR PERSONAL CODE') ?>
                                    .
                                </li>
                                <li>
                                    <?= __('Wait for confirmation on your e-mail. Confirmation will be sent once we have received the payment') ?>
                                    .
                                </li>
                                <li>
                                    <?= __('Once you have received the confirmation, log in to the site and start using the service') ?>
                                    .
                                </li>
                            </ol>
                        </div>
                        <div class="modal-footer" style="text-align: center !important;">
                            <button type="button" class="btn btn-sm btn-secondary"
                                    data-dismiss="modal"><?= __('I understand') ?></button>
                        </div>
                    </div><!-- /.modal-content -->
					<script>
						$(document).ready(function () {
							gtag('event', 'StartBankPayment', {
							  'event_category': 'PaymentByBankTransfer'
							});
						});
					</script>
                <?php endswitch; ?>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script>
        $(document).ready(function () {
            $('#modal-paid').modal('show');
        });
    </script>

<?php endif; ?>