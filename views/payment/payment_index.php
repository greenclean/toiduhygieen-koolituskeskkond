<style>
    th:nth-child(2), td:nth-child(3), td:last-child {
        text-align: center;
    }

    .panel-body {
        text-align: center;
    }

    #payButton {
        border: none;
        background: transparent;
    }

    .last {
        text-align: right;
    }

    tfoot tr td:first-child {
        text-align: right;
    }

    .banklink-payment-method {
        padding: 7px 14px;
        cursor: pointer;
        background: #8b96aa;
        border-radius: 3px;
        padding-bottom: 9px;
    }

    a {
        color: white;
    }

    h4 {
        margin-bottom: 5px;
    }

</style>
<h2><?= __('Order summary') ?></h2>

<div class="order-review" id="checkout-review-load">
    <div id="checkout-review-table-wrapper">

        <?php if (empty($_SESSION['selected_workers'])): ?>
            <div class="alert alert-danger"><?= __('Error') ?></div>
        <?php endif ?>

        <table class="table table-bordered table-striped">
            <thead>
            <tr class="first">
                <th rowspan="1"><?= __('User') ?></th>
                <th rowspan="1"><?= __('Description') ?></th>
                <th colspan="1" class="a-center"><?= __('Price') ?></th>
                <th rowspan="1" class="a-center"><?= __('Quantity') ?></th>
                <th colspan="1" class="a-center"><?= __('Sum') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($_SESSION['selected_workers'] as $worker): ?>
                <tr class="first">
                    <td><?= $worker['name'] ?> (<?= $worker['personal_code'] ?>)</td>
                    <td><?= __($worker['course']['course_name'], "courses.course_name", $worker['course']['course_id']) ?></td>
                    <td class="a-right" data-rwd-label="Price">
                        <span class="cart-price">
                                <span class="price"><?= $worker['course']['course_price'] ?>€</span>
                        </span>
                    </td>
                    <td class="a-center" data-rwd-label="Qty">1</td>
                    <!-- sub total starts here -->
                    <td class="a-right last" data-rwd-label="Subtotal">
                    <span class="cart-price">
                            <span class="price"><?= $worker['course']['course_price'] ?>€</span>
                    </span>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4">
                    <?= __('VAT') ?> <?= VAT_PERCENT ?>%</td>
                <td colspan="1">
                    <?= $vat_price ?>€
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <?= __('Total') ?></td>
                <td colspan="1">
                    <?= $total_price ?>€
                </td>
            </tr>

            </tfoot>
        </table>


    </div>

</div>

<h2><?= __('Payment') ?></h2>
<div class="panel-group" id="accordion">
    <?php if ($total_price != 0): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion"
                   href="#collapse1"><?= __('Payment via banklink') ?></a>
            </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-11">
                        <h4 class="text-left"><?= __('Instructions') ?></h4>
                        <ol style="text-align: left">
                            <li><?= __('From the list below, choose the bank you wish to use to make the payment. You will be redirected to the bank site.') ?>
                            </li>
                            <li><?= __('Log in to bank using your bank account') ?>.</li>
                            <li><?= __('Make the transfer and press the <b>Back to merchant</b> button') ?>.</li>
                            <li><?= __('Start using the bought service') ?>.</li>
                        </ol>
                    </div>

                </div>
                <p id="maksan"><h4><?= __('Choose a bank to continue') ?></h4></p>
                <div id="banklink-payment-methods" data-value="<?= $this->params[0] ?>">
                    <a data-value="Swedbank" class="banklink-payment-method">Swedbank</a>
                    <a data-value="Seb" class="banklink-payment-method">SEB</a>
                    <a data-value="Lhv" class="banklink-payment-method">LHV</a>
                    <a data-value="Nordea" class="banklink-payment-method">Luminor</a>
                    <a data-value="Krediidipank" class="banklink-payment-method">Coop</a>
                    <a data-value="Pocopay" class="banklink-payment-method">Pocopay</a>
                    <br>
                </div>
                <br>
            </div>
        </div>

    </div>
    <?php endif; ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><?= __('Payment by invoice') ?></a>
            </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-11">
                        <h4 class="text-left"><?= __('Instructions') ?></h4>
                        <ol style="text-align: left">
                            <li><?= __('If you wish to have the invoice under company name, fill the blank with company name') ?>.</li>
                            <li><?= __('Press the <b>Send invoice</b> button') ?>.</li>
                            <li><?= __('Wait for our invoice to your e-mail address') ?>.</li>
                            <li><?= __('Pay the invoice') ?>.</li>
                            <li><?= __('Wait for the confirmation that we received the payment, on your e-mail') ?>
                                .
                            </li>
                            <li><?= __('Log in and start using the service') ?>.</li>
                        </ol>
                    </div>

                </div>


                <form id="email" action="payment/orders" class="form email" method="post" role="form">
                    <p><h4><?= __('Thank You. Your invoice will be sent to your e-mail address from Mon-Fri 9am - 17pm.') ?></h4>

                    <div class="form-group">
                        <label class="sr-only" for="">label</label>
                        <input type="text" class="form-control" name="company_name"
                               placeholder="<?= __('Please enter the company name (who will receive the invoice)') ?>"/> <br/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="id" value="<?= $this->params[0] ?>">
                        <button name="saada" class="btn btn-success" type="submit"><?= __('Send invoice') ?></button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>
<script>

    $(function () {

        $('.banklink-payment-method').on('click', function () {
            ajax('payment/choose_payment_method', {
                    id: $('#banklink-payment-methods').data('value'),
                    method: $(this).data('value')

                }, function(json) {
                    window.location.href = json.data
                }
            )
        })
    });

</script>
