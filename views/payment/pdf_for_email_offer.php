<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
            background-color: white;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

    </style>
</head>
<body>

<div class="invoice-box">
    <table>
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Arve number: #<?= $selected_workers['invoice_nr'] ?><br>
                            <?php if ($selected_workers['payment_method_id'] == 1) : ?>
                                Arve väljastatud: <?= $selected_workers['order_date'] ?><br>
                            <?php else : ?>
                                Arve väljastatud ja makstud: <?= $selected_workers['order_date'] ?><br>
                            <?php endif; ?>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="information">
            <td width="65%">
                ARVE VÄLJASTAJA <br>
                Greenclean OÜ <br>
                Paadi tee 3 Haabneeme Viimsi Harjumaa<br>
                KMKR EE101249841<br>
                Reg No 11530451<br>
                Pank: SEB Pank IBAN EE371010220088591013<br>
                Telefon 6700194 Gsm 59129102<br>
                <?= __('info@kohustuslikkoolitus.ee') ?><br>
                <?= __('www.kohustuslikkoolitus.ee') ?><br>
                <?= __('www.toiduhügieen.ee') ?>
            </td>
            <?php if($selected_workers['count_workers']): ?>
                <td></td>
            <?php endif; ?>
            <td width="35%" style="text-align: right">
                KLIENT <br>
                <?= (isset($selected_workers['company_name']) ? $selected_workers['company_name'] : '') ?> <br>
                <?= $this->auth->name ?><br>
                <?= $this->auth->email ?><br>
            </td>
        </tr>
    </table>
    <table>
        <tr class="heading">
            <?php if($selected_workers['count_workers']): ?>
                <td width="20%">
                    Kasutaja
                </td>
            <?php endif ?>
            <td width="75%" style="text-align: left">
                Teenuse nimetus
            </td>
            <td width="5%" style="text-align: right">
                Hind
            </td>
        </tr>

        <?php if($selected_workers['count_workers']): ?>
            <?php foreach ($selected_workers['workers'] as $worker): ?>
                <tr class="item">
                    <td width="20%">
                        <?= $worker['user']['name'] ?>
                    </td>
                    <td width="75%" style="text-align: left">
                        <?php $repurchase = $worker['course']['course_status'] == 'completed' || $worker['course']['course_status'] == 'failed'; ?>
                        <?= __($worker['course']['course_name'], "courses.course_name", $worker['course']['course_id']) . $repurchase ? '(' . __('repurchase course') . ')' : ' ' ?>
                    </td>
                    <td style="text-align: right" width="5%">
                        <?= $worker['course']['course_price'] ?>€
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr class="item">
                <td width="75%">
                    <?php $repurchase = $selected_workers['course']['course_status'] == 'completed' || $selected_workers['course']['course_status'] == 'failed'; ?>
                    <?= __($selected_workers['course']['course_name'], "courses.course_name", $selected_workers['course']['course_id']) . $repurchase ? '(' . __('repurchase course') . ')' : ' ' ?>
                </td>
                <td style="text-align: right">
                    <?= $selected_workers['course']['course_price'] ?>€
                </td>
            </tr>
        <?php endif ?>
    </table>
    <div style="text-align: right">
        <?php if($selected_workers['count_workers']): ?>
            <?php
            $total_course_summa = 0;
            $order_total_price = 0;
            foreach ($selected_workers['workers'] as $worker) {
                $total_course_summa += $worker['course']['course_price'];
                $order_total_price += $worker['order']['order_price'];
            }
            ?>
            <div>
                Summa: <?= ($total_course_summa) ?>€
            </div>
            <div>
                Käibemaks <?= VAT_PERCENT ?>%: <?= round($total_course_summa * VAT_PERCENT / 100, 2) ?>€
            </div>
            <div>
                Kokku käibemaksuga: <?= $order_total_price ?>€
            </div>
        <?php else: ?>
            <div>
                Summa: <?= $selected_workers['course']['course_price'] ?>€
            </div>
            <div>
                Käibemaks <?= VAT_PERCENT ?>%: <?= round($selected_workers['course']['course_price'] * VAT_PERCENT / 100, 2) ?>€
            </div>
            <div>
                Kokku käibemaksuga: <?= $selected_workers['order']['order_price'] ?>€
            </div>
        <?php endif ?>

    </div>
</div>
</body>
</html>