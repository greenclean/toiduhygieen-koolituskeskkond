<?php namespace Halo;

// Init composer auto-loading
if (!@include_once("vendor/autoload.php")) {
    exit('Run composer install');
}

// Project constants
define('PROJECT_VERSION', trim(exec('git log --pretty="%h" -n1 HEAD')));
define('PROJECT_NAME', 'Toiduhügieen');
define('PROJECT_NATIVE_LANGUAGE', '1');
define('DEFAULT_CONTROLLER', 'courses');
define('DEBUG', false);
define('IS_ID', 1);
define('IS_ARRAY', 2);
define('IS_NON_EMPTY', 3);

// Make sure timestamps are correct
date_default_timezone_set('Europe/Tallinn');


// Load app
require 'system/classes/Application.php';

$app = new Application;