#!/bin/bash
cd /sites/kool.toiduhugieen.ee/
chown -R greenclean:www-data .
chown -R www-data:www-data .sessions
chown -R www-data:greenclean /sites/kool.toiduhugieen.ee/vendor/mpdf/
chown -R www-data:www-data uploads