<?php
require_once 'helpers.php';

ob_start();
define('PROJECT_VERSION', 0);
define('BASE_URL', 'http://localhost/toiduhygieen-koolituskeskkond/');
set_include_path('../');

require_once '../config.php';
require_once '../system/database.php';
require_once '../system/functions.php';
require_once '../vendor/autoload.php';

if (q("show columns from translations like 'translation_phrase'")) {
    exit("Database already converted");
}
set_time_limit(1000);
update_test_results_table();
transform_settings_table();
transform_languages_table();
transform_translations_table();
add_languages(['en', 'et']);
transform_deployments_table();
transform_test_results_table();
transform_answers_table();
transform_questions_table();
transform_courses_table();
transform_categories_table();
transform_test_question_set_table();
transform_orders_table();
transform_order_statuses_table();
transform_payment_methods_table();
add_indexes();
fix_foreign_keys();
translate_remaining(['et']);
drop_table('learn');
drop_table('test_question_set_backup-2019-07-13_0012');
translate_course_names_to_english();

exit("<br>\nDone");