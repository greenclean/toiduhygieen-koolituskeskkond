<?php

use Stichoza\GoogleTranslate\GoogleTranslate;
use Toiduhygieen\Language;
use Toiduhygieen\Translation;

const SESSION_DIR = '../.sessions/';

class SessionUnserializer
{
    public static function unserialize($session_data)
    {
        $method = ini_get("session.serialize_handler");
        switch ($method) {
            case "php":
                return self::unserialize_php($session_data);
                break;
            case "php_binary":
                return self::unserialize_phpbinary($session_data);
                break;
            default:
                throw new Exception("Unsupported session.serialize_handler: " . $method . ". Supported: php, php_binary");
        }
    }

    private static function unserialize_php($session_data)
    {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) {
                throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
            }
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }

    private static function unserialize_phpbinary($session_data)
    {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            $num = ord($session_data[$offset]);
            $offset += 1;
            $varname = substr($session_data, $offset, $num);
            $offset += $num;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }
}

/**
 * Returns array of files from session folder
 * @return array|false Files from session folder
 */
function get_session_files()
{
    return scandir(SESSION_DIR);
}

/**
 * Renames a table
 * @param string $from Original table name
 * @param string $to New table name
 */
function rename_table(string $from, string $to): void
{
    q("ALTER TABLE `$from` rename to `$to`");
}

/**
 * Adds a column to the table
 * @param string $table
 * @param string $column
 * @param string $type Column type
 */
function add_column(string $table, string $column, string $type): void
{
    q("alter table `$table` add column `$column` $type");
}

function transform_settings_table(): void
{
    q("alter table settings change setting setting_name varchar(191) not null");
    q("alter table settings change value setting_value varchar(191) null");
}

/**
 * @param array $lng
 */
function transform_languages_table(): void
{
    $lng = [
        1 => 'et',
        2 => 'en',
        3 => 'ru'
    ];

    q("alter table languages add language_code varchar(255) null");

    q("INSERT INTO `languages` (language_code, language_name) VALUES ('af','Afrikaans'),('am','Amharic'),('ar','Arabic'),('az','Azerbaijani'),('be','Belarusian'),('bg','Bulgarian'),('bn','Bengali'),('bs','Bosnian'),('ca','Catalan'),('ceb','Cebuano'),('co','Corsican'),('cs','Czech'),('cy','Welsh'),('da','Danish'),('de','German'),('el','Greek'),('eo','Esperanto'),('es','Spanish'),('eu','Basque'),('fa','Persian'),('fi','Finnish'),('fr','French'),('fy','Frisian'),('ga','Irish'),('gd','Scots Gaelic'),('gl','Galician'),('gu','Gujarati'),('ha','Hausa'),('haw','Hawaiian'),('he','Hebrew'),('hi','Hindi'),('hmn','Hmong'),('hr','Croatian'),('ht','Haitian'),('hu','Hungarian'),('hy','Armenian'),('id','Indonesian'),('ig','Igbo'),('is','Icelandic'),('it','Italian'),('ja','Japanese'),('jv','Javanese'),('ka','Georgian'),('kk','Kazakh'),('km','Khmer'),('kn','Kannada'),('ko','Korean'),('ku','Kurdish'),('ky','Kyrgyz'),('la','Latin'),('lb','Luxembourgish'),('lo','Lao'),('lt','Lithuanian'),('lv','Latvian'),('mg','Malagasy'),('mi','Maori'),('mk','Macedonian'),('ml','Malayalam'),('mn','Mongolian'),('mr','Marathi'),('ms','Malay'),('mt','Maltese'),('my','Myanmar'),('ne','Nepali'),('nl','Dutch'),('no','Norwegian'),('ny','Nyanja (Chichewa)'),('or','Odia (Oriya)'),('pa','Punjabi'),('pl','Polish'),('ps','Pashto'),('pt','Portuguese'),('ro','Romanian'),('rw','Kinyarwanda'),('sd','Sindhi'),('si','Sinhala (Sinhalese)'),('sk','Slovak'),('sl','Slovenian'),('sm','Samoan'),('sn','Shona'),('so','Somali'),('sq','Albanian'),('sr','Serbian'),('st','Sesotho'),('su','Sundanese'),('sv','Swedish'),('sw','Swahili'),('ta','Tamil'),('te','Telugu'),('tg','Tajik'),('th','Thai'),('tk','Turkmen'),('tl','Tagalog (Filipino)'),('tr','Turkish'),('tt','Tatar'),('ug','Uyghur'),('uk','Ukrainian'),('ur','Urdu'),('uz','Uzbek'),('vi','Vietnamese'),('xh','Xhosa'),('yi','Yiddish'),('yo','Yoruba'),('zh','Chinese'),('zu','Zulu')");

    foreach ($lng as $language_id => $language_code) {
        update('languages', [
            'language_code' => $language_code
        ], "language_id = $language_id");
    }

    q("alter table languages modify language_code varchar(255) not null");

    // languages.language_id from int to tinyint unsigned
    modify_column('languages', 'language_id', 'tinyint unsigned not null auto_increment');
}


function transform_translations_table(): void
{
    q("DROP TABLE IF EXISTS translations_new");

    if (q("show tables like 'translations_backup'")) {
        exit('You already have translations_backup table. Refusing to continue!');
    }

    q("
    CREATE TABLE translations_new (
  translation_id int unsigned NOT NULL AUTO_INCREMENT,
  translation_phrase varchar(765) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  translation_state enum('exists_in_code','does_not_exist','dynamic') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'exists_in_code',
  translation_source varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  translation_source_id int NOT NULL DEFAULT '0',
  PRIMARY KEY (translation_id),
  UNIQUE KEY translations_translation_phrase_uindex (translation_phrase,translation_source_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
");

    rename_table('translations', 'translations_backup');
    rename_table('translations_new', 'translations');
}

function transform_deployments_table(): void
{
    q("alter table deployments change commit_date deployment_commit_date datetime not null");
    q("alter table deployments change commit_message deployment_commit_message varchar(191) not null");
    q("alter table deployments change commit_sha deployment_commit_sha varchar(191) not null");
    remove_duplicates_from_table('deployments', 'deployment_commit_sha');
    add_primary_key('deployments', ['deployment_commit_sha']);
    q("alter table deployments change commit_author deployment_commit_author varchar(191) not null");
}

function remove_duplicates_from_table($table, $column)
{
    q("CREATE TABLE {$table}_copy LIKE $table");
    q("INSERT INTO {$table}_copy SELECT * FROM $table GROUP BY $column");
    q("DROP TABLE $table");
    q("ALTER TABLE {$table}_copy RENAME TO $table");
}

/**
 * Moves question and their explanation texts over to translations table adding translation ids to questions table
 */
function export_questions_table_to_translations_table(): void
{
    $rows = get_all("SELECT * FROM test_questions");
    foreach ($rows as $row) {

        // Insert question to translations table
        add_translation($row['test_question'], 'questions', $row['test_question_id']);
        update('translations', ['translation_in_en' => $row['test_question']], "translation_source_id = $row[test_question_id]");
    }
}

function delete_non_english_questions_and_their_answers(): void
{
    q("
        DELETE FROM test_answers
        WHERE test_question_id IN (SELECT test_question_id FROM test_questions WHERE language_id != 2)");
    q("
        DELETE FROM test_questions
        WHERE language_id != 2");
}

/**
 * Returns translation id for given text, creating the translation if it did not already exist.
 * @param string $phrase
 */
function add_translation(string $phrase, $translation_source, $translation_source_id)
{
    Translation::add($phrase, $translation_source, $translation_source_id);
}

/**
 * Moves answers over to translations table adding translation id to answers table
 */
function export_answers_table_to_translations_table(): void
{
    $rows = get_all("SELECT * FROM test_answers");
    foreach ($rows as $row) {
        // Insert answer to translations table
        add_translation($row['test_answer'], 'answers', $row['test_answer_id']);
        update('translations', ['translation_in_en' => $row['test_answer']], "translation_source_id = $row[test_answer_id]");
    }
}

function transform_questions_table(): void
{
    export_questions_table_to_translations_table();
    delete_column_with_foreign_key('test_questions', 'test_question');
    delete_column_with_foreign_key('test_questions', 'language_id');
    delete_column_with_foreign_key('test_questions', 'test_question_answer_explanation');
    rename_column('test_questions', 'test_question_id', 'question_id');
    rename_table('test_questions', 'questions');
}

function transform_answers_table(): void
{
    delete_non_english_questions_and_their_answers();
    export_answers_table_to_translations_table();
    delete_column_with_foreign_key('test_answers', 'test_answer');
    rename_column('test_answers', 'test_answer_id', 'answer_id');
    rename_column('test_answers', 'test_answer_letter', 'answer_letter');
    rename_column('test_answers', 'test_answer_is_correct', 'answer_is_correct');
    rename_column('test_answers', 'test_question_id', 'question_id');
    rename_table('test_answers', 'answers');
    q("alter table answers drop foreign key answers_ibfk_1");
    q("alter table answers add constraint answers_ibfk_1 foreign key (question_id) references test_questions (test_question_id) on delete cascade;");

    // Kosmeetikaks
    q('alter table answers modify answer_letter char not null after answer_id');
}


function transform_courses_table(): void
{
    add_column('courses', 'language_id', 'tinyint unsigned not null default 2');
    add_constraint('courses', 'language_id', 'languages', 'language_id');
}

function transform_categories_table(): void
{
    q("alter table questions drop constraint questions_ibfk_1");
    q("truncate test_categories");

    q("insert into test_categories (test_category_id, test_category_name) values 
(1,'Introduction')
, (2,'Personal hygiene')
, (3,'Microbiology')
, (4,'Cross - contamination')
, (5,'Labeling')
, (6,'Health')
, (7,'Waste management')
, (8,'Training')
, (9,'Continuity of the cold chain')
, (10,'Cleaning and disinfection')
, (11,'Preservation')
, (12,'Production hygiene')
, (13,'Allergy')
, (14,'Legislation')
, (15,'Pest control')
, (16,'CP and CCP')
, (17,'Monitoring system')
, (18,'Transport')
, (19,'Packaging')
, (20,'HACCP')
, (21,'AUDIT')
, (22,'Laboratory analysis')
, (23,'Retention of documents')
, (24,'Corrective action')
, (25,'Traceability')
, (26,'Water')
, (27,'Critical limits')
, (28,'Working group')
, (29,'Recall')
, (30,'McD FO')
, (31,'McD FO ENG')
, (32,'McD MA')
, (33,'McD MA ENG')
, (34,'McD FO RUS')
, (35,'McD MA RUS')
, (36,'Leibur EST')
, (37,'Leibur RUS')
, (38,'Leibur ENG')
, (39,'Leibur 2021 EST')
, (40,'Leibur 2021 ENG')
, (41,'Leibur 2021 RUS')");

    q("alter table questions add constraint questions_ibfk_1 foreign key (test_category_id) references test_categories (test_category_id) on delete cascade");

    $rows = get_all("SELECT * FROM test_categories");
    foreach ($rows as $row) {
        insert("translations", [
            "translation_phrase" => $row['test_category_name'],
            "translation_source" => "test_categories.test_category_name",
            "translation_state" => "dynamic"
        ], true);
    }
}

function transform_test_question_set_table()
{
    rename_table('test_question_set', 'question_set');
    rename_column('question_set', 'test_question_id', 'question_id');
    modify_column('question_set', 'answer_chosen', 'char(1)');
    add_column('question_set', 'answer_chosen_id', 'int unsigned');
}

function transform_test_results_table()
{
    // TODO: eesti ja venekeelsete küsimuste id'd inglise keelsete id'de peale. Samuti ei salvestata küsimuse vastamise hetkel küsimuse ja õige/valitud vastuse fraasi
    rename_column('test_results', 'test_question_id', 'question_id');
    modify_column('test_results', 'test_id', 'int unsigned not null');
}

function transform_orders_table()
{
    modify_column('orders', 'order_status_id', 'tinyint unsigned default 3 not null');
    modify_column('orders', 'payment_method_id', 'tinyint unsigned default 1 not null');
}

function transform_order_statuses_table()
{
    modify_column('order_statuses', 'order_status_id', 'tinyint unsigned auto_increment not null');
}

function transform_payment_methods_table()
{
    modify_column('payment_methods', 'payment_method_id', 'tinyint unsigned auto_increment not null');
}

function add_indexes()
{
    add_primary_key('banklink_payment_workers', ['user_id', 'banklink_payment_id']);
}

function fix_foreign_keys()
{
    add_constraint('banklink_payments', 'order_status_id', 'order_statuses', 'order_status_id');
    add_constraint('banklink_payments', 'payment_method_id', 'payment_methods', 'payment_method_id');
    add_constraint('orders', 'banklink_payment_id', 'banklink_payments', 'banklink_payment_id');
    add_constraint('orders', 'order_status_id', 'order_statuses', 'order_status_id');
    add_constraint('orders', 'payment_method_id', 'payment_methods', 'payment_method_id');
//    add_constraint('question_set','answer_chosen_id','questions','question_id');
//    add_constraint('question_set','question_id','questions','question_id');
    add_constraint('question_set', 'test_id', 'tests', 'test_id');
//    add_constraint('test_results','test_id','tests','test_id');
//    add_constraint('test_results','question_id','questions','question_id');
//    add_constraint('users','employer_id','users','user_id');
}

function add_languages($language_codes)
{
    foreach ($language_codes as $language_code) {
        Language::add($language_code);
    }
}

function translate_remaining($language_codes)
{
    foreach ($language_codes as $language_code) {
        while ($remaining = Translation::google_translate_missing_translations($language_code)) {
            echo "$remaining phrases remaining in $language_code <br> \n";
//            sleep(1);
        }
    }
}

function translate_course_names_to_english()
{
    $rows = get_all("SELECT course_id, course_name FROM courses");
    $google_translate = new GoogleTranslate();
    foreach ($rows as $row) {
        $translation = $google_translate->setTarget('en')->translate($row['course_name']);
        update('courses', ['course_name' => $translation], "course_id = $row[course_id]");
    }
}

function drop_table($table_name)
{
    q("DROP TABLE `$table_name`");
}

/**
 * Changes column type
 * @param string $table
 * @param string $column
 * @param string $type
 */
function modify_column(string $table, string $column, string $type): void
{
    $foreign_keys = get_foreign_keys_pointing_to_column($table, $column);
    drop_foreign_keys($foreign_keys);
    q("alter table `$table` modify column `$column` $type");
    update_types_of_foreign_key_columns($foreign_keys, $type);
    add_foreign_keys($foreign_keys);
}

function add_primary_key(string $table, array $columns): void
{
    $foreign_keys = [];
    foreach ($columns as $column) {
        $foreign_keys[$column] = get_foreign_keys_of_column($table, $column);
        drop_foreign_keys($foreign_keys[$column]);
    }

    $name = implode('_', $columns);
    $columns = implode(',', $columns);

    q("alter table `$table` add constraint `$name` primary key ($columns)");

    foreach ($foreign_keys as $column => $foreign_key) {
        add_foreign_keys($foreign_key);
    }
}

/**
 * @param array $foreign_keys
 * @return mixed
 */
function drop_foreign_keys(array $foreign_keys): void
{
    foreach ($foreign_keys as $foreign_key) {
        drop_constraint($foreign_key['TABLE_NAME'], $foreign_key['CONSTRAINT_NAME']);
    }
}

function add_foreign_keys(array $foreign_keys): void
{
    foreach ($foreign_keys as $foreign_key) {
        add_constraint($foreign_key['TABLE_NAME'], $foreign_key['COLUMN_NAME'], $foreign_key['REFERENCED_TABLE_NAME'], $foreign_key['REFERENCED_COLUMN_NAME']);
    }
}


function update_types_of_foreign_key_columns(array $foreign_keys, string $type): void
{
    foreach ($foreign_keys as $foreign_key) {
        modify_column($foreign_key['TABLE_NAME'], $foreign_key['COLUMN_NAME'], strip_null_and_auto_increment_from_type($type));
    }
}

/**
 * @param string $type
 * @return string|string[]|null
 */
function strip_null_and_auto_increment_from_type(string $type)
{
    return preg_replace('/\s*(auto_increment|not null)/', '', $type);
}


/**
 * Drops given foreign key
 * @param string $table
 * @param string $constraint
 */
function drop_constraint(string $table, string $constraint): void
{
    q("ALTER TABLE `$table` DROP FOREIGN KEY `$constraint`");
}

function add_constraint(string $table, $column, $foreign_table, $foreign_column): void
{
    q("alter table `$table` add constraint `{$table}_{$column}_fk` foreign key (`$column`) references `$foreign_table` (`$foreign_column`) on delete cascade");
}


/**
 * Renames the column
 * @param string $table The table
 * @param string $from The original column name
 * @param string $to New column name
 * @throws Exception
 */
function rename_column(string $table, string $from, string $to): void
{
    $type = get_column_type($table, $from);
    if (!$type) {
        throw new Exception("Column $from not found from table $table!");
    }
    q("alter table `$table` change `$from` `$to` $type");
}

/**
 * Returns column type
 * @param string $table Name of the table
 * @param string $column Name of the column
 * @return string Type of column
 */
function get_column_type(string $table, string $column): string
{
    $type = '';
    $rows = get_all("DESCRIBE `$table`");
    foreach ($rows as $row) {
        if ($row['Field'] === $column) {
            $type = $row['Type'] . " " . ($row['Null'] === 'NO' ? 'NOT NULL' : 'NULL') . " " . $row['Extra'];
        }
    }
    return $type;
}

/**
 * Deletes column from table
 * @param string $table Name of the table where the column that is to be deleted exists
 * @param string $column Name of the column to delete
 */
function delete_column_with_foreign_key(string $table, string $column): void
{
    drop_column_foreign_key($table, $column);

    delete_column($table, $column);
}

/**
 * @param string $table
 * @param string $column
 */
function delete_column(string $table, string $column): void
{
    q("alter table `$table` drop column `$column`");
}

/**
 * @param string $table
 * @param string $column
 */
function drop_column_foreign_key(string $table, string $column): void
{
    $constraint = get_constraint($table, $column);

    if ($constraint) {
        q("ALTER TABLE `$table` DROP FOREIGN KEY `$constraint[CONSTRAINT_NAME]`");
    }
}

/**
 * Returns the data of the foreign key on given column
 * @param string $table
 * @param string $column
 * @return array
 */
function get_constraint(string $table, string $column): array
{
    $row = get_first("
        SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE table_schema='" . DATABASE_DATABASE . "'
        AND table_name='$table'
        AND column_name='$column'");

    return $row ? $row : [];
}

/**
 * Returns information which table.field has a foreign key to specified $table.$column
 * @param string $table
 * @param string $column
 * @return array
 */
function get_foreign_keys_pointing_to_column(string $table, string $column): array
{
    return get_all("
        SELECT 
          TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME

        FROM
          INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE
          REFERENCED_TABLE_SCHEMA = '" . DATABASE_DATABASE . "' AND
          REFERENCED_TABLE_NAME = '$table' AND
          REFERENCED_COLUMN_NAME = '$column'");

}

function get_foreign_keys_of_column(string $table, string $column): array
{
    return get_all("
        SELECT 
          TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME

        FROM
          INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE
          TABLE_SCHEMA = '" . DATABASE_DATABASE . "' AND
          TABLE_NAME = '$table' AND
          REFERENCED_COLUMN_NAME = '$column'");

}

function update_test_results_table(): void
{
    q("alter table test_results
    add question_text varchar(765) charset utf8mb4 not null,
    add user_choice_text varchar(765) charset utf8mb4 not null,
    add correct_choice_text varchar(765) charset utf8mb4 not null;");

        q("
            UPDATE test_results
    LEFT JOIN test_questions questions ON
            test_results.test_question_id = questions.test_question_id
    LEFT JOIN test_answers useranswer ON
        useranswer.test_question_id = test_results.test_question_id AND useranswer.test_answer_letter = test_results.user_choice_letter
    LEFT JOIN test_answers testanswer ON
        testanswer.test_question_id = test_results.test_question_id AND testanswer.test_answer_letter = test_results.test_answer_letter
SET test_results.question_text = questions.test_question,
    test_results.user_choice_text = useranswer.test_answer,
    test_results.correct_choice_text = testanswer.test_answer
    ");
}