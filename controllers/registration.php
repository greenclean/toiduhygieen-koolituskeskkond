<?php
/**
 * Created by PhpStorm.
 * User: carolin
 * Date: 1/19/16
 * Time: 10:48
 */

namespace Halo;


use Toiduhygieen\Email;
use Toiduhygieen\User;

class registration extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;

    public $requires_auth = false;


    function ajax_user_registration()
    {
        header("Access-Control-Allow-Origin: *");

        if (!isset($_POST['nimi']) || !isset($_POST['epost']) || !isset($_POST['isikukood']) || !isset($_POST['suhtluskeel'])) {
            stop(400, ['title' => __('Error'), 'description' => __('Missing required parameters!')]);
        }

        $users['name'] = trim($_POST['nimi']);
        $users['email'] = trim($_POST['epost']);
        $users['personal_code'] = trim($_POST['isikukood']);
        $users['allow_inherit'] = 1; // To make employees see Greenclean's default courses

        User::validate($users);


        // Perform SMTP test on email address
        if (REAL_EMAIL_CHECKING && !Email::isReal($users['email'])) {
            header('Content-Type: application/json');
            stop(400, ['title' => __('Error'), 'description' => __('This email address is not working. Try another one or make sure you typed the email address correctly.')]);
        }

        $users['language_id'] = addslashes($_POST['suhtluskeel']);
        $password = $this->generatePassword(12);
        $users['password'] = password_hash($password, PASSWORD_DEFAULT);
        $user_id = insert("users", $users);
        $_SESSION['user_id'] = $user_id; // tõstab user_id sessionisse, et class __construkt saaks automaatselt sisse logida pärast refreshi
        /* update("users", ["employer_id" => $user_id], "user_id = $user_id");*/

        $default_courses = get_all("SELECT * FROM courses WHERE course_type = 'course' AND default_course = 1");

        foreach ($default_courses as $course) {
            insert('course_user', ["course_id" => $course['course_id'], 'user_id' => $user_id]);
        }

        $email_content = get_first("SELECT * FROM email_texts WHERE email_type_id=1 AND language_id=$users[language_id]");

        if (send_mail($users['email'], $email_content['email_text_subject'], $email_content['email_text_content'] . " " . $password)) {
            header('Content-Type: application/json');
            stop(200, ['title' => __('Notice'), 'description' => __('Password has been sent to e-mail!')]);
        } else {
            header('Content-Type: application/json');
            stop(400, ['title' => __('Error'), 'description' => __('Couldn\'t send your password!')]);
        }
    }


    function new_password()
    {
        if (isset($_POST['email'])) {
            $user_email = addslashes($_POST['email']);
            $email = get_first("SELECT * FROM users LEFT JOIN email_texts using (language_id) WHERE email_type_id=2 AND email='$user_email'");
            $new_password = $this->generatePassword(12);
            $new_password_hashed = password_hash($new_password, PASSWORD_DEFAULT);


            if (!empty($email) && send_mail($user_email, $email['email_text_subject'], $email['email_text_content'] . " " . $new_password)) {
                $flash['success'] = "Teie parool on saadetud e-posti aadressile " . $user_email;
                update("users", ['password' => $new_password_hashed], "email='$user_email'");
            } else {
                $flash['danger'] = __('Couldn\'t send your password!');

            }

            header("refresh:5;url=" . BASE_URL);

            require 'templates/auth_template.php';
            exit();
        }
    }

    // usage: $newpassword = generatePassword(12); // for a 12-char password, upper/lower/numbers.
    // functions that use rand() or mt_rand() are not secure according to the PHP manual.
    function getRandomBytes($nbBytes = 32)
    {
        $bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
        if (false !== $bytes && true === $strong) {
            return $bytes;
        } else {
            throw new \Exception("Unable to generate secure token from OpenSSL.");
        }
    }

    function generatePassword($length)
    {
        return substr(preg_replace("/[^a-zA-Z0-9]/", "", base64_encode($this->getRandomBytes($length + 1))), 0, $length);
    }
}