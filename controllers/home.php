<?php namespace Halo;


class home extends Controller
{
    public $requires_auth = false;

    function index()
    {
        define('PAYMENT_BY_INVOICE', 1);
        define('PAYMENT_BY_BANK_TRANSFER', 2);

        $this->courses = get_all("SELECT * FROM courses WHERE course_type = 'course' AND in_login_status = 0");

    }


}