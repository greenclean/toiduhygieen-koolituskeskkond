<?php namespace Halo;

use Toiduhygieen\Certificate;
use Toiduhygieen\Course;
use Toiduhygieen\Email;
use Toiduhygieen\Language;
use Toiduhygieen\Translation;
use Toiduhygieen\User;
class workers extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $workers;
    public $languages;
    public $courses;
    public $user_courses;

    function index()
    {
        $user_id = $this->auth->user_id;
        $this->workers = $this->get_workers();
        $this->languages = Language::get_all_in_use();
        $this->courses = Course::get_by_user((array)$this->auth);
        $this->user_courses = [];
        unset($_SESSION['selected_workers']);
        foreach ($this->workers as $worker) {

            $existing_courses = get_all("SELECT
                                              order_price,
                                              course_name,
                                              CASE
                                              WHEN course_type = 'certificate'
                                                THEN 'certificate'
                                              WHEN (test_score < 75)
                                                THEN 'failed'
                                              WHEN (test_id IS NULL) AND order_status_id = 1
                                                THEN 'purchased'
                                              WHEN test_score IS NULL AND order_status_id = 1
                                                THEN 'started'
                                              WHEN order_status_id = 3
                                                THEN 'unconfirmed'
                                              WHEN ((test_score >= 75))
                                                THEN 'completed'
                                              WHEN order_status_id = 2 AND course_type = 'course'
                                                  THEN 'declined'
                                              END AS course_status
                                            FROM orders
                                              LEFT JOIN courses USING (course_id)
                                              LEFT JOIN tests USING (order_id)
                                            WHERE orders.user_id = $worker[user_id]");

            $counter = 0;

            foreach ($existing_courses as $existing_course) {

                $counter++;
                $this->user_courses[$worker['user_id']][$counter]['course_name'] = preg_replace('/toiduhügieeni koolitus/i', '', $existing_course['course_name']);
                $this->user_courses[$worker['user_id']][$counter]['course_status'] = $existing_course['course_status'];

                if ($existing_course['course_status'] == 'failed') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-danger';
                } elseif ($existing_course['course_status'] == 'started') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-primary';
                } elseif ($existing_course['course_status'] == 'completed') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-success';
                } elseif ($existing_course['course_status'] == 'purchased') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-warning';
                } elseif ($existing_course['course_status'] == 'unconfirmed') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-secondary';
                } elseif ($existing_course['course_status'] == 'certificate') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-purple';
                } elseif ($existing_course['course_status'] == 'declined') {
                    $this->user_courses[$worker['user_id']][$counter]['label_class_name'] = 'badge-black';
                }
            }
        }


        if ($this->auth->employer_id) {
            $this->team_lead = get_first('SELECT * FROM users WHERE user_id = ' . $this->auth->employer_id);
        }
    }

    function ajax_new()
    {
        validate($_POST['personal_code'], IS_NON_EMPTY, __('personal code'));
        validate($_POST['name'], IS_NON_EMPTY, __('name'));
        validate($_POST['email'], IS_NON_EMPTY, __('email'));

        User::validate($_POST);

        // Perform SMTP test on email address
        if (REAL_EMAIL_CHECKING && !Email::isReal($_POST['email'])) {
            header('Content-Type: application/json');
            stop(400, ['title' => __('Error'), 'description' => __('This email address is not working. Try another one or make sure you typed the email address correctly')]);
        }

        $replacements = [
            $this->auth->name,
            $_POST['name']
        ];
        $this->sendMail($replacements, $_POST['email'], $_POST['language_id'], 11);

        insert('users', [
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'personal_code' => $_POST['personal_code'],
            'language_id' => $_POST['language_id'],
            'password' => password_hash($this->generatePassword(12), PASSWORD_DEFAULT),
            'employer_id' => $this->auth->user_id,
        ]);

        stop(200);
    }


    function post_get_user()
    {
        $user_id = $_POST['user_id'];
        // Get user data and add it to session
        $user = get_first("SELECT * FROM users WHERE user_id = $user_id");
        header('Content-Type: application/json');
        exit(json_encode(['result' => 'Ok', 'data' => $user]));
    }


    function getRandomBytes($nbBytes = 32)
    {
        $bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
        if (false !== $bytes && true === $strong) {
            return $bytes;
        } else {
            throw new \Exception("Unable to generate secure token from OpenSSL.");
        }
    }

    function generatePassword($length)
    {
        return substr(preg_replace("/[^a-zA-Z0-9]/", "", base64_encode($this->getRandomBytes($length + 1))), 0, $length);
    }

    function ajax_worker_save()
    {
        validate($_POST['user_id'], IS_NON_EMPTY, 'user_id');
        validate($_POST['data'], IS_ARRAY, 'data');

        User::validate($_POST['data'], $_POST['user_id']);

        if (User::withConflictingDataExists([
                'users.personal_code' => $_POST['data']['personal_code'],
                'users.name' => $_POST['data']['name']
            ], $_POST['user_id'])) {
            stop(400, ['title' => __('Error'), 'description' => __('Name + personal code already exists')]);
        }

        if ($this->auth->is_admin == 0 && Certificate::count($_POST['user_id'])) {
            stop(400, ['title' => __('Error'), 'description' => __('User has the certificate')]);
        }

        if (REAL_EMAIL_CHECKING && !Email::isReal($_POST['data']['email'])) {
            stop(400, ['title' => __('Error'), 'description' => __('This email address is not working. Try another one or make sure you typed the email address correctly')]);
        }

        // Reset faulty_personal_code because checks have been passed
        $_POST['data']['faulty_personal_code'] = 0;

        User::update($_POST['user_id'], $_POST['data']);


        stop(200);
    }

    public function delete_worker()
    {
        validate($this->params[0]);
        $user_id = $this->params[0];
        $user = get_first("SELECT * FROM users WHERE user_id = $user_id AND employer_id = " . $this->auth->user_id);
        if ($user) {
            $replacements = [
                $this->auth->name,
                $user['name']
            ];


            update("users", ['employer_id' => null], "user_id=" . $user_id);
            $this->sendMail($replacements, $user['email'], $user['language_id'], 10);
        }
        header("Location:" . BASE_URL . 'workers');
    }

    public function delete_teamlead()
    {
        $team_lead = get_first("SELECT * FROM users WHERE user_id = " . $this->auth->employer_id);
        update("users", ['employer_id' => null], "user_id=" . $this->auth->user_id);

        $replacements = [
            $team_lead['name'],
            $this->auth->name
        ];

        $this->sendMail($replacements, $team_lead['email'], $team_lead['language_id'], 14);
        header("Location:" . BASE_URL . 'workers');
    }

    public function ajax_send_guide()
    {

        if (empty($_POST['user_id'])) {
            $_SESSION['selected_workers'][$this->auth->user_id] = (array)$this->auth;
        } // Team leader purchases for his workers
        else if (is_array($_POST['user_id'])) {

            // Clear session selected workers
            unset($_SESSION['selected_workers']);

            // Iterate over selected workers
            foreach (array_keys($_POST['user_id']) as $user_id) {
                $_SESSION['selected_workers'][$user_id] = [];
            }
        } else {
            throw new \Exception(__('Error'));
        }

        // Get relevant users' data
        $user_ids = implode(',', array_keys($_SESSION['selected_workers']));
        $users = get_all("SELECT * FROM users WHERE user_id IN( $user_ids)");

        // Add additional data to selected_workers
        foreach ($users as $user) {

            $replacements = [
                $user['name'],
                $this->auth->name
            ];
            $email_number = strpos($user['personal_code'], '-') === false ? 6 : 7;
            $this->sendMail($replacements, $user['email'], $user['language_id'], $email_number);
        }
    }

    private function sendMail($replacements, $user_email, $language_id, $email_num)
    {
        $patterns = [
            '/{teamleader}/',
            '/{employee username}/',
        ];

        $language_id = $language_id;
        $email = Email::get_content($language_id, $email_num);
        $email_subject = preg_replace($patterns, $replacements, $email['email_text_subject']);
        $email_content = preg_replace($patterns, $replacements, $email['email_text_content']);
        send_mail($user_email, $email_subject, $email_content);
    }

    private function validateDate($date, $format = 'Y-m-d')
    {
        $check_date = \DateTime::createFromFormat($format, $date);
        return $check_date && $check_date->format($format) === $date;
    }

    private function get_workers()
    {
        $workers = get_all("SELECT * FROM users WHERE user_id = " . $this->auth->user_id);
        return array_merge($workers, get_all("SELECT * FROM users WHERE employer_id =" . $this->auth->user_id));
    }

}