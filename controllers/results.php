<?php namespace Halo;

use Toiduhygieen\Language;

class results extends Controller
{
    function index()
    {

        if (!$this->auth->is_admin) {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

        $this->results = get_all("SELECT
                                         test_id,
                                         name,
                                         course_name,
                                         language_name,
                                         test_score,
                                         test_created
                                       FROM tests
                                         LEFT JOIN orders USING (order_id)
                                         LEFT JOIN users ON (tests.user_id = users.user_id)
                                         LEFT JOIN courses USING (course_id)
                                         LEFT JOIN languages on users.language_id = languages.language_id
                                       WHERE test_score IS NOT NULL
                                       ORDER BY test_created DESC");

    }

    function view()
    {
        if (!$this->auth->is_admin) {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

        $this->test_id = $this->params[0];
        $this->user_language = Language::get_current()['language_code'];
        $this->questions = get_all("SELECT test_id,
                            question_id,
                            test_answer_letter,
                            user_choice_letter,
                            question_translations.translation_in_$this->user_language as question,
                            answer_translations.translation_in_$this->user_language as correct_answer,
                            explanation_translations.translation_in_$this->user_language as explanation
                                    FROM test_results
                                         left join translations as question_translations on test_results.question_id = question_translations.translation_source_id AND question_translations.translation_source = 'questions'
                                         left join translations as explanation_translations on test_results.question_id = explanation_translations.translation_source_id
                                         left join translations as answer_translations on test_results.question_id = answer_translations.translation_source_id
                                         WHERE test_id=$this->test_id GROUP BY question_id");
    }
}