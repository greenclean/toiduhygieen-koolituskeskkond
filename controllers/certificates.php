<?php namespace Halo;

use Toiduhygieen\Certificate;
use Toiduhygieen\Transliteration;
use Transliterator;

class certificates extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $email;
    public $certificates;

    function index()
    {
        $user_id = $this->auth->user_id;
        $this->email = get_one("SELECT email FROM users WHERE user_id = $user_id");
        if ($this->auth->is_admin == 1) {
            $this->certificates = get_all("SELECT *
                                                FROM certificates
                                                  LEFT JOIN users USING (user_id)
                                                  LEFT JOIN tests USING (test_id)
                                                  LEFT JOIN orders USING (order_id)
                                                  LEFT JOIN courses USING (course_id)
                                                GROUP BY certificate_id ORDER BY certificate_id DESC");
        } else {
            $user_id = $this->auth->user_id;
            $this->certificates = get_all("SELECT *
                                                FROM certificates
                                                   LEFT JOIN users USING (user_id)
                                                   LEFT JOIN tests USING (test_id)
                                                   LEFT JOIN orders USING (order_id)
                                                   LEFT JOIN courses USING (course_id)
			                                    WHERE employer_id = $user_id OR certificates.user_id = $user_id
			                                    GROUP BY certificate_id ORDER BY certificate_id DESC");
        }
    }

    private function create_certificate($test_id)
    {
        return Certificate::create($test_id, $this->auth->user_id);
    }


    function view_certificate()
    {
        $test_id = $this->params[0];

        if (!$test_id) {
            $errors[] = __('Test ID is not valid');
            require 'templates/error_template.php';
            exit();
        }

        $user = get_first("SELECT * 
                                FROM certificates 
                                  LEFT JOIN users using (user_id) 
                                  LEFT JOIN tests using (test_id) 
                                WHERE test_id=$test_id");

        if (!$user) {
            $errors[] = __('Certificate does not exist!');
            require 'templates/error_template.php';
            exit();

        } else {
            $name = $user['name'];
        }

        $name = Transliteration::symbolsToLatin($name);
        $pdf = $this->create_certificate($test_id);
        $pdf->Output('Sertifikaat_' . preg_replace("/ /", "_", $name) . '.pdf', 'D');
        exit();

    }

    function send()
    {
        if (isset($_POST['user_email'])) {
            update('users', ['email' => $_POST['user_email']], 'user_id =' . $this->auth->user_id);
        }

        $test_id = $this->params[0];

        if ($this->auth->is_admin == 1) {
            $email_content = get_first("SELECT *
                                             FROM certificates
                                               LEFT JOIN tests USING (user_id)
                                               LEFT JOIN users USING (user_id)
                                               LEFT JOIN email_texts USING (language_id)
                                             WHERE certificates.test_id = $test_id AND email_type_id = 3
                                             GROUP BY certificate_id");

        } else {

            $user_language_id = $this->auth->language_id;
            $email_content = get_first("SELECT * FROM users 
                                               LEFT JOIN email_texts using (language_id)
                                               LEFT JOIN certificates using (user_id) 
                                             WHERE email_type_id=3 AND language_id=$user_language_id 
                                               AND certificates.test_id=$test_id");
        }


        //Create a new PHPMailer instance
        $mail = new \PHPMailer\PHPMailer\PHPMailer();


        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isHTML(true);
        $email = $email_content['email'];


        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = SMTP_DEBUG;


        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';


        //Set the hostname of the mail server
        $mail->Host = SMTP_HOST;

        $mail->CharSet = 'UTF-8';
        $mail->Port = SMTP_PORT;


        //Set the encryption system to use - ssl (deprecated) or tls
        if (SMTP_AUTH) {

            $mail->SMTPSecure = SMTP_ENCRYPTION;


            //Whether to use SMTP authentication
            $mail->SMTPAuth = SMTP_AUTH;


            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = SMTP_AUTH_USERNAME;


            //Password to use for SMTP authentication
            $mail->Password = SMTP_AUTH_PASSWORD;
        }


        //Set who the message is to be sent from
        $mail->setFrom(SMTP_FROM);


        //Set who the message is to be sent to
        $mail->addAddress($email);


        //Set the subject line
        $mail->Subject = $email_content['email_text_subject'];


        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
        $mail->Body = $email_content['email_text_content'];


        // Create certificate
        $pdf = $this->create_certificate($test_id);


        // Compose PDF file name
        $pdf_name = 'serts/Sertifikaat' . $email_content['certificate_id'] . '.pdf';


        // Write PDF to file
        $pdf->Output($pdf_name, 'F');


        //Add file attachment to email
        $mail->addAttachment($pdf_name);


        // Send the message, check for errors
        if ($mail->send()) {
            header("refresh:5;url=" . BASE_URL . 'certificates');

        } else {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
    }
}