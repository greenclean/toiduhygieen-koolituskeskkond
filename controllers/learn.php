<?php namespace Halo;

use Toiduhygieen\Course;
use Toiduhygieen\Language;
use Toiduhygieen\LearningMaterial;
use Toiduhygieen\Translation;

class learn extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $makstud_koolitus;
    public $courses;
    public $learning_material;

    function index()
    {
        $user = $this->auth->user_id;
        $this->makstud_koolitus = get_all("SELECT *
                                                FROM orders
                                                WHERE user_id = $user AND order_status_id = 1");
        $this->courses = get_all("SELECT * FROM courses");
        $language_id = $this->auth->language_id;

        $course_ids = [];

        foreach ($this->makstud_koolitus as $order) {
            $course_ids[] = $order['course_id'];
        }

        $course_ids = implode(',', $course_ids);
        if ($course_ids) {

            // Get all paid courses in user's language
            $this->learning_material = LearningMaterial::get([
                'language_id' => $language_id,
                'course_id' => "IN($course_ids)"
            ]);
        }
    }

    function edit()
    {
        $course_id = $this->params[0];
        $this->languages = Language::get_all();

        if ($this->auth->is_admin == 0) {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

        if (isset($_POST['submit'])) {
            if ($_POST['language'] === '') {
                $this->flash['danger'] = __('Course language is not selected. Please re-upload the file');
            } else {
                if ($this->upload()) {
                    // Store flash message in session
                    $_SESSION['flash']['success'] = str_replace('%%%', $_FILES['filename']['name'], __('File %%% successfully uploaded'));

                    // Redirect page to the same url the page to prevent duplicate uploads when the user refreshes the page manually (F5)
                    ob_clean();
                    header('Location: ' . BASE_URL . 'learn/edit/' . $course_id);

                } else {
                    $this->flash['danger'] = str_replace('%%%', $_FILES['filename']['name'], __('Failed to upload file %%%'));
                }
            }
        }
        $this->learning_material = LearningMaterial::get(['course_id'=>$course_id]);
    }

    function upload()
    {
        $course_id = $this->params[0];

        $f = $_FILES["filename"];


        global $db;

        $fp = fopen($f["tmp_name"], 'r');
        $content = fread($fp, filesize($f["tmp_name"]));
        $size = filesize($f["tmp_name"]); // 50MB max because of nginx.conf and php.ini settings, although longblob in db(max4GB)
        fclose($fp);

        $data['course_id'] = $course_id;
        $data['language_id'] = $_POST['language'];
        $data['learning_material_name'] = basename($f['name']);
        $data['learning_material_content'] = "$content";
        $data['learning_material_size'] = "$size";

        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        if (!empty($f['name'])) {

            $mime = finfo_file($finfo, $f['tmp_name']);

        } else {
            return false;
        }

        if ($mime == "application/pdf") {

            insert("learning_materials", $data);
            return true;
        } else {
            return false;
        }


    }

    function delete_material()
    {
        $learning_material_id = $_POST['id'];
        q("delete from learning_materials where learning_material_id= $learning_material_id");
        exit('Ok');
    }

    function download_material()
    {
        if (!isValidID($_GET['id'])) {
            $errors[] = __('Wrong id!');
            require 'templates/error_template.php';
            exit();
        }

        $learning_material = LearningMaterial::get($_GET['id'], true);
        if(empty($learning_material)){
            $errors[] = __('This material does not exist!');
            require 'templates/error_template.php';
            exit();
        }

        ob_clean();
        ob_flush();

        header("Content-Transfer-Encoding: binary");
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $learning_material['learning_material_name'] . '"');

        echo $learning_material['learning_material_content'];
        exit();
    }
}