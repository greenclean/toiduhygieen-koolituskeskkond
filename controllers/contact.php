<?php namespace Halo;

class contact extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $lang;
    public $contact;
    public $requires_auth = false;

    function index()
    {
        // TODO: Translate Contact page with same functionality as questions/answers ()
        $this->lang = empty($this->auth->language_id) ? 2 : $this->auth->language_id;
        $this->contact = get_first("SELECT * FROM page_content where page='contact'");
    }

    function edit()
    {
        if ($this->auth->is_admin == 1) {
            $this->contact = get_first("SELECT * FROM page_content WHERE page_content_id=1");
        } else {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }
    }

    function edit_post()
    {
        $contact = $_POST['contact'];
        update("page_content", $contact, "page_content_id=1");
        header('Location: ' . BASE_URL . 'contact');


    }
}