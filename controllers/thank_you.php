<?php namespace Halo;

use Toiduhygieen\BanklinkPayment;
use Toiduhygieen\Course;
use Toiduhygieen\Email;
use Toiduhygieen\Order;

/**
 * Created by PhpStorm.
 * User: hennotaht
 * Date: 7/29/13
 * Time: 21:48
 */
class thank_you extends Controller
{
    public $requires_auth = false;

    function index()
    {
        if (isset($_POST['json'])) {

            $json = $_POST['json'];
            $json = json_decode($json, TRUE);

            $str = (string)$json['amount'] . (string)$json['currency'] . (string)$json['reference'] . (string)$json['transaction'] . (string)$json['status'] . MK_SECRET_KEY;

            if ($json['signature'] == strtoupper(hash('sha512', $str))) {
                if ($json['status'] == "COMPLETED") {

                    $team_lead_order_id = null;
                    $count_workers = 0;
                    $selected_workers = [];
                    $selected_workers['count_workers'] = $count_workers;
                    $order_ids = [];
                    $team_lead_banklink_payment = BanklinkPayment::get_id($json['reference']);


                    // Make sure Maksekeskus did not bring us to the wrong server
                    if(empty($team_lead_banklink_payment)){
                        $errors[] = __('Payment not found on server')." ".$_SERVER['HTTP_HOST'];
                        require 'templates/error_template.php';
                        exit();
                    }

                    $banklink_payments = BanklinkPayment::get_workers($team_lead_banklink_payment['banklink_payment_id']);

                    // Log the user in again if adblocker blocked the session cookie
                    if (empty($_SESSION['user_id'])) {
                        $this->restore_session_from_payment_data($team_lead_banklink_payment);
                    }

                    // Prevent duplicate orders
                    if (Order::exists($team_lead_banklink_payment['banklink_payment_id'])) {
                        exit('<meta http-equiv="refresh" content="0;url=' . BASE_URL . '" />');
                    }

                    // Check if it's a group purchase
                    if (array_key_exists(0, $banklink_payments)) {
                        $count_workers = count($banklink_payments);
                        $selected_workers['count_workers'] = $count_workers;

                        foreach ($banklink_payments as $user => $selected_worker) {
                            $user = get_first("SELECT * FROM users WHERE user_id = $selected_worker[user_id]");
                            if($user) {
                                $worker_course = Course::get_by_user((array) $user)[$selected_worker['course_id']];
                                if($worker_course) {
                                    $selected_workers['workers'][$user['user_id']]['course'] = $worker_course;
                                    $selected_workers['workers'][$user['user_id']]['user'] = $user;

                                    $order_ids[] = Order::create(
                                        $user['user_id'],
                                        $selected_worker['order_date'] = date('Y-m-d H:i:s'),
                                        $selected_worker['course_id'],
                                        $selected_worker['payment_method_id'],
                                        $selected_worker['order_status_id'] = 1,
                                        $selected_worker['order_price'] = $worker_course['course_total_price'],
                                        $selected_worker['order_price_vat'],
                                        $selected_worker['banklink_payment_id'],
                                        $selected_worker['company_name'] ? $selected_worker['company_name'] : ''
                                    );

                                    $this->sendInstructionsToWorker($selected_worker['email'], $selected_worker['language_id'], $selected_worker['personal_code']);

                                    // If user has purchased additional english language version of the certificate
                                    if ($selected_worker['course_id'] == 3) {
                                        update('users', [
                                            'user_has_unused_english_certificate_version' => 1
                                        ], "user_id = $selected_worker[user_id]");
                                        update('certificates', [
                                            'certificate_include_english_version' => 1
                                        ], "user_id = $selected_worker[user_id]");
                                        update('orders', [
                                            'course_id' => 2
                                        ], "user_id = $selected_worker[user_id] and course_id = 1");

                                    }
                                }
                            }
                        }

                        unset($_SESSION['selected_workers']);

                        if (!empty($order_ids)) {
                            $orders = Order::getMulti($order_ids);
                            foreach ($selected_workers['workers'] as $key => $worker) {
                                foreach ($orders as $order) {
                                    if($order['user_id'] == $key) {
                                        $selected_workers['workers'][$key]['order'] = $order;
                                        $selected_workers['invoice_nr'] = $order['order_id'];
                                        $selected_workers['payment_method_id'] = $order['payment_method_id'];
                                        $selected_workers['order_date'] = $order['order_date'];
                                    }
                                }
                            }
                        }
                    } else {
                        $course =  Course::get_by_user((array) $this->auth)[$team_lead_banklink_payment['course_id']];

                        $order_id = Order::create(
                            $team_lead_banklink_payment['user_id'],
                            $team_lead_banklink_payment['order_date'] = date('Y-m-d H:i:s'),
                            $team_lead_banklink_payment['course_id'],
                            $team_lead_banklink_payment['payment_method_id'] = 3,
                            $team_lead_banklink_payment['order_status_id'] = 1,
                            $team_lead_banklink_payment['order_price'] = Course::get_by_user((array) $this->auth)[$team_lead_banklink_payment['course_id']]['course_total_price'],
                            $team_lead_banklink_payment['order_price_vat'],
                            $team_lead_banklink_payment['banklink_payment_id'],
                            $team_lead_banklink_payment['company_name'] ? $team_lead_banklink_payment['company_name'] : ''
                        );

                        if($order_id) {
                            $order = Order::get($order_id);
                            $selected_workers['invoice_nr'] = $order['order_id'];
                            $selected_workers['payment_method_id'] = $order['payment_method_id'];
                            $selected_workers['order_date'] = $order['order_date'];
                            $selected_workers['course'] = $course;
                            $selected_workers['user'] = $this->auth;
                            $selected_workers['order'] = $order;
                        }

                        // If user has purchased additional english language version of the certificate
                        if ($team_lead_banklink_payment['course_id'] == 3) {
                            update('users', [
                                'user_has_unused_english_certificate_version' => 1
                            ], "user_id = $team_lead_banklink_payment[user_id]");
                            update('certificates', [
                                'certificate_include_english_version' => 1
                            ], "user_id = $team_lead_banklink_payment[user_id]");
                            update('orders', [
                                'course_id' => 2
                            ], "user_id = $team_lead_banklink_payment[user_id] and course_id = 1");

                        }
                    }

                    BanklinkPayment::update_order_status_id($team_lead_banklink_payment['banklink_payment_id']);

                    // Send email notification to admin about new invoice request when paid by invoice
                    if(!empty($selected_workers)) {
                        $this->sendInvoiceMail($selected_workers);
                    }

                    exit('<meta http-equiv="refresh" content="0;url=' . BASE_URL . '" />');

                } else {
                    exit('<meta http-equiv="refresh" content="0;url=' . BASE_URL . 'payment/error?error=1" />');
                }

            } else {
                exit('<meta http-equiv="refresh" content="0;url=' . BASE_URL . 'payment/error?error=2" />');
            }

        } else {
            exit('<meta http-equiv="refresh" content="0;url=' . BASE_URL . 'payment/error?error=3" />');
        }
    }

    private function sendInvoiceMail(array $selected_workers)
    {
        $language = $this->auth->language_id;
        $invoice_email_content = Email::get_content($language, 5);

        ob_start();
        include 'views/payment/pdf_for_email_offer.php';
        $html = ob_get_clean();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullwidth');

        $file_name = 'Invoice_nr_' . $selected_workers['invoice_nr'] . '.pdf';
        $file = $mpdf->Output($file_name, 'S');

        send_mail(
            $this->auth->email,
            $invoice_email_content['email_text_subject'],
            $invoice_email_content['email_text_content'],
            $this->auth->name,
            $file,
            $file_name
        );

        send_mail(
            'arved@greenclean.ee',
            $invoice_email_content['email_text_subject'],
            $invoice_email_content['email_text_content'],
            '',
            $file,
            $file_name
        );
    }

    private function sendInstructionsToWorker($user_email, $language, $personal_code)
    {
        $email_type_id = $this->checkIfPersonalCode($personal_code);
        $instrucations_email_content = Email::get_content($language, $email_type_id);

        ob_start();
        send_mail(
            $user_email,
            $instrucations_email_content['email_text_subject'],
            $instrucations_email_content['email_text_content'],
            $this->auth->name
        );
    }

    private function checkIfPersonalCode($personal_code)
    {
        // Check if user has personal code or birth year(YYYY-MM-DD) and send instruction e-mail accordingly
        return (strpos($personal_code, '-') === false) ? 6 : 7;
    }

    /**
     * @param array $team_lead_banklink_payment
     */
    private function restore_session_from_payment_data(array $team_lead_banklink_payment)
    {
        $_SESSION['user_id'] = $team_lead_banklink_payment['user_id'];

        foreach (get_first("select * from users where user_id='$_SESSION[user_id]';") as $user_attr => $value) {
            $this->auth->$user_attr = $value;

            // For SmartSupp (error_template can't access $this)
            $_SESSION['user'][$user_attr] = $value;
        }
        $this->auth->logged_in = TRUE;
    }

}
