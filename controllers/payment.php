<?php namespace Halo;

use Toiduhygieen\BanklinkPayment;
use Toiduhygieen\Course;
use Toiduhygieen\Order;

/**
 * @property float vat_price
 * @property float total_price
 */
class payment extends Controller
{
    const PAY_BY_INVOICE = 1,
        PAY_BY_TRANSFER = 2,
        PAY_BY_MAKSEKESKUS = 3;
    public $testEnv,
        $shopId,
        $auth,
        $secretKey,
        $transactionId,
        $publishableKey,
        $controller,
        $action,
        $params,
        $vat_price,
        $total_price;

    function index()
    {
        $this->vat_price = 0;
        $this->total_price = 0;
        $course_id = empty($this->params[0]) ? null : $this->params[0];
        validate($course_id);

        // Logged-in user tries to purchase a course for himself
        if (empty($_POST['user_id'])) {
            $_SESSION['selected_workers'][$this->auth->user_id] = (array)$this->auth;
        } // Team leader purchases for his workers
        else if (is_array($_POST['user_id'])) {

            // Clear session selected workers
            unset($_SESSION['selected_workers']);

            // Iterate over selected workers
            foreach (array_keys($_POST['user_id']) as $user_id) {
                $_SESSION['selected_workers'][$user_id] = [];
            }
        } else {
            throw new \Exception(__('Error'));
        }

        // Get relevant users' data
        $user_ids = array_keys($_SESSION['selected_workers'] ?? [0])[0];
        $users = get_all("SELECT * FROM users WHERE user_id IN( $user_ids)");

        // Add additional data to selected_workers
        foreach ($users as $user) {
            $allCourses = Course::get_by_user((array)$user);
            $worker_course = $allCourses[$course_id] ?? null; // Updated line 58
            $_SESSION['selected_workers'][$user['user_id']]['name'] = $user['name'];
            $_SESSION['selected_workers'][$user['user_id']]['personal_code'] = $user['personal_code'];
            $_SESSION['selected_workers'][$user['user_id']]['course'] = $worker_course;
        }

        // Calculate totals
        foreach ($_SESSION['selected_workers'] as $worker) {
            if (isset($worker['course']['course_price']) && isset($worker['course']['course_total_price'])) {  // Added check
                $this->vat_price += round($worker['course']['course_price'] * VAT_PERCENT / 100, 2);
                $this->total_price += round($worker['course']['course_total_price'], 2);
            }
        }

    }


    function orders()
    {
        $payment_total_price = 0;

        // Safety first
        if (empty($_POST['id'])) {
            throw new \Exception('Course id not given');
        }

        $team_lead_order_id = null;
        $count_workers = 0;
        $selected_workers = [];
        $selected_workers['count_workers'] = $count_workers;
        $selected_workers['company_name'] = isset($_POST['company_name']) ? $_POST['company_name'] : '';
        $order_ids = [];

        // Check for multiple invoice rows
        if (isset($_SESSION['selected_workers'])) {
            $count_workers = count($_SESSION['selected_workers']);
            $selected_workers['count_workers'] = $count_workers;
            foreach ($_SESSION['selected_workers'] as $user_id => $selected_worker) {
                $user = get_first("SELECT * FROM users WHERE user_id = $user_id");
                if ($user) {
                    $worker_course = Course::get_by_user((array)$user)[$_POST['id']];
                    if ($worker_course) {
                        $selected_workers['workers'][$user_id]['course'] = $worker_course;
                        $selected_workers['workers'][$user_id]['user'] = $user;

                        // Creating new order
                        $order['course_id'] = $_POST['id'];
                        $order['user_id'] = $user_id;
                        $order['company_name'] = isset($_POST['company_name']) ? $_POST['company_name'] : '';
                        $order['payment_method_id'] = isset($_POST['saada']) ? self::PAY_BY_INVOICE : self::PAY_BY_TRANSFER;
                        $order['order_price'] = $worker_course['course_total_price'];
                        $order['order_price_vat'] = VAT_PERCENT;
                        $order_ids[] = insert("orders", $order);

                        $payment_total_price += $order['order_price'];
                    }
                }
            }

            unset($_SESSION['selected_workers']);

            if (!empty($order_ids)) {
                $orders = Order::getMulti($order_ids);
                foreach ($selected_workers['workers'] as $key => $worker) {
                    foreach ($orders as $order) {
                        if ($order['user_id'] == $key) {
                            $selected_workers['workers'][$key]['order'] = $order;
                            $selected_workers['invoice_nr'] = $order['order_id'];
                            $selected_workers['payment_method_id'] = $order['payment_method_id'];
                            $selected_workers['order_date'] = $order['order_date'];
                        }
                    }
                }
            }
        }

        if ($payment_total_price == 0) {
            foreach ($order_ids as $order_id) {
                Order::setStatus($order_id, ORDER_STATUS_CONFIRMED);

            }
        }

        // Send email notification to admin about new invoice request when paid by invoice
        if (!empty($selected_workers)) {
            $this->sendInvoiceMail($selected_workers);
        }

        // Redirect to front page
        exit('<meta http-equiv="refresh" content="0;url=' . BASE_URL . '" />');
    }

    private function sendInvoiceMail(array $selected_workers)
    {
        $invoice_email_content = get_first("SELECT * FROM email_texts
                                               LEFT JOIN email_type USING (email_type_id)
                                             WHERE email_type_id = 4 AND language_id = " . $this->auth->language_id);
        ob_start();
        include 'views/payment/pdf_for_email_offer.php';
        $html = ob_get_clean();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullwidth');

        $file_name = 'Invoice_nr_' . $selected_workers['invoice_nr'] . '.pdf';
        $file = $mpdf->Output($file_name, 'S');

        send_mail(
            $this->auth->email,
            $invoice_email_content['email_text_subject'],
            $invoice_email_content['email_text_content'],
            $this->auth->name,
            $file,
            $file_name
        );

        send_mail(
            'arved@greenclean.ee',
            $invoice_email_content['email_text_subject'],
            $invoice_email_content['email_text_content'],
            '',
            $file,
            $file_name
        );
    }


    function maksekeskusPaymentMethods($course_id, $selected_payment_method)
    {
        require_once 'classes/Maksekeskus/Maksekeskus.php';

        $maksekeskus = new \Maksekeskus\Maksekeskus(MK_SHOP_ID, MK_PUBLISHABLE_KEY, MK_SECRET_KEY, MK_TEST_ENV);

        $returnFromMaksekeskus = 'http://localhost/toiduhygieen/thankyou'; // TEST
        // ^ Selle rea ja all $request body sees transaction_urli koos tema sisuga võib enne live'i pushimist ära võtta

        $order_total_price = 0;
        foreach ($_SESSION['selected_workers'] as $user) {
            $order_total_price = round(($user['course']['course_total_price'] + $order_total_price), 2);
        }

        // $order_total_price = round($course['course_total_price'] * count($_SESSION['selected_workers']), 2);
        $banklink_payment_id = null;
        BanklinkPayment::create($this->auth->user_id, $order_date = date('Y-m-d H:i:s'), $course_id, self::PAY_BY_MAKSEKESKUS, 3, $order_total_price, VAT_PERCENT);
        $banklink_payment_id = BanklinkPayment::get($course_id, $this->auth->user_id)['banklink_payment_id'];
        foreach ($_SESSION['selected_workers'] as $user_id => $selected_worker) {
            BanklinkPayment::create_worker($banklink_payment_id, $user_id);
        }

        $request_body = array(
            'transaction' => array(
                'amount' => $order_total_price,
                'currency' => "EUR",
                'reference' => $banklink_payment_id,
            ),
            'customer' => array(
                'ip' => $_SERVER['REMOTE_ADDR'],
                "email" => $this->auth->email,
                "country" => "ee",
                "locale" => "en"
            )
        );

        $transaction = $maksekeskus->createTransaction($request_body);

        $transaction_check = $maksekeskus->getTransaction($transaction->id);

        $request_params = array(
            'transaction' => $transaction->id,
        );

        $methods = $maksekeskus->getPaymentMethods($request_params);

        $available_methods = array();

        foreach ($methods->banklinks as $method) {
            $method_name = ucfirst($method->name);
            $available_methods["$method_name"] = $method->url;
        }

        foreach ($available_methods as $method_name => $method_link) {
            if ($method_name === $selected_payment_method) {
                return $method_link;
            }
        }

    }

    function cancel()
    {
        $errors[] = __('Payment has been declined');
        require 'templates/error_template.php';
        exit();
    }

    public function error()
    {
        $error_code = $_GET['error'];
        $errors = [];
        if ($error_code) {
            switch ($error_code):
                case 1:
                    $error = __('Payment has been declined');
                    break;
                case 2:
                    $error = __('signature is not valid');
                    break;
                case 3:
                    $error = __('Payment has been declined');
                    break;
            endswitch;

            $errors[] = $error;
        }

        require 'templates/error_template.php';
        exit();
    }

    function ajax_choose_payment_method()
    {
        if (empty($_POST['id']) && (empty($_POST['method']))) {
            throw new \Exception('Course id not given');
        }

        $payment_method = $_POST['method'];
        $course_id = $_POST['id'];

        $link = $this->maksekeskusPaymentMethods($course_id, $payment_method);

        // Redirect to MK
        stop(200,$link);
    }


}