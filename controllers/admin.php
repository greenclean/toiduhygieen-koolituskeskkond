<?php namespace Halo;

use HTMLPurifier;
use HTMLPurifier_Config;
use Toiduhygieen\Course;
use Toiduhygieen\Email;
use Toiduhygieen\Language;
use Toiduhygieen\Pagination;
use Toiduhygieen\Question;
use Toiduhygieen\Translation;
use Toiduhygieen\User;

class admin extends Controller
{
    public $auth;
    public $controller;
    public $action;
    public $params;
    public $types;
    public $languages;
    public $showOnlyFaulty;
    public $courses;
    public $pagination;
    public $users;
    public $emails;
    public $languages_in_use;
    public $languages_not_in_use;
    public $show_untranslated;
    public $translations;
    public $statistics;
    public $translations_where_phrase_is_too_long;

    public function __construct($auth)
    {

        $this->auth = $auth;

        if ($this->auth->is_admin != 1) {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }
    }

    function index()
    {

    }

    function email()
    {
        if ($this->auth->is_admin == 1) {
            $this->types = get_all("SELECT * FROM email_type");
            $this->languages = Language::get_all();
            $this->emails = get_all("SELECT * FROM email_texts LEFT JOIN email_type USING (email_type_id) LEFT JOIN languages USING (language_id)");
        } else {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

    }

    function ajax_email_edit()
    {
        $id = $_POST['id'];
        $this->types = get_all("SELECT * FROM email_type");
        $this->languages = Language::get_all();
        $email_edit = get_all("SELECT * FROM email_texts LEFT JOIN email_type using (email_type_id) LEFT JOIN languages using (language_id) WHERE email_text_id=$id");
        header('Content-Type: application/json');
        exit(json_encode(['result' => 'OK', 'data' => $email_edit]));


    }

    function post_email_text_edit()
    {

        if (isset($_POST['go'])) {
            $id = $_POST['id'];
            $data = $_POST['data'];
            update("email_texts", $data, "email_text_id=$id");
            header("Location:" . BASE_URL . 'admin/email');
            exit();
        }
    }

    function ajax_email_delete()
    {
        $email_text_id = $_POST['id'];
        q("delete from email_texts where email_text_id= $email_text_id");
        exit('OK');

    }

    function customers()
    {
        $type = empty($_GET['type']) ? "all" : $_GET['type'];
        $this->showOnlyFaulty = empty($_GET['showOnlyFaulty']) ? "" : 'checked';

        $this->courses = get_all("SELECT * FROM courses WHERE course_type = 'course'");
        if ($this->auth->is_admin !== "1") {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

        $this->pagination = new Pagination(['users.user_id', 'users.name', 'courses.course_name',
            'users.personal_code', 'users.email', 'teamLeader.name']);

        if ($this->showOnlyFaulty) {
            $this->pagination->criteria[] = "users.faulty_personal_code = 1";
        }

        $this->users = User::get(
            $this->pagination->criteria,
            $this->pagination->offset,
            $this->pagination->perPage);

        $this->pagination->setTotal(
            isset($this->users[0]) ? $this->users[0]["user_count"] : 0);

        $this->languages = Language::get_all_in_use();

    }

    function ajax_check_email()
    {
        stop(200, ['emailCheck' => Email::checkEmail($_POST['email'])]);
    }

    function ajax_customer_save()
    {

        validate($_POST['user_id'], IS_NON_EMPTY, 'user_id');
        validate($_POST['data'], IS_ARRAY, 'data');

        User::validate($_POST['data'], $_POST['user_id']);

        $user = get_first("SELECT * FROM users WHERE user_id=$_POST[user_id]");
        $data = $_POST['data'];
        $data['employer_id'] = empty($data['employer_id']) || !is_numeric($data['employer_id']) ? null : $data['employer_id'];

        $worker_ids = empty($_POST['worker_ids']) ? [] : explode(',', $_POST['worker_ids']);
        User::setWorkers($_POST['user_id'], $worker_ids);

        if ($user['employer_id']) {
            $old_teamlider = get_first("SELECT * FROM users where user_id = $user[employer_id]");
            $replacements = [
                $old_teamlider['name'],
                $user['name']
            ];

            if ($data['employer_id'] && $user['employer_id'] != $data['employer_id']) {
                //You were removed from {teamleader username} team
                $this->sendMail($replacements, $user['email'], $user['language_id'], 10);

                //{employee username} has left your team
                $this->sendMail($replacements, $old_teamlider['email'], $old_teamlider['language_id'], 14);

                //You are now a member of {teamleader username} team
                $new_teamlider = get_first("SELECT * FROM users where user_id = $data[employer_id]");

                $replacements = [
                    $new_teamlider['name'],
                    $user['name']
                ];

                $this->sendMail($replacements, $user['email'], $user['language_id'], 11);

                //{employee username} is now a member of your team
                $this->sendMail($replacements, $new_teamlider['email'], $new_teamlider['language_id'], 13);

            } elseif (empty($data['employer_id'])) {
                //You were removed from {teamleader username} team
                $this->sendMail($replacements, $user['email'], $user['language_id'], 10);

                //{employee username} has left your team
                $this->sendMail($replacements, $old_teamlider['email'], $old_teamlider['language_id'], 14);
            }
        } else {
            if ($data['employer_id']) {
                //You are now a member of {teamleader username} team
                $new_teamlider = get_first("SELECT * FROM users where user_id = $data[employer_id]");

                $replacements = [
                    $new_teamlider['name'],
                    $user['name']
                ];

                $this->sendMail($replacements, $user['email'], $user['language_id'], 11);

                //{employee username} is now a member of your team
                $this->sendMail($replacements, $new_teamlider['email'], $new_teamlider['language_id'], 13);
            }
        }

        if ($data['allow_inherit']) {
            $data['allow_inherit'] = 1;
        } else {
            $data['allow_inherit'] = 0;
        }

        update("users", $data, "user_id=$_POST[user_id]");
        q("delete FROM course_user WHERE user_id = $_POST[user_id]");
        if (!empty($_POST['courses'])) {
            foreach ($_POST['courses'] as $course) {
                insert('course_user', ['user_id' => $_POST['user_id'], 'course_id' => $course]);
            }
        }

    }

    private function sendMail($replacements, $user_email, $language_id, $email_num)
    {
        $patterns = [
            '/{teamleader}/',
            '/{employee username}/',
        ];

        $language_id = $language_id;
        $email = Email::get_content($language_id, $email_num);
        $email_subject = preg_replace($patterns, $replacements, $email['email_text_subject']);
        $email_content = preg_replace($patterns, $replacements, $email['email_text_content']);
        send_mail($user_email, $email_subject, $email_content);
    }

    function ajax_customer_edit()
    {
        $id = $_POST['id'];
        $user = get_first("SELECT u.user_id, u.name, u.email, u.language_id, u.personal_code, u.allow_inherit, u.employer_id, tl.name as team_leader_name FROM users u
                                  LEFT JOIN users tl ON tl.user_id = u.employer_id
                                  WHERE u.user_id= $id GROUP BY u.user_id");

        if ($user) {
            $user['user_courses'] = get_all('SElECT * FROM course_user where user_id = ' . $user['user_id']);
            $user['workers'] = get_all("SELECT * FROM users WHERE employer_id = $user[user_id]");

            header('Content-Type: application/json');
            stop(200, $user);
        }
    }

    function get_workers()
    {
        $ids = $_POST['id'];
        $user = get_all("SELECT * FROM users  WHERE user_id IN ($ids) AND user_id != " . $this->auth->user_id);

        if ($user) {
            header('Content-Type: application/json');
            exit(json_encode(['result' => 'Ok', 'data' => $user]));
        }

        exit();
    }

    public function translation()
    {
        $show_untranslated = empty($_GET['show_untranslated']) ? [] : explode(',', $_GET['show_untranslated']);
        $this->languages_in_use = Language::get_all_in_use();
        $this->languages_not_in_use = Language::get_all_not_in_use();
        $this->show_untranslated = array_flip($show_untranslated);
        $this->translations = Translation::get_untranslated($show_untranslated);
        $this->statistics = Translation::get_statistics();
        $this->translations_where_phrase_is_too_long = Translation::get_all_languages(['LENGTH(translation_phrase) >= 765']);

    }

    public function AJAX_translation_edit()
    {

        if (empty($_POST['translation_id']) || !is_numeric($_POST['translation_id']) || $_POST['translation_id'] <= 0) {
            stop(400, 'Invalid translation_id');
        }
        if (empty($_POST['language_code'])) {
            stop(400, 'Invalid languageCode');
        }

        $translation = $this->purify_html($_POST['translation']);

        // Make it so that an empty translation allows for automatic translation
        $translation = $translation === '' ? null : $translation;

        update('translations', [
            "translation_in_$_POST[language_code]" => $translation
        ], "translation_id = $_POST[translation_id]");
    }

    public function AJAX_translation_edit_dynamic()
    {

        if (empty($_POST['translation_source_id']) || !is_numeric($_POST['translation_source_id']) || $_POST['translation_source_id'] <= 0) {
            stop(400, 'Invalid translation_source_id');
        }
        if (empty($_POST['translation_source'])) {
            stop(400, 'Invalid translation_source');
        }
        if (empty($_POST['language_code'])) {
            stop(400, 'Invalid language_code');
        }
        $translation = $this->purify_html($_POST['translation']);

        // Remove leading/trailing whitespace from translation
        $translation = trim($translation);

        // Make it so that an empty translation allows for automatic translation
        $translation = $translation === '' ? null : $translation;

        $translation_source = addslashes($_POST['translation_source']);

        if (get_one("SELECT * FROM translations WHERE translation_source = '$translation_source' AND translation_source_id = $_POST[translation_source_id]")) {
            update('translations', [
                "translation_in_$_POST[language_code]" => $translation
            ], "translation_source = '$translation_source' AND translation_source_id = $_POST[translation_source_id]");
        } else {
            Translation::add($translation, 'questions.explanation', $_POST['translation_source_id']);
            update('translations',
                ["translation_in_$_POST[language_code]" => $translation],
                "translation_source = 'questions.explanation' AND translation_source_id =$_POST[translation_source_id]");
        }

    }

    public function purify_html($html): string
    {

        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $purifier = new HTMLPurifier($config);
        return $purifier->purify($html);
    }

    public function AJAX_translation_add_language()
    {

        if (!Language::is_valid_code($_POST['language_code']) || is_numeric($_POST['language_code'])) {
            stop(400, "Invalid languageCode");
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        Language::add($_POST['language_code']);

        if ($_POST['language_code'] != 'en') {
            Translation::insert_missing_dynamic_phrases();
            Translation::google_translate_missing_translations($_POST['language_code']);
        }
    }

    public function AJAX_translation_delete_language()
    {

        if (!Language::is_valid_code($_POST['language_code'])) {
            stop(400, "Invalid languageCode");
        }

        if (empty($_POST['language_id'])) {
            stop(400, "Invalid language_id");
        }

        foreach (Course::language_ids_in_use() as $result) {
            if ($result['language_id'] === $_POST['language_id']) {
                stop(400, "Courses with " . strtoupper($_POST['language_code']) . " primary language found");
            }
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        Language::delete($_POST['language_code']);

    }

    public function AJAX_translate_remaining_strings()
    {
        if (!Language::is_valid_code($_POST['language_code'])) {
            stop(400, "Invalid languageCode");
        }
        Translation::google_translate_missing_translations($_POST['language_code']);
        $stats = Translation::get_statistics([$_POST['language_code']]);

        // Return remaining untranslated string count
        stop(200, ['untranslatedCount' => $stats[$_POST['language_code']]['remaining']]);

    }

    public function AJAX_translate_primary_language_phrases_for_new_question_modal()
    {
        // Prevent automatic translation if a field is empty, to avoid separators in translated strings
        foreach ($_POST['primary_phrases'] as $untranslatedString) {
            if (empty($untranslatedString)) {
                stop(400, "Empty primary-language field(s)");
            }
        }

        if (empty($_POST['primary_phrases']) || !is_array($_POST['primary_phrases']) || empty($_POST['primary_phrases'][0])) {
            stop(400, "Invalid primary_phrases strings");
        }
        if (empty($_POST['language_from']) || !Language::is_valid_code($_POST['language_from'])) {
            stop(400, "Invalid language_from");
        }
        if (empty($_POST['language_to']) || !Language::is_valid_code($_POST['language_to'])) {
            stop(400, "Invalid language_to");
        }

        stop(200, Translation::google_translate($_POST['language_from'], $_POST['language_to'], $_POST['primary_phrases']));

    }

    public function courses()
    {
        $this->courses = get_all("SELECT * FROM courses WHERE course_type = 'course'");
        $this->languages_in_use = Language::get_all_in_use();
    }

    public function course_save()
    {
        if (isset($_POST['save'])) {
            $data = $_POST['data'];
            $data['course_type'] = 'course';

            if (isset($data['default_course']) && !empty($data['default_course'])) {
                $data['default_course'] = 1;
            } else {
                $data['default_course'] = 0;
            }

            if (isset($data['in_login_status']) && !empty($data['in_login_status'])) {
                $data['in_login_status'] = 1;
            } else {
                $data['in_login_status'] = 0;
            }

            if (isset($data['course_repeatable']) && !empty($data['course_repeatable'])) {
                $data['course_repeatable'] = 1;
            } else {
                $data['course_repeatable'] = 0;
            }

            $course = insert('courses', $data);

            if ($course && $data['default_course']) {
                $this->courseUsers($data['default_course'], $course);
            }

            header("Location:" . BASE_URL . 'admin/courses');
            exit();
        }
    }

    private function courseUsers($default_course, $course_id)
    {
        $users = get_all('SELECT user_id FROM users WHERE is_admin != 1');

        if ($default_course == 1) {
            foreach ($users as $user) {
                insert('course_user', ["course_id" => $course_id, 'user_id' => $user['user_id']]);
            }
        } else {
            foreach ($users as $user) {
                q("delete FROM course_user WHERE course_id = $course_id AND user_id= $user[user_id]");
            }
        }
    }

    public function edit_course()
    {
        $this->languages_in_use = Language::get_all_in_use();
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $this->course = get_first("SELECT * FROM courses WHERE course_id=$id");
        } else {
            header("Location:" . BASE_URL . 'admin/courses');
            exit();
        }
    }

    public function course_update()
    {
        if (isset($_POST['update'])) {
            $data = $_POST['data'];
            $data['course_type'] = 'course';

            if (isset($data['default_course']) && !empty($data['default_course'])) {
                $data['default_course'] = 1;
            } else {
                $data['default_course'] = 0;
            }

            if (isset($data['in_login_status']) && !empty($data['in_login_status'])) {
                $data['in_login_status'] = 1;
            } else {
                $data['in_login_status'] = 0;
            }

            if (isset($data['course_repeatable']) && !empty($data['course_repeatable'])) {
                $data['course_repeatable'] = 1;
            } else {
                $data['course_repeatable'] = 0;
            }

            $this->courseUsers($data['default_course'], $_POST['course_id']);
            update("courses", $data, "course_id = $_POST[course_id]");
        }
        header("Location:" . BASE_URL . 'admin/courses');
        exit();
    }

    public function delete_course()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            q("delete FROM questions WHERE course_id=$id");
            q("delete FROM courses WHERE course_id=$id");
            q("delete from learning_materials where course_id= $id");
        }
        header("Location:" . $_SERVER['HTTP_REFERER']);
        exit();
    }

    public function questions()
    {
        $course_id = (int)$this->params[0];
        $this->categories = get_all("SELECT * FROM test_categories");
        $this->questions = Question::get_all(['course_id = ' . $course_id]);
        $this->translated_languages = Language::get_all_in_use();
        $this->course = Course::get($course_id);
        $this->course_language = Language::get($this->course['language_id']);

        // Remove primary language from translated language
        unset($this->translated_languages[$this->course_language['language_code']]);
    }

    public function edit_questions()
    {
        $question_id = $this->params[1];
        $this->categories = get_all("SELECT * FROM test_categories");
        $this->languages = Language::get_all();
        if ($question_id) {
            $this->question = get_first("SELECT * FROM questions WHERE question_id = $question_id");
            $this->answers = get_all("SELECT * FROM answers WHERE question_id = $question_id");
        } else {
            header("Location:" . $_SERVER['HTTP_REFERER']);
            exit();
        }
    }

    public function question_update()
    {
        if (isset($_POST['update'])) {
            $data = $_POST['data'];
            $question_id = $_POST['question_id'];

            if ($_POST['new_category']) {
                $new_category_id = insert('test_categories', ['test_category_name' => $_POST['new_category']]);
                if ($new_category_id) {
                    $data['test_category_id'] = $new_category_id;
                }
            }

            update("questions", $data, "question_id = " . $question_id);

            $answers = $_POST['answer'];
            $orders = range('A', 'Z');

            foreach ($answers as $key => $answer) {
                $answer_data = [];
                if ($key == $_POST['correct_answer']) {
                    $answer_data['answer_is_correct'] = 1;
                } else {
                    $answer_data['answer_is_correct'] = 0;
                }

                $answer_data['answer'] = $answer;
                $answer_data['answer_letter'] = $orders[$key];

                if (key_exists($key, $_POST['answer_id'])) {
                    update("answers", $answer_data, "answer_id = " . $_POST['answer_id'][$key]);
                } else {
                    $answer_data['question_id'] = $question_id;
                    insert('answers', $answer_data);
                }
            }
        }

        header("Location:" . BASE_URL . 'admin/questions/' . $this->params[0]);
        exit();
    }

    public function delete_question()
    {
        $question_id = $this->params[0];
        if ($question_id) {
            q("delete FROM answers WHERE question_id = $question_id");
            q("delete FROM questions WHERE question_id = $question_id");
        }
        header("Location:" . $_SERVER['HTTP_REFERER']);
        exit();
    }

    public function AJAX_delete_question()
    {
        if (empty($_POST['question_id']) || !is_numeric($_POST['question_id']) || $_POST['question_id'] < 0) {
            stop(400, 'Invalid question_id');
        }

        q("delete FROM translations WHERE translation_source_id = $_POST[question_id]");
        q("delete FROM questions WHERE question_id = $_POST[question_id]");
    }

    public function AJAX_add_answer_row_to_main_table()
    {
        if (empty($_POST['question_id']) || !is_numeric($_POST['question_id']) || $_POST['question_id'] < 0) {
            stop(400, 'Invalid question_id');
        }

        if (empty($_POST['answer_count']) || !is_numeric($_POST['answer_count']) || $_POST['answer_count'] < 0) {
            stop(400, 'Invalid answer_count');
        }

        $alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        $answer_id = insert('answers', [
            'answer_letter' => $alphabet[$_POST['answer_count']],
            'answer_is_correct' => 0,
            'question_id' => $_POST['question_id']
        ]);
        Translation::add("answer_id $answer_id for question_id $_POST[question_id]", 'answers', $answer_id);
        stop(200, $answer_id);
    }

    public function AJAX_delete_answer()
    {
        if (empty($_POST['answer_id']) || !is_numeric($_POST['answer_id']) || $_POST['answer_id'] < 0) {
            stop(400, 'Invalid answer_id');
        }

        q("delete FROM answers WHERE answer_id = $_POST[answer_id]");
        q("delete FROM translations WHERE translation_source_id = $_POST[answer_id] AND translation_source = 'answers'");
    }

    public function AJAX_question_category_edit()
    {

        if (empty($_POST['question_id']) || !is_numeric($_POST['question_id']) || $_POST['question_id'] < 0) {
            stop(400, 'Invalid question_id');
        }

        if (empty($_POST['test_category_id']) || !is_numeric($_POST['test_category_id']) || $_POST['test_category_id'] < 0) {
            stop(400, 'Invalid test_category_id');
        }

        update('questions', [
            'test_category_id' => $_POST['test_category_id']
        ], "question_id = $_POST[question_id]");
    }

    public function AJAX_question_correct_answer_edit()
    {

        if (empty($_POST['question_id']) || !is_numeric($_POST['question_id']) || $_POST['question_id'] < 0) {
            stop(400, 'Invalid question_id');
        }

        if (empty($_POST['answer_id']) || !is_numeric($_POST['answer_id']) || $_POST['answer_id'] < 0) {
            stop(400, 'Invalid answer_id');
        }

        update('answers', [
            'answer_is_correct' => 0
        ], "question_id = $_POST[question_id]");

        update('answers', [
            'answer_is_correct' => 1
        ], "answer_id = $_POST[answer_id]");

    }

    public function AJAX_save_new_question()
    {

        if (empty($_POST['question']) || !array($_POST['question']) || count($_POST['question']) < 6) {
            stop(400, 'Invalid question');
        }

        $q = $_POST['question'];

        if (empty($q['category_id']) || $q['category_id'] == 0) {
            stop(400, 'Invalid category');
        }

        if (empty($q['course_id'])) {
            stop(400, 'Invalid course_id');
        }

        if (empty($q['course_language']) || !in_array($q['course_language'], Language::get_codes_in_use())) {
            stop(400, 'Invalid course_language');
        }

        $alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        $question_id = insert('questions', [
            'course_id' => $q['course_id'],
            'test_category_id' => $q['category_id']
        ]);

        foreach ($q['answers'] as $answer_number => $answer) {
            $answer_is_correct = $answer['answerIsCorrect'] === "false" ? 0 : 1;
            $answer_id = insert('answers', [
                'answer_letter' => $alphabet[$answer_number],
                'answer_is_correct' => $answer_is_correct,
                'question_id' => $question_id
            ]);
            $this->insertPhraseTranslationsToTranslationsTable($answer['translations'], 'answers', $q['course_language'], $answer_id);
        }

        $this->insertPhraseTranslationsToTranslationsTable($q['question'], 'questions', $q['course_language'], $question_id);
        $this->insertPhraseTranslationsToTranslationsTable($q['explanation'], 'questions.explanation', $q['course_language'], $question_id);

    }

    /**
     * @param $q
     * @return array
     */
    public function getCriteria($q)
    {
        if (empty($q) || !is_string($q)) {
            return [];
        }
        $criteria = explode(' ', $q);
        var_dump($criteria);
        foreach ($criteria as $criterion) {
            $criterion = addslashes($criterion);
            $criteria2[] = "(course_name LIKE '%$criterion%' OR "
                . "email LIKE '%$criterion%' OR "
                . "name LIKE '%$criterion%' OR "
                . "order_date LIKE '%$criterion%' OR "
                . "order_id LIKE '%$criterion%' OR "
                . "order_price LIKE '%$criterion%' OR "
                . "order_status_name LIKE '%$criterion%' OR "
                . "payment_method_name LIKE '%$criterion%' OR "
                . "personal_code LIKE '%$criterion%')";
        }
        return $criteria2;
    }


    /**
     * Insert dynamic phrase translations to translations table
     * @param $translations
     * @param $source_table
     * @param $course_language
     * @param $id
     * @return bool|int
     */
    private function insertPhraseTranslationsToTranslationsTable($translations, $source_table, $course_language, $id)
    {
        // Inserts to translation_phrase in course_language which is not always english, however, automatic translation in translations table should detect from_language instead of using 'en' constant
        $data = [
            'translation_phrase' => "$source_table#$id",
            'translation_state' => 'dynamic',
            'translation_source' => $source_table,
            'translation_source_id' => $id
        ];

        $data = $this->add_translation_columns($translations, $data);

        insert('translations', $data);

        // Prevent gaps in translations.translation_id due to auto_increment increasing with ON DUPLICATE KEY UPDATE..
        Translation::$translations[$translations[$course_language]] = $translations[$course_language];
    }

    /**
     * @param $translations
     * @param array $data
     * @return array
     */
    private function add_translation_columns($translations, array $data): array
    {

        // Add translation columns
        foreach ($translations as $language_code => $translation) {
            $data["translation_in_$language_code"] = $translation;
        }
        return $data;
    }
}

