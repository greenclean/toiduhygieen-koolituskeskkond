<?php namespace Halo;

use Toiduhygieen\Course;

class courses extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $courses;
    public $payment_method_id;
    public $requires_auth = true;
    public $team_lead;

    function index()
    {
        define('PAYMENT_BY_INVOICE', 1);
        define('PAYMENT_BY_BANK_TRANSFER', 2);

        $this->courses = Course::get_by_user((array) $this->auth);

        $this->payment_method_id = empty($_SESSION['payment_method_id']) ? FALSE : $_SESSION['payment_method_id'];
        unset($_SESSION['payment_method_id']);
    }
}