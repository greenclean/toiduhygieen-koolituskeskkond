<?php namespace Halo;

use Toiduhygieen\Order;
use Toiduhygieen\Pagination;

class orders extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $pagination;
    public $orders;

    function index()
    {

        if ($this->auth->is_admin != 1) {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

        $this->pagination = new Pagination(["course_name", "email", "name", "order_date", "order_id",
            "order_price", "order_status_name", "payment_method_name", "personal_code"]);

        $this->orders = Order::get(
            $this->pagination->criteria,
            $this->pagination->offset,
            $this->pagination->perPage);

        $this->pagination->setTotal(
            isset($this->orders[0]) ? $this->orders[0]["order_count"] : 0);

    }


    function order_status()
    {
        if ($this->auth->is_admin != 1) {
            $errors[] = __('No rights to do that!');
            require 'templates/error_template.php';
            exit();
        }

        $order_id = $_POST['id'];
        $status = $_POST['status'];
        Order::setStatus($order_id, $status);

        exit("OK");
    }
}