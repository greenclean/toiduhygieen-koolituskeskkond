<?php namespace Halo;

use Toiduhygieen\Certificate;
use Toiduhygieen\Language;
use Toiduhygieen\Test;

class tests extends Controller
{
    public $controller;
    public $action;
    public $params;
    public $auth;
    public $test;
    public $incorrectAnswers;
    public $NoOfCorrectAnswers;
    public $testScore;

    function index()
    {
        // Get user's latest paid but uncompleted test
        $test = get_first("
            SELECT * FROM orders 
              LEFT JOIN tests USING (order_id)
              JOIN courses USING (course_id)
            WHERE 
              orders.user_id = {$this->auth->user_id} 
              AND order_status_id = 1
              AND test_score IS NULL
              AND course_type != 'certificate'
            ORDER BY test_id DESC");

        if (!empty($test['order_id'])) { // Purchase was made

            if (empty($test['test_id'])) { // Test not created

                // Create a test and redirect to it
                redirect('tests/' . Test::generate($test['order_id'], $this->auth->language_id));
            }

            // Redirect user to his/her half finished test
            redirect('tests/' . $test['test_id']);
        }

        // Show an error
        $this->flash['danger'] = __('No tests found. Purchase a course to solve the test!');

    }

    function get_answers()
    {
        $test = $_POST['test'];
        $answers = get_all("select question_id, answer_id, answer, answer_letter, answer_chosen from answers left join question_set using (question_id) where test_id=$test");
        header('Content-Type: application/json');
        exit(json_encode(['res' => "OK", 'data' => $answers]));
    }

    function view()
    {
        $test_id = $this->params[0];

        validate($test_id);

        $test = Test::get($test_id);

        // Redirect to index if foreign test or invalid test_id
        if(empty($test['user_id']) || $test['user_id'] !== $this->auth->user_id){
            redirect('tests');
        }

        $this->test = $test;

    }

    function view_results()
    {

        // Init some values
        $user = $this->auth;
        $test_id = empty($this->params[0]) ? null : $this->params[0];
        $this->incorrectAnswers = [];

        // Check that the user has pressed Submit button (simple hacking prevention)
        if (!isset($_POST['testSubmit'])) {
            redirect('results/'.$test_id);
        }

        // Validate that a valid test_id is given
        validate($test_id, IS_ID);

        // Get the test's data
        $test = Test::get($test_id);

        // 404
        if (empty($test)) {
            error_out(__('No such test exists'));
        }

        // Check that this test has not been submitted before
        if ($test['test_score'] !== null) {
            if ($test['test_score'] < 75) {
                error_out(__('The result for this test was') . ' ' . $test['test_score'] . '. '. ' ' . __('Test failed.') .' '.
                    "<button onclick='location=\"courses\"' class=\"btn btn-primary\">" . __('Back to homepage') . "</button>");
            } else {
                redirect('certificates');
            }
        }

        // Check user answers
        foreach ($_POST['chosenAnswerLetter'] as $question_id => $answer_letter_chosen) {

            // SQLi protection
            if (strlen($answer_letter_chosen) > 1) {
                stop(400, 'Invalid answer_letter');
            }

            update('question_set', ['answer_chosen' => $answer_letter_chosen], "question_id = $question_id AND test_id = $test_id");

            $correct_answer = get_first("SELECT * FROM answers 
                                                LEFT JOIN questions using (question_id)
                                                LEFT JOIN test_categories using (test_category_id)
                                                LEFT JOIN translations on translation_source_id=answer_id
                                            WHERE question_id=$question_id AND answer_is_correct=1");
            $chosen_answer = get_first("SELECT * FROM question_set 
                                                LEFT JOIN answers using (question_id)
                                                LEFT JOIN translations on translations.translation_source_id = question_set.question_id 
                                            WHERE question_id = $question_id and answer_letter = '$answer_letter_chosen' and test_id = $test_id and translation_source = 'questions'");
            $user_language_code = Language::get_current()['language_code'];

            $answer_chosen_id = $chosen_answer['answer_id'];
            $user_choice_text = get_first("SELECT * FROM translations where translation_source_id = $answer_chosen_id and translation_source = 'answers'")["translation_in_$user_language_code"];

            $question = get_first("SELECT * FROM translations where translation_source_id = $question_id and translation_source = 'questions'");
            $explanation = get_first("SELECT * FROM translations where translation_source_id = $question_id and translation_source = 'questions.explanation'");
            if ($correct_answer['answer_id'] !== $chosen_answer['answer_id']) {
                $this->incorrectAnswers[$question_id]['general'] = $correct_answer;
                $this->incorrectAnswers[$question_id]['question_text'] = $question["translation_in_$user_language_code"] ? $question["translation_in_$user_language_code"] : $question["translation_phrase"];
                $this->incorrectAnswers[$question_id]['user_choice'] = !$answer_letter_chosen ? __('not selected') : $answer_letter_chosen;
                $this->incorrectAnswers[$question_id]['user_choice_text'] = !$user_choice_text ? __('not selected') : $user_choice_text;
                $this->incorrectAnswers[$question_id]['correct_choice_text'] = $correct_answer["translation_in_$user_language_code"];
                if (!empty($explanation)) {
                    $this->incorrectAnswers[$question_id]['question_explanation_text'] = $explanation["translation_in_$user_language_code"] ? $explanation["translation_in_$user_language_code"] : $explanation["translation_phrase"];
                }
            }
                insert("test_results", [
                    'test_id' => $test_id,
                    'question_id' => $question_id,
                    'test_answer_letter' => $correct_answer['answer_letter'],
                    'question_text' => $question["translation_in_$user_language_code"],
                    'user_choice_text' => $user_choice_text,
                    'correct_choice_text' => $correct_answer["translation_in_$user_language_code"],
                    'user_choice_letter' => $answer_letter_chosen]);
        }

        // Count questions in given test's question set
        $questions_count = get_one("
            SELECT count(question_id)  
            FROM `question_set`
            JOIN tests USING(test_id)
            WHERE test_id = $test_id
            GROUP BY test_id");

        // Assert that we have at least one question
        if (!is_numeric($questions_count) || $questions_count < 1) {
            error_out(__('Questions were missing'));
        }

        // Count score
        $this->NoOfCorrectAnswers = $questions_count - count($this->incorrectAnswers);
        $this->testScore = round($this->NoOfCorrectAnswers / $questions_count * 100);

        // Store users' test results
        if ($this->testScore >= 75 || $test['course_repeatable'] == 0) {
            update("tests", [
                'user_id' => $user->user_id,
                'test_score' => $this->testScore,
                'test_score_count' => $this->NoOfCorrectAnswers], "test_id=$test_id");
        } else {
            update("question_set", ['answer_chosen' => ''], "test_id=$test_id");
        }

        if ($this->testScore >= 75) {

            // Generate a new certificate
            $certificate_id = insert("certificates", [
                'test_id' => $test_id,
                'user_id' => $user->user_id,
                'certificate_include_english_version' => $user->user_has_unused_english_certificate_version]);

            // Create certificate PDF file
            $pdf = Certificate::create($test_id, $user->user_id);

            // Compose PDF file name
            $certificate_name = 'serts/Sertifikaat' . $certificate_id . '.pdf';

            // Write PDF to file
            $pdf = $pdf->Output($certificate_name, 'S');

            // Send certificate to the employer
            send_mail(
                $user->email,
                __('User') . ' ' . $user->name . ' ' . __('certificate'),
                __('Certificate is attached'),
                '',
                $pdf,
                $certificate_name);

            // If user has purchased additional english language version of the certificate
            if ($user->user_has_unused_english_certificate_version) {
                update('users', [
                    'user_has_unused_english_certificate_version' => 0
                ], "user_id = {$user->user_id}");
            }
        };

        $this->test = $test;
    }

    function save_questions()
    {
        $test_id = $this->params[0];
        header("Location:" . BASE_URL . 'tests/' . $test_id);
        exit();
    }

    function AJAX_save_user_choice()
    {
        if (empty($_POST['answer_id']) || !is_numeric($_POST['answer_id']) || $_POST['answer_id'] < 0) {
            stop(400, 'Invalid answer_id');
        }

        if (empty($_POST['question_id']) || !is_numeric($_POST['question_id']) || $_POST['question_id'] < 0) {
            stop(400, 'Invalid question_id');
        }

        if (empty($_POST['test_id']) || !is_numeric($_POST['test_id']) || $_POST['test_id'] < 0) {
            stop(400, 'Invalid test_id');
        }

        if (strlen($_POST['answer_letter']) > 1) {
            stop(400, 'Invalid answer_letter');
        }

        update("question_set", [
            'answer_chosen_id' => $_POST['answer_id'],
            'answer_chosen' => $_POST['answer_letter']
        ], "test_id=$_POST[test_id] AND question_id=$_POST[question_id]");

    }

}
