<?php

use Lkallas\Estonianpin\EstonianPIN;
use Toiduhygieen\Language;
use Toiduhygieen\Request;
use Toiduhygieen\Translation;

/**
 * Display a fancy error page and quit.
 * @param $error_msg
 * @param int $code Http status code to return
 */
function error_out($error_msg, $code = 500)
{

    // Return HTTP RESPONSE CODE to browser
    if(!empty($_SERVER['SERVER_PROTOCOL'])){
        header($_SERVER["SERVER_PROTOCOL"] . " $code Something went wrong", true, $code);
    }


    // Set error message
    $errors[] = $error_msg;

    if (Request::isAjax() || Request::isCli()) {
        stop(400, $error_msg);
    }

    // Show pretty error, too, to humans
    require __DIR__ . '/../templates/error_template.php';


    // Stop execution
    exit();
}

/**
 * @return string
 */
/**
 * @param string $translation_phrase Text to be translated
 * @param string|null $dynamic_source Specify database table.column for value validation
 * @param null $source_id
 * @return string|null
 * @throws Exception
 */
    function __(string $translation_phrase, string $dynamic_source = null, $source_id = null): ?string
{

    if (empty($dynamic_source) && !empty($source_id)) {
        throw new Exception("Invalid use of function __ ('$translation_phrase', '$dynamic_source', '$source_id'): missing dynamic_source");
    }

    if (!empty($dynamic_source) && empty($source_id)) {
        throw new Exception("Invalid use of function __ ('$translation_phrase', '$dynamic_source', '$source_id'): missing source_id");
    }

    $translation_phrase = trim($translation_phrase);

    // We don't want such things ending up in db
    if ($translation_phrase === '') {
        return '';
    }

    $translations = Translation::get_strings($dynamic_source ? 'dynamic' : 'static');

    // Db does not store more than 765 bytes
    if (strlen($translation_phrase) > 765) {
        error_out('Translation phrase ' . htmlentities($translation_phrase) . ' is too long.');
    }

    if ($dynamic_source) {
        // Return the translation if it's there
        if (isset($translations[$dynamic_source][$source_id])) {

            // Return original string if untranslated
            if ($translations[$dynamic_source][$source_id] === NULL)
                return $translation_phrase;

            // Else return translated string
            return $translations[$dynamic_source][$source_id];
        }
    } else {
        // Return the translation if it's there
        if (isset($translations[$translation_phrase])) {

            // Return original string if untranslated
            if ($translations[$translation_phrase] === NULL)
                return $translation_phrase;

            // Else return translated string
            return $translations[$translation_phrase];
        }
    }

    // Right, so we don't have this in our db yet

    // Insert new stub
    Translation::add($translation_phrase, $dynamic_source, $source_id);

    // And return the original string
    return nl2br($translation_phrase);

}

function debug($msg)
{
    if (defined(DEBUG) and DEBUG == true) {
        echo "<br>\n";
        $file = debug_backtrace()[0]['file'];
        $line = debug_backtrace()[0]['line'];
        echo "[" . $file . ":" . $line . "] <b>" . $msg . "</b>";
    }
}

function send_mail($to, $subject, $message, $sender_name = "toidu", $file = null, $filename = null)
{
    $headers = "MIME-Version: 1.0\n";
    $headers .= "From: =?utf-8?b?" . base64_encode($sender_name) . "?= <" . SMTP_FROM . ">\n";
    $headers .= "Content-Type: text/html;charset=utf-8\n";
    $headers .= "X-Mailer: PHP/" . phpversion();
    mailerSend($to, $subject, $message, $file, $filename);
    return true;
}

function mailerSend($to, $subject, $body, $file, $filename)
{
    $mail = new \PHPMailer\PHPMailer\PHPMailer();

    try {

        if (SMTP_USE_SENDMAIL) {
            $mail->isSendmail();
        } else if (SMTP_USE_MAIL) {
            $mail->isMail();
        } else {
            $mail->isSMTP();
            $mail->Host = SMTP_HOST;
            $mail->Port = SMTP_PORT;
            $mail->SMTPSecure = SMTP_ENCRYPTION;

            if (SMTP_AUTH) {

                // Whether to use SMTP authentication
                $mail->SMTPAuth = SMTP_AUTH;

                // Username to use for SMTP authentication - use full email address for gmail
                $mail->Username = SMTP_AUTH_USERNAME;


                // Password to use for SMTP authentication
                $mail->Password = SMTP_AUTH_PASSWORD;

            }

            $mail->SMTPDebug = SMTP_DEBUG;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

        }

        // Set mail properties
        $mail->setFrom(SMTP_FROM, SMTP_FROM);
        $mail->CharSet = 'UTF-8';
        $mail->addAddress($to);
        $mail->Subject = $subject;
        $mail->msgHTML($body);
        //$mail->AltBody = 'Sõnumi nägemiseks kasutage palun HTML ühilduvat meiliklienti.';

        // DKIM
        $mail->DKIM_domain = 'greenclean.ee';
        $mail->DKIM_private = 'certs/dkim_private_key.pem';
        $mail->DKIM_selector = '1520068111.greenclean';
        $mail->DKIM_passphrase = '';
        $mail->DKIM_identity = $mail->From;

        // Attachment
        if ($file) $mail->addStringAttachment($file, $filename);


        // Send
        if (!$mail->send()) {
            exit('Error sending mail : ' . $mail->ErrorInfo);
        }

    } catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }

}

function send_error_report($message, $trace = null)
{
    $trace = empty($trace) ? get_backtrace() : $trace;

    $auth = empty($_SESSION['user_id']) ? null : get_all("select * from users where user_id = $_SESSION[user_id]");

    $referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
    mailerSend(
        ERROR_EMAIL,
        'An error occurred in the project' . PROJECT_NAME,
        '<pre> PHP' . date('Y-m-d H:i:s ') . strtr(json_encode([
            'error_message' => $message,
            'SERVER' => [
                'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
                'HTTP_REFERER' => $referer,
            ],
            'POST' => $_POST,
            'SESSION' => $_SESSION,
            'COOKIE' => $_COOKIE,
            'AUTH' => $auth,
            'backtrace' => $trace
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_LINE_TERMINATORS), [
            ' ' => '&nbsp',
            '\r\n' => '<br>',
            '\n' => '<br>',
            '\"' => '"'
        ]) . '</pre>', null, null);
}

/**
 * @param $code
 * @param bool $data
 */
function stop($code, $data = false)
{
    $response['status'] = "$code";

    // Hide actual error in production in case of system errors
    if ($code == 500 && ENV == ENV_PRODUCTION) {
        send_error_report($data);
        $data = __('There was an error');
    }

    if ($data) {
        $response['data'] = $data;
    }

    // Change HTTP status code
    http_response_code($code);

    exit(json_encode($response));
}

function get_backtrace()
{
    $backtrace = debug_backtrace();
    array_shift($backtrace);

    return json_encode($backtrace, JSON_PRETTY_PRINT);
}

function isValidID($id): bool
{
    return !!filter_var($id, FILTER_VALIDATE_INT) && $id > 0;
}

function redirect($url)
{
    header("Location:" . BASE_URL . $url);
    exit();
}

function validate($var, $type = IS_ID, $name = null)
{

    if ($type == IS_ID && !isValidID($var)
        || $type == IS_ARRAY && !is_array($var)
        || $type == IS_NON_EMPTY && strlen($var) == 0) {
        error_out(__('Invalid parameter') . ($name ? " $name" : ""));
    }
}

function prettyDate($date): string
{
    if ($date == '-1') {
        return "never";
    }
    $time = strtotime($date);
    $now = time();
    $ago = $now - $time;
    if ($time == $now) {
        return "now";
    }
    if ($ago < 60) {
        $when = round($ago);
        $s = ($when == 1) ? "second" : "seconds";
        return "$when $s ago";
    } elseif ($ago < 3600) {
        $when = round($ago / 60);
        $m = ($when == 1) ? "minute" : "minutes";
        return "$when $m ago";
    } elseif ($ago >= 3600 && $ago < 86400) {
        $when = round($ago / 60 / 60);
        $h = ($when == 1) ? "hour" : "hours";
        return "$when $h ago";
    } elseif ($ago >= 86400 && $ago < 2629743.83) {
        $when = round($ago / 60 / 60 / 24);
        $d = ($when == 1) ? "day" : "days";
        return "$when $d ago";
    } elseif ($ago >= 2629743.83 && $ago < 31556926) {
        $when = round($ago / 60 / 60 / 24 / 30.4375);
        $m = ($when == 1) ? "month" : "months";
        return "$when $m ago";
    } else {
        $when = round($ago / 60 / 60 / 24 / 365);
        $y = ($when == 1) ? "year" : "years";
        return "$when $y ago";
    }
}

function isValidPersonalCodeOrBirthdate($personal_code): bool
{
    if (personalCodeIsPersonalCode($personal_code)) {
        return true;
    }

    $date = explode('-', $personal_code);

    if (empty($date)) {
        return false;
    }

    if (count($date) !== 3) {
        return false;
    }

    if (!is_numeric($date[0]) || !is_numeric($date[1]) || !is_numeric($date[2])) {
        return false;
    }

    if (!checkdate($date[1], $date[2], $date[0])) {
        return false;
    }

    return true;
}

function personalCodeIsPersonalCode($personal_code): bool
{
    if (!(new EstonianPIN())->validate($personal_code)) {
        return false;
    }

    return true;
}
