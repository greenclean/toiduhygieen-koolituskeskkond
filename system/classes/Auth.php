<?php namespace Halo;

use Toiduhygieen\Language;

/**
 * Class auth authenticates user and permits to check if the user has been logged in
 * Automatically loaded when the controller has $requires_auth property.
 */
class Auth
{

    public $user_id;
    public $password;
    public $active;
    public $email;
    public $deleted;
    public $personal_code;
    public $language_id;
    public $name;
    public $address;
    public $user;
    public $employer_id;
    public $approv_employer;
    public $allow_inherit;
    public $user_has_unused_english_certificate_version;
    public $faulty_personal_code;
    public $logged_in= FALSE;
    public $is_admin = FALSE;

    function __construct()
    {
        $_SESSION['language_id'] = Language::get_current()['language_id'];
        if (isset($_SESSION['user_id'])) {
            $this->logged_in = TRUE;
            $user = get_first("SELECT *
                                       FROM users
                                       WHERE user_id = '{$_SESSION['user_id']}'");
            $_SESSION['language_id'] = $user['language_id'];
            $this->load_user_data($user);
        }
    }

    /**
     * Verifies if the user is logged in and authenticates if not and POST contains username, else displays the login form
     * @return bool Returns true when the user has been logged in
     */
    function require_auth()
    {
        global $errors;
        $authenticated = false;

        // If user has already logged in...
        if ($this->logged_in) {
            return TRUE;
        }

        // Authenticate by POST data
        if (isset($_POST['password_or_id'])) {
            $name_or_email = addslashes(trim($_POST['email_or_name']));
            $password = addslashes(trim($_POST['password_or_id']));

            $users = get_all("SELECT * FROM users 
                                    WHERE users.name  = '$name_or_email'
                                    AND deleted = 0");


            if (empty($users)) {
                $flash['danger'] = __('Username does not exist!');
                require 'templates/auth_template.php';
                exit();
            }

            foreach ($users as $user) {

                $password_auth = password_verify($password, $user['password']);

                $id_auth = ($password == $user['personal_code'] ? true : false);

                if ($user['is_admin'] == 1 && $id_auth) {

                    $flash['danger'] = __('Wrong password! Don\'t forget, an administrator can only log in with a password.');
                    require 'templates/auth_template.php';
                    exit();

                } else {
                    if ($password_auth || $id_auth) {
                        $authenticated = true;
                        $correct_user = $user;
                    }
                }
            }

            if (!$authenticated) {
                $flash['danger'] = __('Invalid username or password');
                require 'templates/auth_template.php';
                exit();
            }

            // Log the user in
            $_SESSION['user_id'] = $correct_user['user_id'];

            // Redirect user from POST to GET to avoid ERR_CACHE_MISS
            header("Location: $_SERVER[REQUEST_URI]");
            exit();

        }

        // Display the login form
        require 'templates/auth_template.php';

        // Prevent loading the requested controller (not authenticated)
        exit();
    }

    /**
     * Dynamically add all user table fields as object properties to auth object
     * @param $user
     */
    public function load_user_data($user)
    {
        foreach ($user as $user_attr => $value) {
            $this->$user_attr = $value;

            // For SmartSupp (error_template can't access $this)
            $_SESSION['user'][$user_attr] = $value;
        }
        $this->logged_in = TRUE;
    }
}
