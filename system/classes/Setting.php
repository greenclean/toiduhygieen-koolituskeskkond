<?php


namespace Halo;


class Setting
{
    static function get($setting_name)
    {
        $setting_name = addslashes($setting_name);
        return get_one("SELECT setting_value from settings WHERE setting_name = '$setting_name'");
    }

    public static function set(string $setting_name, string $setting_value)
    {
        insert('settings', [
            'setting_name' => $setting_name,
            'setting_value' => $setting_value], true);
    }
}