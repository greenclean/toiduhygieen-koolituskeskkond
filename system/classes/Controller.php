<?php namespace Halo;

class Controller
{
    public $template = 'master';
    public $requires_auth = true;
    public $flash = null;

    public function set_flash()
    {
        if(!empty($_SESSION['flash'])){

            // Copy flash messages from session to controller
            $this->flash = $_SESSION['flash'];

            // Delete flash messages from session
            unset($_SESSION['flash']);
        }
    }

    function render($template)
    {
        // Make controller variables available to view
        extract(get_object_vars($this));

        // Load view
        require 'templates/' . $template . '_template.php';
    }
} 
