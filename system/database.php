<?php
/**
 * Database functions
 * Not included in class to shorten typing effort.
 */

connect_db();
function connect_db()
{
    global $db;

    @$db = new mysqli(DATABASE_HOSTNAME, DATABASE_USERNAME, DATABASE_PASSWORD);
    if ($connection_error = mysqli_connect_error()) {

        if(!empty($_SERVER['SERVER_PROTOCOL'])){
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        }

        exit('There was an error trying to connect to database at ' . DATABASE_HOSTNAME . ':<br><b>' . $connection_error . '</b>');

    }
    mysqli_select_db($db, DATABASE_DATABASE) or db_error_out('<b>Error:</b><i> ' . mysqli_error($db) . '</i><br>
		This usually means that MySQL does not have a database called <b>' . DATABASE_DATABASE . '</b>.<br><br>
		Create that database and import some structure into it from <b>doc/database.sql</b> file:<br>
		<ol>
		<li>Open database.sql</li>
		<li>Copy all the SQL code</li>  
		<li>Go to phpMyAdmin</li>
		<li>Create a database called <b>' . DATABASE_DATABASE . '</b></li>
		<li>Open it and go to <b>SQL</b> tab</li>
		<li>Paste the copied SQL code</li>
		<li>Hit <b>Go</b></li>
		</ol>', 500);

    // Switch to utf8
    if (!$db->set_charset("utf8")) {
        trigger_error(sprintf("Error loading character set utf8: %s\n", $db->error));
        exit();
    }

    q("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
    q("SET sql_mode=(SELECT REPLACE(@@sql_mode,'STRICT_TRANS_TABLES',''))");

}

function q($sql, &$query_pointer = NULL): int
{
    global $db;
    print_sql_when_debugging($sql);
    $query_pointer = mysqli_query($db, $sql) or db_error_out();
    switch (substr($sql, 0, 6)) {
        case 'SELECT':
            exit("q($sql): Please don't use q() for SELECTs, use get_one() or get_first() or get_all() instead.");
        case 'UPDA':
            exit("q($sql): Please don't use q() for UPDATEs, use update() instead.");
        default:
            return mysqli_affected_rows($db);
    }
}

/**
 * Prints out the SQL when DATABASE_DEBUG is truthy and optionally the call stack when DATABASE_DEBUG >2
 * @param $sql
 */
function print_sql_when_debugging($sql): void
{
    if (DATABASE_DEBUG > 0) {
        print_formatted_sql($sql);
        if (DATABASE_DEBUG > 1) {
            try {
                throw new Exception;
            } catch (Exception $e) {
                print_formatted_trace($e);
            }
        }
    }
}

/**
 * @param string $sql
 * @param string $color
 */
function print_formatted_sql(string $sql): void
{
    $sql = trim($sql);
    $color = get_debug_line_color($sql);
    echo "<pre style='color:$color; margin:0'>";
    echo $sql;
    echo "</pre>";
}

/**
 * @param Exception $e
 */
function print_formatted_trace(Exception $e): void
{
    $trace = get_formatted_trace($e);
    echo "<pre style='color:grey; margin: 0 0 6px;'>";
    print_r($trace);
    echo "</pre>";
}

/**
 * @param Exception $e
 * @return string|string[]|null
 */
function get_formatted_trace(Exception $e)
{
    $trace = htmlentities($e->getTraceAsString());
    $trace = remove_basedir_from_trace($trace);
    $trace = remove_self_from_trace($trace);
    $trace = remove_main_from_trace($trace);
    return $trace;
}

/**
 * Removes the database_debug part from trace
 * @param string $trace
 * @return string|string[]|null
 */
function remove_self_from_trace(string $trace)
{
    $trace = preg_replace("/#0.*\n/", '', $trace);
    return $trace;
}

/**
 * Removes "#N {main}" part from the trace
 * @param string $trace
 * @return string
 */
function remove_main_from_trace(string $trace): string
{
    return preg_replace("/#\d+ \{main\}/", '', $trace);
}

/**
 * @param string $trace
 * @return string|string[]|null
 */
function remove_basedir_from_trace(string $trace)
{
    $basedir = dirname(__DIR__) . '/';
    $trace = preg_replace("/" . preg_quote($basedir, '/') . "/", '', $trace);
    return $trace;
}

/**
 * @param string $sql
 * @return string
 */
function get_debug_line_color(string $sql): string
{
    $lookup = [
        'SELEC' => 'green',
        'INSER' => 'blue',
        'UPDAT' => 'purple',
        'ALTER' => 'navy',
        'CREAT' => 'brown',
        'RENAM' => 'pink',
        'DELET' => 'red',
        'DROP ' => 'red'
    ];

    $color = @$lookup[strtoupper(substr($sql, 0, 5))];
    if (!$color) {
        $color = 'black';
    }
    return $color;
}

function get_one($sql, $debug = FALSE)
{
    global $db;

    print_sql_when_debugging($sql);
    switch (substr(trim($sql), 0, 6)) {
        case 'SELECT':
            $q = mysqli_query($db, $sql) or db_error_out();
            $result = mysqli_fetch_array($q);
            return empty($result) ? NULL : $result[0];
        default:
            exit('get_one("' . $sql . '") failed because get_one expects SELECT statement.');
    }
}

function get_all($sql)
{
    global $db;
    print_sql_when_debugging($sql);
    $q = mysqli_query($db, $sql) or db_error_out();
    while (($result[] = mysqli_fetch_assoc($q)) || array_pop($result)) {
        ;
    }
    return $result;
}

function get_first($sql): array
{
    global $db;
    print_sql_when_debugging($sql);
    $q = mysqli_query($db, $sql) or db_error_out();
    $first_row = mysqli_fetch_assoc($q);
    return empty($first_row) ? array() : $first_row;
}

function get_col($sql): array
{
    global $db;
    $result = [];

    print_sql_when_debugging($sql);
    $col = preg_match('/^SELECT(?:\s+DISTINCT)*\s+([^ ]+)\s+FROM.*/iUm', $sql, $output_array);
    if (count($output_array) != 2) {
        db_error_out($sql, "get_col() cannot determine selected column");
    }
    $col = $output_array[1];

    // Check that there is just a single column selected
    if (strpos($col, ',') !== false) {
        db_error_out($sql, 'get_col() requires that exactly one column is selected');
    }

    $q = mysqli_query($db, $sql) or db_error_out();
    while (($row = mysqli_fetch_assoc($q))) {
        $result[] = $row[$col];
    }
    return $result;
}

function db_error_out($sql = null, $db_error = null)
{
    global $db;

    $protocol = !empty($_SERVER["SERVER_PROTOCOL"]) ? $_SERVER["SERVER_PROTOCOL"] : "HTTP";
    header($protocol . " 500 Internal server error", true, 500);

    $preg_delimiter = '/';

    $db_error = $db_error ? $db_error : mysqli_error($db);

    if (strpos($db_error, 'You have an error in SQL syntax') !== FALSE) {
        $db_error = '<b>Syntax error in</b><pre> ' . substr($db_error, 135) . '</pre>';

    }


    $backtrace = debug_backtrace();

    $file = $backtrace[1]['file'];
    $file = str_replace(dirname(__DIR__), '', $file);

    $line = $backtrace[1]['line'];
    $function = isset($backtrace[1]['function']) ? $backtrace[1]['function'] : NULL;

    // Get arguments
    $args = isset($backtrace[1]['args']) ? $backtrace[1]['args'] : NULL;

    // Protect the next statement failing with "Malformed UTF-8 characters, possibly incorrectly encoded" error when $args contains binary
    array_walk_recursive($args, function (&$item) {

        // Truncate item to 1000 bytes if it is longer
        if (strlen($item) > 1000) $item = mb_substr($item, 0, 1000);


        $item = mb_convert_encoding($item, 'UTF-8', 'UTF-8');

        $item = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '.', $item);
    });

    // Serialize arguments
    $args = json_encode($args, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

    // Fault highlight
    preg_match("/check the manual that corresponds to your MySQL server version for the right syntax to use near '([^']+)'./", $db_error, $output_array);
    if (!empty($output_array[1])) {
        $fault = $output_array[1];
        $fault_quoted = preg_quote($fault);


        $args = preg_replace($preg_delimiter . "(\w*\s*)$fault_quoted" . $preg_delimiter, "<span class='fault'>\\1$fault</span>", $args);

        $args = stripslashes($args);
    }


    $location = "<b>$file</b><br><b>$line</b>: ";
    if (!empty($function)) {

        $args = str_replace("SELECT", '<br>SELECT', $args);
        $args = str_replace("\n", '<br>', $args);
        $args = str_replace("\t", '&nbsp;', $args);


        $code = "$function(<span style=\" font-family: monospace; ;padding:0; margin:0\">$args</span>)";
        $location .= "<span class=\"line-number-position\">&#x200b;<span class=\"line-number\">$code";

    }


    // Generate stack trace
    $e = new Exception();
    $trace = print_r(preg_replace('/#(\d+) \//', '#$1 ', str_replace(dirname(dirname(__FILE__)), '', $e->getTraceAsString())), 1);
    $trace = nl2br(preg_replace('/(#1.*)\n/', "<b>$1</b>\n", $trace));

    $output = '<h1>Database error</h1>' .
        '<p>' . $db_error . '</p>' .
        '<p><h3>Location</h3> ' . $location . '<br>' .
        '<p><h3>Stack trace</h3>' . $trace . '</p>';


    if (isset($_GET['ajax']) || PHP_SAPI === 'cli') {
        ob_end_clean();
        echo strip_tags($output);

    } else {
        $errors[] = $output;
        require 'templates/error_template.php';
    }

    die();

}

/**
 * @param $table string The name of the table to be inserted into.
 * @param $data array Array of data. For example: array('field1' => 'mystring', 'field2' => 3);
 * @param bool $onDuplicateKeyUpdate Whether or not to add ON DUPLICATE KEY UPDATE part.
 * @return bool|int Returns the ID of the inserted row or FALSE when fails.
 */
function insert($table, $data = [], $onDuplicateKeyUpdate = false)
{
    global $db;
    if ($table and is_array($data)) {
        $values = implode(',', escape($data));
        $onDuplicateKeyUpdate = $onDuplicateKeyUpdate ? "ON DUPLICATE KEY UPDATE {$values}" : '';
        $values = $values ? "SET {$values} " . $onDuplicateKeyUpdate : '() VALUES()';
        $sql = "INSERT INTO `{$table}` $values";
        print_sql_when_debugging($sql);
        $q = mysqli_query($db, $sql) or db_error_out($sql);
        $id = mysqli_insert_id($db);
        return ($id > 0) ? $id : FALSE;
    } else {
        return FALSE;
    }
}

function update($table, array $data, $where)
{
    global $db;
    if ($table and is_array($data) and !empty($data)) {
        $values = implode(',', escape($data));

        if (!empty($where)) {
            $sql = "UPDATE `{$table}` SET {$values} WHERE {$where}";
        } else {
            $sql = "UPDATE `{$table}` SET {$values}";
        }
        print_sql_when_debugging($sql);
        $id = mysqli_query($db, $sql) or db_error_out();
        return ($id > 0) ? $id : FALSE;
    } else {
        return FALSE;
    }
}

function escape(array $data): array
{
    $values = array();
    $IN_regex = '/^IN\s*\(/i'; // IN(foo, bar)

    if (!empty($data)) {

        // Loop over all members of $data
        foreach ($data as $field => $value) {

            // Escape field names containing database name
            $field = str_replace('.', '`.`', $field);

            // Skip escaping if field is numeric (user must escape)
            if (is_numeric($field)) {
                $values[] = $value;
            } // If value is supposed to be NULL
            elseif ($value === NULL) {
                $values[] = "`$field`=NULL";

                // If value is array and has a member called no_escape then skip this member
            } elseif (is_array($value) && isset($value['no_escape'])) {
                $values[] = "`$field`=" . addslashes($value['no_escape']);

                // If value begins with IN(, do not escape
            } elseif (preg_match($IN_regex, $value)) {
                $values[] = "`$field` " . $value;

                // All other cases
            } else {
                $values[] = "`$field`='" . addslashes($value) . "'";
            }
        }
    }
    return $values;
}