<script>
    // Error translations
    SERVER_ERROR_UNKNOWN_FORMAT = '<?=__('Server replied in an unknown format')?>';
    SERVER_ERROR_FORBIDDEN = '<?=__('Forbidden')?>';
    SERVER_ERROR_OTHER = '<?=__('Server responded with an error. Try again later')?>';
    UPLOAD_ERROR_FILE_NOT_RECOGNIZED = '<?=__('Uploaded file cannot be recognised')?>';
    SERVER_ERROR_OOPS = '<?=__('Error')?>';
    ENV = '<?= ENV ?>';
    ENV_PRODUCTION = '<?= ENV_PRODUCTION ?>';
    ENV_DEVELOPMENT = '<?= ENV_DEVELOPMENT ?>';
</script>